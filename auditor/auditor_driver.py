"""
AuditorDriver module
"""

import configargparse
from aiohttp import web
from aiohttp.web_request import Request
from decimal import Decimal
import simplejson as json
from web3.auto import w3
from typing import Dict, List, Set, AsyncIterable
import numpy as np
import coloredlogs, verboselogs
import os
from collections import defaultdict
import asyncio
import websockets
from websockets import WebSocketServerProtocol, WebSocketClientProtocol
import copy

from trading_clients.auditor.shared.identifier import Identifier
from trading_clients.auditor.shared.identifier_types import IdentifierTypes
from trading_clients.auditor.state.identifiers.trader_identifier import TraderIdentifier
from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.state.market_specs import MarketSpecs
from trading_clients.auditor.state.organic_insurance_fund import OrganicInsuranceFund
from trading_clients.auditor.state.price_source import PriceSource
from trading_clients.auditor.state.registered_member import RegisteredMember
from trading_clients.auditor.state.stats import Stats
from trading_clients.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from trading_clients.auditor.state.identifiers.position_identifier import (
    PositionIdentifier,
)
from trading_clients.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from trading_clients.auditor.transactions.advance_epoch import AdvanceEpoch
from trading_clients.auditor.transactions.all_price_checkpoints import (
    AllPriceCheckpoints,
)
from trading_clients.auditor.transactions.cancel import Cancel
from trading_clients.auditor.transactions.change import Change
from trading_clients.auditor.transactions.funding import Funding
from trading_clients.auditor.transactions.genesis import Genesis
from trading_clients.auditor.transactions.identifiers.cancel_identifier import (
    CancelIdentifier,
)
from trading_clients.auditor.transactions.identifiers.fill_identifier import (
    FillIdentifier,
)
from trading_clients.auditor.transactions.identifiers.post_identifier import (
    PostIdentifier,
)
from trading_clients.auditor.transactions.identifiers.price_checkpoint_identifier import (
    PriceCheckpointIdentifier,
)
from trading_clients.auditor.transactions.identifiers.strategy_update_identifier import (
    StrategyUpdateIdentifier,
)
from trading_clients.auditor.transactions.leave import Leave
from trading_clients.auditor.transactions.liquidation import Liquidation
from trading_clients.auditor.transactions.pnl_settlement import PnlSettlement
from trading_clients.auditor.transactions.post_order import PostOrder
from trading_clients.auditor.transactions.trade_mining import TradeMining
from trading_clients.auditor.transactions.trader_update import TraderUpdate
from trading_clients.auditor.transactions.withdraw import Withdraw
from trading_clients.auditor.transactions.withdraw_ddx import WithdrawDDX
from trading_clients.auditor.utils import (
    bytes_array_to_h256,
    h256_to_bytes_array,
    pack_bytes,
    ComplexOutputEncoder,
)
from trading_clients.auditor.smt.h256 import H256
from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.book_order import BookOrder
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.position import Position
from trading_clients.auditor.state.price import Price
from trading_clients.auditor.state.strategy import Strategy
from trading_clients.auditor.state.trader import Trader
from trading_clients.auditor.state.item_types import ItemType
from trading_clients.auditor.transactions.complete_fill import CompleteFill
from trading_clients.auditor.transactions.partial_fill import PartialFill
from trading_clients.auditor.transactions.post import Post
from trading_clients.auditor.transactions.strategy_update import StrategyUpdate
from trading_clients.auditor.shared.order_book import OrderBook
from trading_clients.client.asyncio_utils import empty_queue
from trading_clients.client.rest_message import RestMessage, RestMessageType
from trading_clients.client.websocket_message import (
    WebsocketEventType,
    WebsocketMessageType,
    WebsocketMessage,
)
from trading_clients.contract_addresses import contract_addresses
from trading_clients.helpers.utils import get_deployment_from_staging_env

fmt_str = "[%(asctime)s] %(levelname)s @ line %(lineno)d: %(message)s"
log_level = os.environ.get("PYTHON_LOG").upper() if "PYTHON_LOG" in os.environ else 100
coloredlogs.install(
    fmt="%(asctime)s,%(msecs)03d %(levelname)s %(message)s", level=log_level,
)
logger = verboselogs.VerboseLogger(__name__)


class AuditorDriver:
    """
    Defines an AuditorDriver.
    """

    def __init__(self, staging_env: str):
        """
        Initialize an AuditorDriver. An Auditor allows any third-party to
        process a state snapshot of the DerivaDEX Sparse Merkle Tree (SMT)
        and transaction log entries to validate the integrity of the
        exchange. The driver essentially maintains its own SMT and can
        transition its state upon receiving transaction log entries. The
        root hashes must match. As a nice byproduct, the Auditor can serve
        as the backend data store by serving an intuitive REST and
        WebSocket API to traders. In this way, traders can send commands
        and subscribe to exchange information, while also having the
        confidence that their data is up-to-date and accurate.

        Parameters
        ----------
        staging_env : str
            Staging env to determine USDC ERC-20 token address
        """

        # Set the USDC ERC-20 token address
        self.usdc_address = contract_addresses[
            get_deployment_from_staging_env(staging_env)
        ]["addresses"]["usdcAddress"]

    def _reset(self):
        # Initialize an empty SMT
        self.smt = SparseMerkleTree()

        # Initialize empty order book and latest price leaves. These
        # technically are abstractions above the SMT for easier/faster
        # access for a trader client
        self.order_books: Dict[str, OrderBook] = {}
        self.latest_price_leaves: Dict[str, Price] = {}

        # Initialize a data construct for pending transaction log
        # entries. The Trader API may on rare occasion send transactions
        # to subscribing WebSocket customers out of order. We maintain
        # a backlog of pending transaction log entries in this scenario
        # so that although we may receive transaction log entries out
        # of order, we will always apply them to the SMT in order.
        self.pending_tx_log_entries = defaultdict(dict)

        # Current root hash derived locally. For every transaction event
        # emitted by the transaction log, the state root hash prior to
        # the transaction being applied. As such, we maintain the
        # current root hash to compare against the next inbound
        # transaction log's root hash.
        self.current_tx_root_hash = (
            "0x0000000000000000000000000000000000000000000000000000000000000000"
        )
        self.current_batch_tx_root_hash = (
            "0x0000000000000000000000000000000000000000000000000000000000000000"
        )

        # A set of user subscriptions (identifier topic strings). Only
        # the superset topic strings are saved at any given time.
        self.user_subscriptions: Dict[WebSocketServerProtocol, Set[str]] = {}

        self.first_head = True

        # More Pythonic ask-for-forgiveness approaches for the queues
        # and event below

        # Set up an asyncio queue for messages that will be added by
        # the Auditor and popped to send to the API
        try:
            empty_queue(self.api_auditor_queue)
        except AttributeError:
            self.api_auditor_queue = asyncio.Queue()

        # Set up an asyncio queue for messages that will be added by
        # the Auditor and popped to send to the Trader
        try:
            empty_queue(self.trader_auditor_queue)
        except AttributeError:
            self.trader_auditor_queue = asyncio.Queue()

        # Check if the transaction log partial (state snapshot and tx log snapshot) has been
        # processed
        try:
            self.tx_log_partial_loaded_event.clear()
        except AttributeError:
            self.tx_log_partial_loaded_event: asyncio.Event = asyncio.Event()

    @property
    def expected_epoch_id(self):
        return self._expected_epoch_id

    @expected_epoch_id.setter
    def expected_epoch_id(self, epoch_id):
        self._expected_epoch_id = epoch_id

    @property
    def expected_tx_ordinal(self):
        return self._expected_tx_ordinal

    @expected_tx_ordinal.setter
    def expected_tx_ordinal(self, tx_ordinal):
        self._expected_tx_ordinal = tx_ordinal

    def add_subscription(
        self, websocket: WebSocketServerProtocol, subscription_identifier: Identifier,
    ) -> bool:
        """
        Consider adding a subscription identifier to the set of Trader
        subscriptions. In order for it to be added, it must be of
        valid format and not already a subset of an existing
        subscription.

        Parameters
        ----------
        subscription_identifier : Identifier
            Subscription identifier being considered
        """

        # Loop through the set of existing Trader subscriptions
        for subscription in self.user_subscriptions[websocket]:
            # Split the subscription strings on the "/" delimiter
            subscription_parts = subscription.split("/")

            if subscription_parts[1] == IdentifierTypes.STRATEGY:
                # If existing subscription topic is tracking Strategy
                # leaf updates, wrap it in a StrategyIdentifier
                target_subscription_identifier = StrategyIdentifier.decode_topic_string_into_cls(
                    subscription
                )
            elif subscription_parts[1] == IdentifierTypes.POSITION:
                # If existing subscription topic is tracking Position
                # leaf updates, wrap it in a PositionIdentifier
                target_subscription_identifier = PositionIdentifier.decode_topic_string_into_cls(
                    subscription
                )
            elif subscription_parts[1] == IdentifierTypes.BOOK_ORDER:
                # If existing subscription topic is tracking BookOrder
                # leaf updates, wrap it in a BookOrderIdentifier
                target_subscription_identifier = BookOrderIdentifier.decode_topic_string_into_cls(
                    subscription
                )
            elif subscription_parts[1] == IdentifierTypes.PRICE_CHECKPOINT:
                # If existing subscription topic is tracking
                # PriceCheckpoint transaction events, wrap it in a
                # PriceCheckpointIdentifier
                target_subscription_identifier = PriceCheckpointIdentifier.decode_topic_string_into_cls(
                    subscription
                )
            elif subscription_parts[1] == IdentifierTypes.FILL:
                # If existing subscription topic is tracking Fill
                # transaction events, wrap it in a FillIdentifier
                target_subscription_identifier = FillIdentifier.decode_topic_string_into_cls(
                    subscription
                )
            elif subscription_parts[1] == IdentifierTypes.STRATEGY_UPDATE:
                # If existing subscription topic is tracking
                # StrategyUpdate transaction events, wrap it in a
                # StrategyUpdateIdentifier
                target_subscription_identifier = StrategyUpdateIdentifier.decode_topic_string_into_cls(
                    subscription
                )
            elif subscription_parts[1] == IdentifierTypes.POST:
                # If existing subscription topic is tracking Post
                # transaction events, wrap it in a PostIdentifier
                target_subscription_identifier = PostIdentifier.decode_topic_string_into_cls(
                    subscription
                )
            elif subscription_parts[1] == IdentifierTypes.CANCEL:
                # If existing subscription topic is tracking Cancel
                # transaction events, wrap it in a CancelIdentifier
                target_subscription_identifier = CancelIdentifier.decode_topic_string_into_cls(
                    subscription
                )

            if subscription_identifier.encompasses_identifier(
                target_subscription_identifier
            ):
                # If the proposed subscription topic is a superset of
                # an existing subscription, remove the existing one
                # since it's now unnecessary
                self.user_subscriptions[websocket].remove(subscription)
            elif target_subscription_identifier.encompasses_identifier(
                subscription_identifier
            ):
                # If the proposed subscription topic is a subset of
                # an existing subscription, exit without adding the
                # proposed subscription since it's unnecessary
                return False

        # Add the proposed subscription topic string to the list of
        # existing Trader subscriptions
        self.user_subscriptions[websocket].add(subscription_identifier.topic_string)

        return True

    async def unregister_trader(self, websocket: WebSocketServerProtocol):
        """
        Unregister a trader when they disconnect to the Auditor, which
        will clear out any subscriptions they may have.
        """

        del self.user_subscriptions[websocket]

    def process_tx_log_event(
        self, tx_log_event: Dict, suppress_trader_queue: bool
    ) -> None:
        """
        Process an individual transaction log entry. Each entry will be
        appropriately decoded into the correct Transaction type and
        handled differently to adjust the SMT.

        Parameters
        ----------
        tx_log_event : Dict
            A transaction log event
        suppress_trader_queue : bool
            Suppress trader queue messages
        """

        # Add the transaction log event to the pending transaction log
        # entries to be processed either now or later
        self.pending_tx_log_entries[tx_log_event["epochId"]][
            tx_log_event["txOrdinal"]
        ] = tx_log_event

        # Loop through all the pending transaction log entries that
        # should be processed now given the expected epoch ID and
        # transaction ordinal
        while (
            self.expected_epoch_id in self.pending_tx_log_entries
            and self.expected_tx_ordinal
            in self.pending_tx_log_entries[self.expected_epoch_id]
        ):
            # Retrieve the transaction log entry event that should be
            # processed now
            tx_log_event = self.pending_tx_log_entries[self.expected_epoch_id].pop(
                self.expected_tx_ordinal
            )

            if (
                tx_log_event["batchId"] == self.latest_batch_id
                and tx_log_event["stateRootHash"] != self.current_batch_tx_root_hash
            ):
                raise Exception(
                    f"Tx log root hash ({tx_log_event['stateRootHash']}) != current batch root hash ({self.current_batch_tx_root_hash}"
                )
            elif (
                tx_log_event["batchId"] != self.latest_batch_id
                and tx_log_event["stateRootHash"] != self.current_tx_root_hash
            ):
                # Ensure that the current local state root hash matches
                # the inbound transaction log entry's state root hash
                raise Exception(
                    f"Tx log root hash ({tx_log_event['stateRootHash']}) != current root hash ({self.current_tx_root_hash}"
                )

            # Extract the transaction type from the event (e.g. Post,
            # CompleteFill, etc.)
            tx_type = tx_log_event["event"]["t"]

            logger.success(
                f"\u2713 - processing ({tx_type}; tx log root hash ({tx_log_event['stateRootHash']}) == current root hash ({self.current_tx_root_hash}; tx ({tx_log_event}"
            )

            if tx_type == "Post":
                # Post transaction
                tx: PostOrder = PostOrder.decode_value_into_cls(tx_log_event)
                tx.process_tx(
                    self.smt,
                    self.trader_auditor_queue,
                    suppress_trader_queue,
                    order_books=self.order_books,
                )
            elif tx_type == "CompleteFill":
                # CompleteFill transaction
                tx: CompleteFill = CompleteFill.decode_value_into_cls(tx_log_event)
                tx.process_tx(
                    self.smt,
                    self.trader_auditor_queue,
                    suppress_trader_queue,
                    order_books=self.order_books,
                    usdc_address=self.usdc_address,
                )
            elif tx_type == "PartialFill":
                # PartialFill transaction
                tx: PartialFill = PartialFill.decode_value_into_cls(tx_log_event)
                tx.process_tx(
                    self.smt,
                    self.trader_auditor_queue,
                    suppress_trader_queue,
                    order_books=self.order_books,
                    usdc_address=self.usdc_address,
                )
            elif tx_type == "Liquidation":
                # Liquidation transaction
                tx: Liquidation = Liquidation.decode_value_into_cls(tx_log_event)
                tx.process_tx(
                    self.smt,
                    self.trader_auditor_queue,
                    suppress_trader_queue,
                    order_books=self.order_books,
                    usdc_address=self.usdc_address,
                    latest_price_leaves=self.latest_price_leaves,
                )
            elif tx_type == "Cancel":
                # Cancel transaction
                tx: Cancel = Cancel.decode_value_into_cls(tx_log_event)
                tx.process_tx(
                    self.smt,
                    self.trader_auditor_queue,
                    suppress_trader_queue,
                    order_books=self.order_books,
                )
            elif tx_type == "StrategyUpdate":
                # StrategyUpdate transaction
                tx: StrategyUpdate = StrategyUpdate.decode_value_into_cls(tx_log_event)
                tx.process_tx(
                    self.smt, self.trader_auditor_queue, suppress_trader_queue,
                )
            elif tx_type == "TraderUpdate":
                # TraderUpdate transaction
                tx: TraderUpdate = TraderUpdate.decode_value_into_cls(tx_log_event)
                tx.process_tx(
                    self.smt, self.trader_auditor_queue, suppress_trader_queue,
                )
            elif tx_type == "PriceCheckpoint":
                # AllPriceCheckpoints transaction
                tx: AllPriceCheckpoints = AllPriceCheckpoints.decode_value_into_cls(
                    tx_log_event
                )
                tx.process_tx(
                    self.smt,
                    self.trader_auditor_queue,
                    suppress_trader_queue,
                    latest_price_leaves=self.latest_price_leaves,
                )
            elif tx_type == "PnlSettlement":
                # PnlSettlement transaction
                tx: PnlSettlement = PnlSettlement.decode_value_into_cls(tx_log_event)
                tx.process_tx(
                    self.smt,
                    self.trader_auditor_queue,
                    suppress_trader_queue,
                    latest_price_leaves=self.latest_price_leaves,
                    usdc_address=self.usdc_address,
                )
            elif tx_type == "Funding":
                # Funding transaction
                tx: Funding = Funding.decode_value_into_cls(tx_log_event)
                tx.process_tx(
                    self.smt,
                    self.trader_auditor_queue,
                    suppress_trader_queue,
                    latest_price_leaves=self.latest_price_leaves,
                    get_funding_rate=self.get_funding_rate,
                    get_position_leaves_for_symbol=self.get_position_leaves_for_symbol,
                    get_price_leaves_for_symbol=self.get_price_leaves_for_symbol,
                    usdc_address=self.usdc_address,
                )
            elif tx_type == "TradeMining":
                # Funding transaction
                tx: TradeMining = TradeMining.decode_value_into_cls(tx_log_event)
                tx.process_tx(
                    self.smt,
                    self.trader_auditor_queue,
                    suppress_trader_queue,
                    get_stats_leaves=self.get_stats_leaves,
                )
            elif tx_type == "Withdraw":
                # Withdraw transaction
                tx: Withdraw = Withdraw.decode_value_into_cls(tx_log_event)
                tx.process_tx(
                    self.smt, self.trader_auditor_queue, suppress_trader_queue,
                )
            elif tx_type == "WithdrawDdx":
                # WithdrawDDX transaction
                tx: WithdrawDDX = WithdrawDDX.decode_value_into_cls(tx_log_event)
                tx.process_tx(
                    self.smt, self.trader_auditor_queue, suppress_trader_queue,
                )
            elif tx_type == "EpochMarker":
                # EpochMarker transaction
                if tx_log_event["event"]["c"]["kind"] == "Genesis":
                    tx: Genesis = Genesis.decode_value_into_cls(tx_log_event)
                    tx.process_tx(
                        self.smt,
                        self.trader_auditor_queue,
                        suppress_trader_queue,
                        auditor_instance=self,
                        expected_epoch_id=AuditorDriver.expected_epoch_id.fset,
                        expected_tx_ordinal=AuditorDriver.expected_tx_ordinal.fset,
                    )
                else:
                    tx: AdvanceEpoch = AdvanceEpoch.decode_value_into_cls(tx_log_event)
                    tx.process_tx(
                        self.smt,
                        self.trader_auditor_queue,
                        suppress_trader_queue,
                        auditor_instance=self,
                        expected_epoch_id=AuditorDriver.expected_epoch_id.fset,
                        expected_tx_ordinal=AuditorDriver.expected_tx_ordinal.fset,
                    )
            elif tx_type == "MembershipChange":
                if tx_log_event["event"]["c"]["action"] == "Change":
                    tx: Change = Change.decode_value_into_cls(tx_log_event)
                    tx.process_tx(
                        self.smt, self.trader_auditor_queue, suppress_trader_queue
                    )
                else:
                    tx: Leave = Leave.decode_value_into_cls(tx_log_event)
                    tx.process_tx(
                        self.smt, self.trader_auditor_queue, suppress_trader_queue
                    )

            # Set the current state root hash locally
            self.current_tx_root_hash = (
                f"0x{h256_to_bytes_array(self.smt.root()).hex()}"
            )

            if self.latest_batch_id != tx_log_event["batchId"]:
                self.current_batch_tx_root_hash = tx_log_event["stateRootHash"]
                self.latest_batch_id = tx_log_event["batchId"]

            # Increment the expected transaction ordinal by 1 (will be
            # reset back to 0 only when the epoch advances)
            self.expected_tx_ordinal += 1
            logger.info(
                f"Processed {tx_type}; arrived at new state root hash ({self.current_tx_root_hash})"
            )

    def process_state_snapshot(
        self, expected_epoch_id: int, state_snapshot: Dict
    ) -> None:
        """
        Process a state snapshot and initialize the SMT accordingly.

        Parameters
        ----------
        expected_epoch_id : int
            Expected epoch ID for incoming transactions after the
            state snapshot
        state_snapshot : Dict
            The state snapshot structured as a dictionary with the
            format: {<hash(leaf_key, leaf_value), (leaf_key, leaf_value)}
        """

        # Loop through state snapshot dictionary items
        for state_snapshot_key, state_snapshot_value in state_snapshot.items():
            # Compute the first and second words since with these two
            # blocks of data, we can determine what type of leaf we are
            # dealing with

            # Peel the item discriminant off (the first byte of the
            # leaf key) to determine what kind of leaf it is
            item_discriminant = w3.toInt(bytes.fromhex(state_snapshot_key[2:])[:1])

            if item_discriminant == ItemType.TRADER:
                # Leaf item is of Trader type
                item = Trader.abi_decode_value_into_cls(state_snapshot_value)
            elif item_discriminant == ItemType.STRATEGY:
                # Leaf item is of Strategy type
                item = Strategy.abi_decode_value_into_cls(state_snapshot_value)
            elif item_discriminant == ItemType.POSITION:
                # Leaf item is of Position type
                item = Position.abi_decode_value_into_cls(state_snapshot_value)
            elif item_discriminant == ItemType.BOOK_ORDER:
                # Leaf item is of BookOrder type
                item = BookOrder.abi_decode_value_into_cls(state_snapshot_value)

                # Convert the state snapshot key from hexstr to bytes
                state_snapshot_key_bytes = bytes.fromhex(state_snapshot_key[2:])
                book_order_identifer = BookOrderIdentifier.decode_key_into_cls(
                    state_snapshot_key_bytes, item
                )
                # Initialize an OrderBook for this symbol if it doesn't already exist
                if book_order_identifer.symbol not in self.order_books:
                    self.order_books[book_order_identifer.symbol] = OrderBook(self.smt)

                # Process BookOrder leaf in local OrderBook (hasn't technically made it's way
                # into SMT yet, but that's ok
                self.order_books[
                    book_order_identifer.symbol
                ].process_book_order_from_state(
                    bytes_array_to_h256(state_snapshot_key_bytes), item
                )
            elif item_discriminant == ItemType.PRICE:
                # Leaf item is of Price type
                item = Price.abi_decode_value_into_cls(state_snapshot_value)
            elif item_discriminant == ItemType.INSURANCE_FUND:
                # Leaf item is of OrganicInsuranceFund type
                item = OrganicInsuranceFund.abi_decode_value_into_cls(
                    state_snapshot_value
                )
            elif item_discriminant == ItemType.STATS:
                # Leaf item is of Stats type
                item = Stats.abi_decode_value_into_cls(state_snapshot_value)
            elif item_discriminant == ItemType.MEMBER:
                # Leaf item is of RegisteredMember type
                item = RegisteredMember.abi_decode_value_into_cls(state_snapshot_value)
            elif item_discriminant == ItemType.MARKET_SPECS:
                # Leaf item is of MarketSpecs type
                item = MarketSpecs.abi_decode_value_into_cls(state_snapshot_value)
            elif item_discriminant == ItemType.PRICE_SOURCE:
                # Leaf item is of PriceSource type
                item = PriceSource.abi_decode_value_into_cls(state_snapshot_value)
            else:
                raise Exception("Unhandled SMT leaf item type")

            # Convert the state snapshot key from hexstr to bytes
            state_snapshot_key_bytes = bytes.fromhex(state_snapshot_key[2:])

            # Update the SMT with the H256 repr of the key and the item
            self.smt.update(bytes_array_to_h256(state_snapshot_key_bytes), item)

        self.expected_epoch_id = expected_epoch_id
        self.expected_tx_ordinal = 0

    # ************** DATA GETTERS ************** #

    def get_price_leaves_for_symbol(self, symbol: str) -> List:
        """
        Get Price leaves from SMT for a given market. This is used
        internally when computing the funding rate since we need to
        obtain all the Price leaves in the state to derive the
        time-weighted average of the premium rate.

        Parameters
        ----------
        symbol : str
            Market symbol
        """

        # Derive Price leaf key prefix
        price_key_prefix = ItemType.PRICE.to_bytes(1, byteorder="little") + pack_bytes(
            symbol
        )

        return [
            (node.key, node.value)
            for node_hash, node in self.smt.store().leaves_map().items()
            if h256_to_bytes_array(node.key)[:7] == price_key_prefix
        ]

    def get_position_leaves_for_symbol(self, symbol: str) -> List:
        """
        Get Position leaves from SMT for a given market. This is used
        internally at the time of processing a FundingRate transaction
        since we need to iterate through all the open positions on the
        exchange to handle funding rate distributions.

        Parameters
        ----------
        symbol : str
            Market symbol
        """

        # Derive Position leaf key prefix
        position_key_prefix = ItemType.POSITION.to_bytes(
            1, byteorder="little"
        ) + pack_bytes(symbol)

        return [
            (node.key, node.value)
            for node_hash, node in self.smt.store().leaves_map().items()
            if h256_to_bytes_array(node.key)[:7] == position_key_prefix
        ]

    def get_stats_leaves(self) -> List:
        """
        Get Stats leaves from SMT. This is used internally at the time
        of processing a TradeMining transaction since we need to iterate
        through all the stats data for each trader to obtain the maker
        and taker volumes they contributed in order to determine their
        trade mining allocation.
        """

        stats_key_prefix = ItemType.STATS.to_bytes(1, byteorder="little")
        return [
            (node.key, node.value)
            for node_hash, node in self.smt.store().leaves_map().items()
            if h256_to_bytes_array(node.key)[:1] == stats_key_prefix
        ]

    def get_funding_rate(self, symbol: str) -> Decimal:
        """
        Get the projected funding rate for the upcoming funding
        distribution pay period for a given symbol

        Parameters
        ----------
        symbol : str
            Market symbol
        """

        # Get price leaves for symbol
        price_leaves_for_symbol = self.get_price_leaves_for_symbol(symbol)

        # Compute average premium rate across all price checkpoints
        avg_premium_rate = np.mean(
            [
                price_leaf_value.ema / price_leaf_value.index_price
                for price_key_h256, price_leaf_value in price_leaves_for_symbol
            ]
        )

        # Any values between [-0.0005, 0.0005] => 0
        unclamped_funding_rate = max(Decimal("0.0005"), avg_premium_rate) + min(
            Decimal("-0.0005"), avg_premium_rate
        )

        # Cap the funding rate bounds to [-0.005, 0.05]
        return min(Decimal("0.005"), max(Decimal("-0.005"), unclamped_funding_rate))

    def get_trader_snapshot(self, trader_identifier: TraderIdentifier) -> List[Dict]:
        """
        Get a snapshot of Trader leaves given a particular identifier.

        Parameters
        ----------
        trader_identifier : TraderIdentifier
            A topic-based string wrapper indicating the set of Trader
            leaves that should be returned
        """

        # Derive the Trader key bytes prefix given the topic
        trader_key_prefix = trader_identifier.encoded_key

        if trader_identifier.is_max_granular_key:
            # If the topic is maximally set, we have a specific leaf
            # we are querying, and can retrieve it from the SMT
            # accordingly

            # Get the Trader leaf given the H256 repr of the trader
            # key
            trader_key_h256 = trader_identifier.as_h256
            trader_leaf = self.smt.get(trader_key_h256)

            if trader_leaf == H256.zero():
                # If Trader leaf doesn't exist, return Empty leaf
                trader_leaf = Empty()

            # Return a snapshot with a single Trader leaf item
            return [{"t": trader_identifier.topic_string, "c": trader_leaf}]

        # Obtain the Trader leaves that adhere to the prefix computed
        # above
        trader_leaves = [
            (node.key, node.value)
            for node_hash, node in self.smt.store().leaves_map().items()
            if h256_to_bytes_array(node.key)[: len(trader_key_prefix)]
            == trader_key_prefix
        ]

        # Return a snapshot containing the Trader leaves obtained
        return [
            {
                "t": TraderIdentifier.decode_key_into_cls(
                    h256_to_bytes_array(trader_key_h256)
                ).topic_string,
                "c": trader_leaf,
            }
            for trader_key_h256, trader_leaf in trader_leaves
        ]

    def get_strategy_snapshot(
        self, strategy_identifier: StrategyIdentifier
    ) -> List[Dict]:
        """
        Get a snapshot of Strategy leaves given a particular identifier.

        Parameters
        ----------
        strategy_identifier : StrategyIdentifier
            A topic-based string wrapper indicating the set of Strategy
            leaves that should be returned
        """

        # Derive the Strategy key bytes prefix given the topic
        strategy_key_prefix = strategy_identifier.encoded_key

        if strategy_identifier.is_max_granular_key:
            # If the topic is maximally set, we have a specific leaf
            # we are querying, and can retrieve it from the SMT
            # accordingly

            # Get the Strategy leaf given the H256 repr of the strategy
            # key
            strategy_key_h256 = strategy_identifier.as_h256
            strategy_leaf = self.smt.get(strategy_key_h256)

            if strategy_leaf == H256.zero():
                # If Strategy leaf doesn't exist, return Empty leaf
                strategy_leaf = Empty()

            # Return a snapshot with a single Strategy leaf item
            return [{"t": strategy_identifier.topic_string, "c": strategy_leaf,}]

        # Obtain the Strategy leaves that adhere to the prefix computed
        # above
        strategy_leaves = [
            (node.key, node.value)
            for node_hash, node in self.smt.store().leaves_map().items()
            if h256_to_bytes_array(node.key)[: len(strategy_key_prefix)]
            == strategy_key_prefix
        ]

        # Return a snapshot containing the Strategy leaves obtained
        return [
            {
                "t": StrategyIdentifier.decode_key_into_cls(
                    h256_to_bytes_array(strategy_key_h256)
                ).topic_string,
                "c": strategy_leaf,
            }
            for strategy_key_h256, strategy_leaf in strategy_leaves
        ]

    def get_position_snapshot(
        self, position_identifier: PositionIdentifier
    ) -> List[Dict]:
        """
        Get a snapshot of Position leaves given a particular identifier.

        Parameters
        ----------
        position_identifier : PositionIdentifier
            A topic-based string wrapper indicating the set of Position
            leaves that should be returned
        """

        # Derive the Position key bytes prefix given the topic
        position_key_prefix = position_identifier.encoded_key

        if position_identifier.is_max_granular_key:
            # If the topic is maximally set, we have a specific leaf
            # we are querying, and can retrieve it from the SMT
            # accordingly

            # Get the Position leaf given the H256 repr of the Position
            # key
            position_key_h256 = position_identifier.as_h256
            position_leaf = self.smt.get(position_key_h256)

            if position_leaf == H256.zero():
                # If Position leaf doesn't exist, return Empty leaf
                position_leaf = Empty()

            # Return a snapshot with a single Position leaf item
            return [{"t": position_identifier.topic_string, "c": position_leaf,}]

        # Obtain the Position leaves that adhere to the prefix computed
        # above
        position_leaves = [
            (node.key, node.value)
            for node_hash, node in self.smt.store().leaves_map().items()
            if h256_to_bytes_array(node.key)[: len(position_key_prefix)]
            == position_key_prefix
        ]

        # Return a snapshot containing the Position leaves obtained
        return [
            {
                "t": PositionIdentifier.decode_key_into_cls(
                    h256_to_bytes_array(position_key_h256)
                ).topic_string,
                "c": position_leaf,
            }
            for position_key_h256, position_leaf in position_leaves
        ]

    def get_book_order_snapshot(
        self, book_order_identifier: BookOrderIdentifier
    ) -> List[Dict]:
        """
        Get a snapshot of BookOrder leaves given a particular identifier.

        Parameters
        ----------
        book_order_identifier : BookOrderIdentifier
            A topic-based string wrapper indicating the set of BookOrder
            leaves that should be returned
        """

        # Derive the BookOrder key bytes prefix given the topic
        book_order_key_prefix = book_order_identifier.encoded_key

        # Obtain the BookOrder leaves that adhere to the prefix computed
        # above
        book_order_leaves = [
            (node.key, node.value)
            for node_hash, node in self.smt.store().leaves_map().items()
            if h256_to_bytes_array(node.key)[: len(book_order_key_prefix)]
            == book_order_key_prefix
        ]

        # The BookOrder topic format is slightly different than any
        # other state entry, in that you can include selectors for
        # trader address and strategy id, both of which are not part
        # of the leaf key, but rather part of the leaf value. This makes
        # the snapshot slightly more involved.
        if book_order_identifier.abbrev_strategy_id_hash is not None:
            # Return the currently open book orders for a symbol,
            # trader address, and strategy id
            return [
                {
                    "t": BookOrderIdentifier.decode_key_into_cls(
                        h256_to_bytes_array(book_order_key_h256), book_order_leaf
                    ).topic_string,
                    "c": book_order_leaf,
                }
                for book_order_key_h256, book_order_leaf in book_order_leaves
                if book_order_leaf.trader_address
                == book_order_identifier.trader_address
                and book_order_leaf.strategy_id_hash
                == book_order_identifier.abbrev_strategy_id_hash
            ]
        elif book_order_identifier.trader_address is not None:
            # Return the currently open book orders for a symbol and
            # trader address
            return [
                {
                    "t": BookOrderIdentifier.decode_key_into_cls(
                        h256_to_bytes_array(book_order_key_h256), book_order_leaf
                    ).topic_string,
                    "c": book_order_leaf,
                }
                for book_order_key_h256, book_order_leaf in book_order_leaves
                if book_order_leaf.trader_address
                == book_order_identifier.trader_address
            ]

        # Return the currently open book orders for a symbol
        return [
            {
                "t": BookOrderIdentifier.decode_key_into_cls(
                    h256_to_bytes_array(book_order_key_h256), book_order_leaf
                ).topic_string,
                "c": book_order_leaf,
            }
            for book_order_key_h256, book_order_leaf in book_order_leaves
        ]

    # ************** WEBSOCKET FUNCTIONALITY ************** #

    async def _handle_tx_log_update_message(self, message: Dict) -> None:
        """
        Handle the transaction log message received from the Trader
        API upon subscription. This will be either the Partial (includes
        the state snapshot SMT data as of the most recent
        checkpoint and the transaction log entries from that point
        up until now) or Update messages (streaming messages of
        individual transaction log entries from this point onwards).
        These messages are parsed to get things into the same format
        such that the Auditor can be used as-is by the integration
        tests as well.

        Parameters
        ----------
        message : Dict
            Transaction log update message
        """

        def get_parsed_tx_log_entry(tx_log_entry: Dict):
            """
            Parse an individual transaction log entry into a format
            suitable for the Auditor. This format was selected so
            that the Auditor can be reused as-is by the integration
            tests with no changes needed.

            Parameters
            ----------
            tx_log_entry : Dict
                Transaction log message
            """

            return {
                "event": tx_log_entry["event"],
                "requestIndex": int(tx_log_entry["requestIndex"]),
                "epochId": int(tx_log_entry["epochId"]),
                "txOrdinal": int(tx_log_entry["ordinal"]),
                "batchId": int(tx_log_entry["batchId"]),
                "timeValue": int(tx_log_entry["timeValue"]),
                "stateRootHash": tx_log_entry["stateRootHash"],
            }

        if message["t"] == WebsocketEventType.SNAPSHOT:
            # If transaction log message is of type Snapshot, we will
            # need to process the snapshot of state leaves as of the
            # most recent checkpoint and

            # Extract the state snapshot leaves, which is the state
            # snapshot as of the most recent completed checkpoint
            # at the time of subscribing to the transaction log
            parsed_state_snapshot = message["c"]["leaves"]

            # Process the state snapshot
            self.process_state_snapshot(
                int(message["c"]["epochId"]), parsed_state_snapshot,
            )

        elif message["t"] == WebsocketEventType.HEAD:
            # If transaction log message is of type Head, we will need
            # to process the snapshot of transactions from the most
            # recent checkpoint up until now.

            # Extract the transaction log entries from the latest
            # checkpointed epoch up until now
            pre_parsed_tx_log_snapshot = message["c"]
            # If there are transactions in the log to be processed
            # in this partial response

            # Parse the transaction log entries suitable for the
            # Auditor such that it can be reused as-is by the
            # integration tests
            parsed_tx_log_entry = get_parsed_tx_log_entry(message["c"])

            if self.first_head:
                # If this is the first tx log entry of the head response

                # Initialize the current local state root hash to the SMT's root
                # hash after having loaded the state snapshot
                self.current_tx_root_hash = (
                    f"0x{h256_to_bytes_array(self.smt.root()).hex()}"
                )
                self.current_batch_tx_root_hash = self.current_tx_root_hash

                self.latest_batch_id = parsed_tx_log_entry["batchId"]

                for symbol, order_book in self.order_books.items():
                    order_book.process_book_orders_from_queue()
                self.first_head = False

            # Process the transaction log entries
            self.process_tx_log_event(parsed_tx_log_entry, True)

        elif message["t"] == WebsocketEventType.TAIL:
            if not self.tx_log_partial_loaded_event.is_set():
                self.tx_log_partial_loaded_event.set()

            # If transaction log message is of type Update

            # If there are transaction log entries to process
            parsed_tx_log_entry = get_parsed_tx_log_entry(message["c"])

            # Process the transaction log entry
            self.process_tx_log_event(parsed_tx_log_entry, False)

    async def trader_auditor_consumer_handler(self, websocket, path):
        """
        Trader <> Auditor consumer handler for messages that are received
        by the Auditor from the Trader.

        Parameters
        ----------
        websocket : WebSocketServerProtocol
            The WS connection instance between Trader and Auditor
        """

        # Loop through messages as they come in on the WebSocket
        async for message in websocket:
            # JSON-serialize the inbound message
            data = json.loads(message)

            if data["t"] == WebsocketMessageType.SUBSCRIBE:
                # If Auditor has received a Subscription message from
                # the Trader
                topic_string = data["c"]

                # Split message topic on the '/' delimiter
                msg_parts = topic_string.split("/")

                # Check if subscription is to state updates to SMT
                # leaves or to transaction log entries
                if msg_parts[0] == "STATE":
                    # Subscription is to state updates to SMT leaves

                    if msg_parts[1] == IdentifierTypes.TRADER:
                        # Subscription to Trader leaf updates

                        # Derive TraderIdentifier wrapper
                        identifier = TraderIdentifier.decode_topic_string_into_cls(
                            topic_string
                        )

                        # Retrieve a snapshot based on the topic to send
                        # as a Partial to the Trader
                        snapshot_data = self.get_trader_snapshot(identifier)
                    elif msg_parts[1] == IdentifierTypes.STRATEGY:
                        # Subscription to Strategy leaf updates

                        # Derive StrategyIdentifier wrapper
                        identifier = StrategyIdentifier.decode_topic_string_into_cls(
                            topic_string
                        )

                        # Retrieve a snapshot based on the topic to send
                        # as a Partial to the Trader
                        snapshot_data = self.get_strategy_snapshot(identifier)
                    elif msg_parts[1] == IdentifierTypes.POSITION:
                        # Subscription to Position leaf updates

                        # Derive PositionIdentifier wrapper
                        identifier = PositionIdentifier.decode_topic_string_into_cls(
                            topic_string
                        )

                        # Retrieve a snapshot based on the topic to send
                        # as a Partial to the Trader
                        snapshot_data = self.get_position_snapshot(identifier)
                    elif msg_parts[1] == IdentifierTypes.BOOK_ORDER:
                        # Subscription to BookOrder leaf updates

                        # Derive BookOrderIdentifier wrapper
                        identifier = BookOrderIdentifier.decode_topic_string_into_cls(
                            topic_string
                        )

                        # Retrieve a snapshot based on the topic to send
                        # as a Partial to the Trader
                        snapshot_data = self.get_book_order_snapshot(identifier)

                    subscription_added = self.add_subscription(websocket, identifier)

                    if self.trader_auditor_queue is not None and subscription_added:
                        # Add the Partial snapshot response to the
                        # queue to be sent to the Trader
                        await websocket.send(
                            ComplexOutputEncoder().encode(
                                ItemMessage(
                                    topic_string,
                                    WebsocketEventType.PARTIAL,
                                    snapshot_data,
                                    None,
                                )
                            )
                        )
                elif msg_parts[0] == "TX_LOG":
                    # Subscription is to transaction log entries

                    if msg_parts[1] == IdentifierTypes.PRICE_CHECKPOINT:
                        # Subscription to PriceCheckpoint transactions

                        # Derive PriceCheckpointIdentifier wrapper
                        identifier = PriceCheckpointIdentifier.decode_topic_string_into_cls(
                            topic_string
                        )
                    elif msg_parts[1] == IdentifierTypes.FILL:
                        # Subscription to Fill transactions

                        # Derive FillIdentifier wrapper
                        identifier = FillIdentifier.decode_topic_string_into_cls(
                            topic_string
                        )
                    elif msg_parts[1] == IdentifierTypes.STRATEGY_UPDATE:
                        # Subscription to StrategyUpdate transactions

                        # Derive StrategyUpdateIdentifier wrapper
                        identifier = StrategyUpdateIdentifier.decode_topic_string_into_cls(
                            topic_string
                        )
                    elif msg_parts[1] == IdentifierTypes.POST:
                        # Subscription to Post transactions

                        # Derive PostIdentifier wrapper
                        identifier = PostIdentifier.decode_topic_string_into_cls(
                            topic_string
                        )
                    elif msg_parts[1] == IdentifierTypes.CANCEL:
                        # Subscription to Cancel transactions

                        # Derive CancelIdentifier wrapper
                        identifier = CancelIdentifier.decode_topic_string_into_cls(
                            topic_string
                        )

                    # Add subscription to list of Trader's subscriptions
                    self.add_subscription(websocket, identifier)

                elif msg_parts[0] == "COMMAND_RECEIPT":
                    # Add the proposed subscription topic string to the list of
                    # existing Trader subscriptions
                    self.user_subscriptions[websocket].add("COMMAND_RECEIPT")

            if data["t"] == WebsocketMessageType.REQUEST:
                # If Auditor has received a Request message from
                # the Trader (i.e. placing orders, cancelling orders,
                # withdrawing funds), add the message to the queue
                # to send from the Auditor to the API
                self.api_auditor_queue.put_nowait(
                    WebsocketMessage.decode_value_into_cls(data)
                )

    async def trader_auditor_producer_handler(
        self, websocket: WebSocketServerProtocol, path: str
    ):
        """
        Trader <> Auditor producer handler for messages that are sent
        from the Auditor to the Trader.

        Parameters
        ----------
        websocket : WebSocketServerProtocol
            The WS connection instance between API and Auditor
        """

        while True:
            # Receive the oldest message (FIFO) in the queue and
            # send after serialization to the Trader
            message: WebsocketMessage = await self.trader_auditor_queue.get()

            for subscribed_websocket in self.user_subscriptions.keys():
                if (
                    message.message_type
                    in [
                        WebsocketMessageType.INFO,
                        WebsocketMessageType.SEQUENCED,
                        WebsocketMessageType.SAFETY_FAILURE,
                    ]
                    and "COMMAND_RECEIPT"
                    in self.user_subscriptions[subscribed_websocket]
                ):
                    # If WebSocket message type is of a type that should be
                    # simply serialized and relayed back to the Trader.
                    # Such scenarios include sequencing (success) or
                    # erroneous responses to commands (placing orders,
                    # canceling orders, withdrawing)
                    await subscribed_websocket.send(
                        ComplexOutputEncoder().encode(message)
                    )
                else:
                    try:
                        message_copy = copy.deepcopy(message)
                        # Check if any existing subscriptions cover the
                        # inbound message topic, in which case we should
                        # send this to the Trader, otherwise skip
                        subscription_topic = [
                            topic
                            for topic in self.user_subscriptions[subscribed_websocket]
                            if message_copy.message_type.startswith(topic)
                        ][0]

                        # Override the parent message topic (initially set
                        # to be the most granular level) with the
                        # subscription type (since this may be a superset
                        # of the message)
                        message_copy.message_type = subscription_topic

                        # Send serialized message to Trader
                        await subscribed_websocket.send(
                            ComplexOutputEncoder().encode(message_copy)
                        )
                    except IndexError:
                        pass

    async def api_auditor_consumer_handler(
        self, websocket: WebSocketClientProtocol, path: str
    ):
        """
        API <> Auditor consumer handler for messages that are received
        by the Auditor from the API.

        Parameters
        ----------
        websocket : WebSocketServerProtocol
            The WS connection instance between API and Auditor
        """

        async def _inner_messages(
            ws: websockets.WebSocketClientProtocol,
        ) -> AsyncIterable[str]:
            try:
                while True:
                    try:
                        msg: str = await asyncio.wait_for(ws.recv(), timeout=30.0)
                        yield msg
                    except asyncio.TimeoutError:
                        try:
                            pong_waiter = await ws.ping()
                            await asyncio.wait_for(pong_waiter, timeout=30.0)
                        except asyncio.TimeoutError:
                            raise
            except asyncio.TimeoutError:
                print("WebSocket ping timed out. Going to reconnect...")
                return
            except websockets.ConnectionClosed:
                return
            finally:
                await ws.close()

        # Loop through messages as they come in on the WebSocket
        async for message in _inner_messages(websocket):
            # JSON-serialize the inbound message
            data = json.loads(message)

            if "t" not in data:
                # Non topical data, such as rate-limiting message
                continue

            topic = data["t"]

            if topic in ["Snapshot", "Head", "Tail"]:
                # If the message is a TxLogUpdate, this is something
                # that should be processed by the Auditor

                # Handle transaction log message
                await self._handle_tx_log_update_message(data)

    async def api_auditor_producer_handler(
        self, websocket: WebSocketClientProtocol, path: str
    ):
        """
        API <> Auditor producer handler for messages that are sent
        from the Auditor to the API.

        Parameters
        ----------
        websocket : WebSocketServerProtocol
            The WS connection instance between API and Auditor
        """

        # Start things off with a subscription to the TxLogUpdate
        # channel on the API to receive a snapshot and streaming
        # updates to the transaction log
        tx_log_update_subscription = WebsocketMessage(
            "SubscribeMarket", {"events": ["TxLogUpdate"]}
        )
        self.api_auditor_queue.put_nowait(tx_log_update_subscription)

        try:
            while True:
                # Receive the oldest message (FIFO) in the queue and
                # send after serialization to the API
                message = await self.api_auditor_queue.get()
                await websocket.send(ComplexOutputEncoder().encode(message))
        except websockets.ConnectionClosed:
            print("Connection has been closed (api_auditor_producer_handler)")

    async def api_auditor_server(
        self, staging_env: str,
    ):
        """
        Sets up the DerivaDEX API <> Auditor server with consumer and
        producer tasks. The consumer is when the Auditor receives
        messages from the API, and the producer is when the Auditor
        sends messages to the API.

        Parameters
        ----------
        staging_env : str
            Staging environment (test || pre || alpha || beta)
        """

        def _generate_uri_token(staging_env: str,):
            """
            Generate URI token to connect to the API

            Parameters
            ----------
            staging_env : str
                Staging environment (test || pre || alpha || beta)
            """

            # Construct and return WS connection url with format
            return f"wss://{staging_env}.derivadex.io/node0/v2/txlog"

        while True:
            try:
                # Set up a WS context connection given a specific URI
                async with websockets.connect(
                    _generate_uri_token(staging_env), max_size=2 ** 32
                ) as websocket_client:
                    try:
                        # Set up the consumer
                        consumer_task = asyncio.ensure_future(
                            self.api_auditor_consumer_handler(websocket_client, None)
                        )

                        # Set up the producer
                        producer_task = asyncio.ensure_future(
                            self.api_auditor_producer_handler(websocket_client, None)
                        )

                        # These should essentially run forever unless one of them
                        # is stopped for some reason
                        done, pending = await asyncio.wait(
                            [consumer_task, producer_task],
                            return_when=asyncio.FIRST_COMPLETED,
                        )
                        for task in pending:
                            task.cancel()
                    finally:
                        logger.info(f"API <> Auditor server outer loop restarting")

                        # Reset Auditor state upon reconnection
                        if self.user_subscriptions:
                            await asyncio.wait(
                                [
                                    user_sub_websocket.close()
                                    for user_sub_websocket in list(
                                        self.user_subscriptions
                                    )
                                ]
                            )
                        self._reset()
                        continue

            except asyncio.CancelledError:
                raise
            except Exception as e:
                print(
                    "Unexpected error with WebSocket connection. Retrying after 30 seconds...",
                )
                await asyncio.sleep(30.0)

                # Reset Auditor state upon reconnection
                self._reset()

    async def trader_auditor_server(
        self, websocket: WebSocketServerProtocol, path: str
    ):
        """
        Sets up the Trader client <> Auditor server with consumer and
        producer tasks. The consumer is when the Auditor receives
        messages from the Trader, and the producer is when the Auditor
        sends messages to the Trader.

        Parameters
        ----------
        websocket : WebsocketProtocol
            The WS connection URI for the Auditor to connect to the
            DerivaDEX API
        """

        try:
            # Initialize subscriptions set for websocket
            self.user_subscriptions[websocket] = set()

            await self.tx_log_partial_loaded_event.wait()

            # Set up the consumer
            consumer_task = asyncio.ensure_future(
                self.trader_auditor_consumer_handler(websocket, path)
            )

            # Set up the producer
            producer_task = asyncio.ensure_future(
                self.trader_auditor_producer_handler(websocket, path)
            )

            # These should essentially run forever unless one of them
            # is stopped for some reason
            done, pending = await asyncio.wait(
                [consumer_task, producer_task], return_when=asyncio.FIRST_COMPLETED,
            )
            for task in pending:
                task.cancel()
        finally:
            # Unregister the connected trader to remove any
            # subscriptions. A connecting trader will need to
            # thus resubscribe upon reconnection.
            await self.unregister_trader(websocket)

    # ************** REST FUNCTIONALITY ************** #

    async def get_trader_rest(self, request: Request):
        """
        REST endpoint to obtain Trader leaves.

        Parameters
        ----------
        request : Request
            An HTTP request for Trader leaves
        """

        try:
            # Obtain topic string from the request
            topic = request.rel_url.query["topic"]

            # Decode the topic string into a TraderIdentifier wrapper
            trader_topic = TraderIdentifier.decode_topic_string_into_cls(topic)

            # Obtain the snapshot of Trader leaves given the topic
            contents = self.get_trader_snapshot(trader_topic)

            # Package the contents into a RestMessage
            http_response = RestMessage(RestMessageType.SUCCESS, contents)
        except Exception as e:
            http_response = RestMessage(RestMessageType.ERROR, str(e))

        # Serialize and return the response
        return web.json_response(http_response, dumps=ComplexOutputEncoder().encode)

    async def get_strategy_rest(self, request: Request):
        """
        REST endpoint to obtain Strategy leaves.

        Parameters
        ----------
        request : Request
            An HTTP request for Strategy leaves
        """

        # Obtain topic string from the request
        topic = request.rel_url.query["topic"]

        try:
            # Decode the topic string into a StrategyIdentifier wrapper
            strategy_topic = StrategyIdentifier.decode_topic_string_into_cls(topic)

            # Obtain the snapshot of Strategy leaves given the topic
            contents = self.get_strategy_snapshot(strategy_topic)

            # Package the contents into a RestMessage
            http_response = RestMessage(RestMessageType.SUCCESS, contents)
        except Exception as e:
            http_response = RestMessage(RestMessageType.ERROR, str(e))

        # Serialize and return the response
        return web.json_response(http_response, dumps=ComplexOutputEncoder().encode)

    async def get_position_rest(self, request):
        """
        REST endpoint to obtain Position leaves.

        Parameters
        ----------
        request : Request
            An HTTP request for Position leaves
        """

        # Obtain topic string from the request
        topic = request.rel_url.query["topic"]

        try:
            # Decode the topic string into a PositionIdentifier wrapper
            position_topic = PositionIdentifier.decode_topic_string_into_cls(topic)

            # Obtain the snapshot of Strategy leaves given the topic
            contents = self.get_position_snapshot(position_topic)

            # Package the contents into a RestMessage
            http_response = RestMessage(RestMessageType.SUCCESS, contents)
        except Exception as e:
            http_response = RestMessage(RestMessageType.ERROR, str(e))

        # Serialize and return the response
        return web.json_response(http_response, dumps=ComplexOutputEncoder().encode)

    async def get_book_orders_rest(self, request):
        """
        REST endpoint to obtain BookOrder leaves.

        Parameters
        ----------
        request : Request
            An HTTP request for BookOrder leaves
        """

        # Obtain topic string from the request
        topic = request.rel_url.query["topic"]

        try:
            # Decode the topic string into a BookOrderIdentifier wrapper
            book_order_topic = BookOrderIdentifier.decode_topic_string_into_cls(topic)

            # Obtain the snapshot of Strategy leaves given the topic
            contents = self.get_book_order_snapshot(book_order_topic)

            # Package the contents into a RestMessage
            http_response = RestMessage(RestMessageType.SUCCESS, contents)
        except Exception as e:
            http_response = RestMessage(RestMessageType.ERROR, str(e))

        # Serialize and return the response
        return web.json_response(http_response, dumps=ComplexOutputEncoder().encode)

    async def trader_http_server(self):
        """
        HTTP server available between Auditor <> Trader client.
        """

        # Set up HTTP web app
        app = web.Application()

        # Set up REST endpoints / routes
        app.add_routes([web.get("/trader", self.get_trader_rest)])
        app.add_routes([web.get("/strategy", self.get_strategy_rest)])
        app.add_routes([web.get("/position", self.get_position_rest)])
        app.add_routes([web.get("/book_order", self.get_book_orders_rest)])

        # Run in non-blocking / async fashion
        runner = web.AppRunner(app)
        await runner.setup()
        site = web.TCPSite(runner, "0.0.0.0", 8766)
        await site.start()

        while True:
            await asyncio.sleep(3600)  # sleep forever

    # ************** ASYNCIO ENTRYPOINT ************** #

    async def main(self, staging_env: str):
        """
        Main entry point for the Auditor. It sets up the various
        coroutines to run on the event loop - the trader <> auditor
        WS server, the API <> auditor WS server, and a REST / HTTP
        server.

        Parameters
        ----------
        staging_env : str
            Staging environment (test || pre || alpha || beta)
        """

        # Initialize parameters inside event loop
        self._reset()

        trader_auditor_server = websockets.serve(
            self.trader_auditor_server, "0.0.0.0", 8765
        )

        await asyncio.gather(
            trader_auditor_server,
            self.api_auditor_server(staging_env),
            self.trader_http_server(),
        )


if __name__ == "__main__":
    # Load config file to run AuditorDriver
    p = configargparse.ArgParser()
    p.add_argument(
        "-c",
        "--config",
        required=True,
        is_config_file_arg=True,
        help="config file path",
    )
    p.add_argument(
        "-e",
        "--staging-env",
        required=True,
        help="staging environment (test || pre || alpha || beta)",
    )

    options = p.parse_args()

    print(options)
    print("----------")
    print(p.format_help())
    print("----------")
    print(p.format_values())

    auditor_driver = AuditorDriver(options.staging_env)

    asyncio.run(auditor_driver.main(options.staging_env))
