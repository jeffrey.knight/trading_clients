from trading_clients.auditor.shared.identifier import Identifier
from trading_clients.auditor.shared.identifier_types import IdentifierTypes
from trading_clients.auditor.utils import is_valid_none_list


class CancelIdentifier(Identifier):
    def __init__(self, symbol: str):
        if not is_valid_none_list([symbol]):
            raise Exception("Invalid CancelIdentifier generation")

        self.symbol = symbol

    def encompasses_identifier(self, identifier):
        if type(identifier).__name__ != "CancelIdentifier":
            return False
        if self.symbol is not None:
            return self.symbol == identifier.symbol
        return True

    @property
    def topic_string(self):
        return f"{'/'.join(filter(None, ['TX_LOG', IdentifierTypes.CANCEL, self.symbol]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a CancelIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.CANCEL:
            raise Exception(f"Invalid topic: must be of {IdentifierTypes.CANCEL} type")

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        return cls(safe_list_get(topic_parts, 2, None),)

    @classmethod
    def decode_tx_into_cls(cls, tx):
        """
        Decode a Post transaction into a CancelIdentifier.

        Parameters
        ----------
        tx : Cancel
            Cancel transaction
        """

        return cls(tx.symbol)

    @property
    def is_max_granular(self):
        return self.symbol is not None
