from trading_clients.auditor.shared.identifier import Identifier
from trading_clients.auditor.shared.identifier_types import IdentifierTypes
from trading_clients.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from trading_clients.auditor.state.strategy import Strategy
from trading_clients.auditor.utils import is_valid_none_list


class FillIdentifier(Identifier):
    def __init__(self, symbol: str, trader_address: str, abbrev_strategy_id_hash: str):
        if not is_valid_none_list([symbol, trader_address, abbrev_strategy_id_hash]):
            raise Exception("Invalid FillIdentifier generation")

        self.symbol = symbol
        self.trader_address = trader_address
        self.abbrev_strategy_id_hash = abbrev_strategy_id_hash

    def encompasses_identifier(self, identifier):
        if type(identifier).__name__ != "FillIdentifier":
            return False
        if self.abbrev_strategy_id_hash is not None:
            return (
                self.symbol == identifier.symbol
                and self.trader_address == identifier.trader_address
                and self.abbrev_strategy_id_hash == identifier.abbrev_strategy_id_hash
            )
        elif self.trader_address is not None:
            return (
                self.symbol == identifier.symbol
                and self.trader_address == identifier.trader_address
            )
        elif self.symbol is not None:
            return self.symbol == identifier.symbol
        return True

    @property
    def topic_string(self):
        return f"{'/'.join(filter(None, ['TX_LOG', IdentifierTypes.FILL, self.symbol, self.trader_address, self.abbrev_strategy_id_hash]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a FillIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.FILL:
            raise Exception(f"Invalid topic: must be of {IdentifierTypes.FILL} type")

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        trader_address = safe_list_get(topic_parts, 3, None)
        return cls(
            safe_list_get(topic_parts, 2, None),
            trader_address.lower()
            if trader_address is not None
            else trader_address,
            safe_list_get(topic_parts, 4, None),
        )

    @classmethod
    def decode_tx_into_cls(cls, tx):
        """
        Decode a fill tx into FillIdentifier.

        Parameters
        ----------
        tx : Fill
            Fill transaction
        """

        if tx.reason == "Trade":
            return (
                cls(
                    tx.symbol,
                    tx.maker_outcome.trader_address,
                    f"{StrategyIdentifier.generate_strategy_id_hash(tx.maker_outcome.strategy_id).hex()}",
                ),
                cls(
                    tx.symbol,
                    tx.taker_outcome.trader_address,
                    f"{StrategyIdentifier.generate_strategy_id_hash(tx.taker_outcome.strategy_id).hex()}",
                ),
            )
        else:
            return cls(
                tx.symbol,
                tx.maker_outcome.trader_address,
                f"{StrategyIdentifier.generate_strategy_id_hash(tx.maker_outcome.strategy_id).hex()}",
            )

    @property
    def is_max_granular(self):
        return (
            self.symbol is not None
            and self.trader_address is not None
            and self.abbrev_strategy_id_hash is not None
        )
