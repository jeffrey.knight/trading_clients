"""
LiquidatedPosition module
"""

from decimal import Decimal
from typing import Dict, List

from trading_clients.auditor.transactions.adl_outcome import AdlOutcome
from trading_clients.auditor.transactions.cancel import Cancel
from trading_clients.auditor.transactions.event import Event
from trading_clients.auditor.transactions.event_types import EventType
from trading_clients.auditor.transactions.fill import Fill
from trading_clients.auditor.transactions.i128 import I128
from trading_clients.auditor.transactions.liquidation_fill import LiquidationFill
from trading_clients.auditor.transactions.price_checkpoint import PriceCheckpoint


class LiquidatedPosition(Event):
    """
    Defines a LiquidatedPosition
    """

    def __init__(
        self,
        price_checkpoint: PriceCheckpoint,
        amount: Decimal,
        fills: List[LiquidationFill],
        canceled_orders: List[Cancel],
        bankruptcy_price: Decimal,
        adl_outcomes: List[AdlOutcome],
        liquidation_spread: Decimal,
        request_index: int,
    ):
        """
        Initialize a LiquidatedPosition transaction. A LiquidatedPosition
        has data pertaining to a liquidated position.

        Parameters
        ----------
        price_checkpoint : PriceCheckpoint
            The PriceCheckpoint triggering the liquidation
        amount : Decimal
            Liquidated balance amount
        fills : List[LiquidationFill]
            Liquidation fills that took over position
        canceled_orders: List[Cancel]
            Canceled maker orders as a result of the liquidation
        bankruptcy_price : Decimal
            Position's bankruptcy price
        adl_outcomes : List[AdlOutcome]
            Positions that were ADL'd as a result of the liquidation
        liquidation_spread : Decimal
            Insurance fund capitalization liquidation spread due to liquidation
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.price_checkpoint = price_checkpoint
        self.amount = amount
        self.fills = fills
        self.canceled_orders = canceled_orders
        self.bankruptcy_price = bankruptcy_price
        self.adl_outcomes = adl_outcomes
        self.liquidation_spread = liquidation_spread

    def repr_json(self):
        return {
            "eventType": EventType.LIQUIDATION,
            "requestIndex": self.request_index,
            "price_checkpoint": self.price_checkpoint,
            "amount": str(self.amount),
            "fills": self.fills,
            "canceledOrders": self.canceled_orders,
            "bankruptcyPrice": str(self.bankruptcy_price),
            "adlOutcomes": self.adl_outcomes,
            "liquidationSpread": self.liquidation_spread,
        }

    def __repr__(self):
        return f"LiquidatedPosition (event)"
