"""
Trader Update module
"""
import asyncio
from decimal import Decimal
from typing import Dict

from trading_clients.auditor.smt.h256 import H256
from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.identifiers.trader_identifier import TraderIdentifier
from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.state.trader import Trader
from trading_clients.auditor.transactions.event import Event
from trading_clients.auditor.transactions.event_message import EventMessage
from trading_clients.auditor.transactions.event_types import EventType
from trading_clients.auditor.transactions.identifiers.trader_update_identifier import (
    TraderUpdateIdentifier,
)
from trading_clients.auditor.utils import bytes_array_to_h256
from trading_clients.client.asyncio_utils import place_message_in_queue
from trading_clients.client.websocket_message import WebsocketEventType


class TraderUpdate(Event):
    """
    Defines a Trader Update
    """

    def __init__(
        self,
        trader_address: str,
        amount: Decimal,
        update_type: int,
        tx_hash: str,
        request_index: int,
    ):
        """
        Initialize a TraderUpdate transaction. A TraderUpdate is an
        update to a trader's DDX account (such as depositing or withdrawing
        DDX).

        Parameters
        ----------
        trader : str
           Trader's Ethereum address this strategy belongs to
        amount: Decimal
           The amount of collateral deposited or withdrawn
        update_type: int
           Update type (Deposit=0, Withdraw=1)
        tx_hash: str
           The Ethereum transaction's hash
        """

        self.trader_address = trader_address
        self.amount = amount
        self.update_type = update_type
        self.tx_hash = tx_hash
        self.request_index = request_index

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a StrategyUpdate
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        trader_update_tx_event = raw_tx_log_event["event"]["c"]

        def update_type_to_int(update_type: str):
            """
            Convert 'Deposit' and 'Withdraw' to 0 and 1 integer values.

            Parameters
            ----------
            update_type : str
                Either 'Deposit' or 'Withdraw'
            """

            return 0 if update_type == "Deposit" else 1

        return cls(
            trader_update_tx_event["trader"],
            Decimal(trader_update_tx_event["amount"]),
            update_type_to_int(trader_update_tx_event["updateType"]),
            trader_update_tx_event["txHash"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a StrategyUpdate transaction. A StrategyUpdate consists
        of information relating to updates for a trader's strategy, such
        as when their free or frozen collateral has changed due to a
        deposit or withdrawal. This will update the Strategy leaf in the
        SMT.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Place the TraderUpdate event message on the queue to
        # consider sending to the Trader
        trader_update_identifier = TraderUpdateIdentifier.decode_tx_into_cls(self)
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            EventMessage(
                trader_update_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": trader_update_identifier.topic_string, "c": self,},
            ),
        )

        # Construct a TraderIdentifier and corresponding encoded
        # key
        trader_identifier = TraderIdentifier(self.trader_address)
        trader_key = trader_identifier.encoded_key
        trader_key_h256 = bytes_array_to_h256(trader_key)

        # Get the Trader leaf given the key above
        trader_leaf = smt.get(trader_key_h256)

        if self.update_type == 0:
            # If TraderUpdate is of deposit type

            if trader_leaf == H256.zero():
                # Initialize a new Trader Leaf
                trader_leaf = Trader(
                    Decimal("0"),
                    Decimal("0"),
                    "0x0000000000000000000000000000000000000000",
                )

            trader_leaf.free_ddx_balance += self.amount

            # Update the SMT with the H256 repr of the key and the
            # Trader leaf
            smt.update(trader_key_h256, trader_leaf)

            # Place the Trader leaf update message on the queue to
            # consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    trader_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {"t": trader_identifier.topic_string, "c": trader_leaf,},
                    self,
                ),
            )
        else:
            # If TraderUpdate is of withdrawal type
            if (
                trader_leaf == H256.zero()
                or isinstance(trader_leaf, Empty)
                or trader_leaf.frozen_ddx_balance < self.amount
            ):
                raise Exception(
                    "Trader leaf either non-existent or insufficiently capitalized to facilitate withdrawal"
                )

            # Adjust the existing Trader leaf by decrementing the
            # free collateral by the amount in the withdrawal event
            trader_leaf.frozen_ddx_balance -= self.amount

            # Update the SMT with the H256 repr of the key and the
            # Trader leaf
            smt.update(trader_key_h256, trader_leaf)

            # Place the Trader leaf update message on the queue to
            # consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    trader_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {"t": trader_identifier.topic_string, "c": trader_leaf},
                    self,
                ),
            )

    def repr_json(self):
        return {
            "eventType": EventType.TRADER_UPDATE,
            "requestIndex": self.request_index,
            "traderAddress": self.trader_address,
            "amount": str(self.amount),
            "updateType": self.update_type,
            "txHash": self.tx_hash,
        }

    def __repr__(self):
        return (
            f"Trader Update (event): "
            f"request_index = {self.request_index}; "
            f"trader_address = {self.trader_address}; "
            f"amount: {self.amount}; "
            f"update_type: {self.update_type}; "
            f"tx_hash: {self.tx_hash}"
        )
