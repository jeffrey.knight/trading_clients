"""
AdlOutcome module
"""

from decimal import Decimal
from typing import Dict, List

from trading_clients.auditor.transactions.event import Event
from trading_clients.auditor.transactions.event_types import EventType
from trading_clients.auditor.transactions.fill import Fill


class AdlOutcome(Event):
    """
    Defines an AdlOutcome
    """

    def __init__(
        self,
        trader_address: str,
        strategy_id: str,
        collateral_amount: Decimal,
        new_collateral: Decimal,
        position_amount: Decimal,
        new_position_balance: Decimal,
        request_index: int,
    ):
        """
        Initialize an AdlOutcome. An AdlOutcome represents a scenario
        where a strategy has been auto-deleveraged due to a liquidation.

        Parameters
        ----------
        trader_address : str
            Auto-deleveraged trader's ethereum address
        strategy_id : str
            Auto-deleveraged strategy ID
        collateral_amount : Decimal
            Collateral amount credited to strategy as a result of being
            profitable at the time of being auto-deleveraged
        new_collateral : Decimal
            New collateral amount for strategy
        position_amount : Decimal
            Position size auto-deleveraged
        new_position_balance : Decimal
            New position balance after being auto-deleveraged
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.trader_address = trader_address
        self.strategy_id = strategy_id
        self.collateral_amount = collateral_amount
        self.new_collateral = new_collateral
        self.position_amount = position_amount
        self.new_position_balance = new_position_balance

    def repr_json(self):
        return {
            "eventType": EventType.LIQUIDATION,
            "requestIndex": self.request_index,
            "traderAddress": self.trader_address,
            "strategyId": self.strategy_id,
            "collateralAmount": str(self.collateral_amount),
            "newCollateral": str(self.new_collateral),
            "positionAmount": str(self.position_amount),
            "newPositionBalance": str(self.new_position_balance),
        }

    def __repr__(self):
        return f"AdlOutcome (event)"
