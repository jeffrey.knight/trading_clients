"""
EpochMarker module
"""

from typing import Dict

from trading_clients.auditor.transactions.event import Event


class EpochMarker(Event):
    """
    Defines an EpochMarker
    """

    def __init__(
        self, kind: str, state_root_hash: str, request_index: int,
    ):
        """
        Initialize an EpochMarker non-transitioning transaction.

        Parameters
        ----------
        kind : str
           Type of EpochMarker (Genesis | AdvanceEpoch)
        state_root_hash : str
           State root hash at time of marker
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.state_root_hash = state_root_hash
        self.kind = kind

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        raise NotImplementedError
