"""
AdvanceEpoch module
"""
import asyncio
from typing import Dict

from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.transactions.epoch_marker import EpochMarker
from trading_clients.auditor.transactions.event_types import EventType


class AdvanceEpoch(EpochMarker):
    """
    Defines an AdvanceEpoch
    """

    def __init__(
        self, kind: str, state_root_hash: str, new_epoch_id: int, request_index: int,
    ):
        """
        Initialize an AdvanceEpoch non-transitioning transaction.

        Parameters
        ----------
        kind : str
           Type of EpochMarker (Genesis | AdvanceEpoch)
        state_root_hash : str
           State root hash at time of marker
        new_epoch_id : int
           New epoch ID after epoch marker
        request_index : int
            Sequenced request index of transaction
        """

        super().__init__(kind, state_root_hash, request_index)
        self.new_epoch_id = new_epoch_id

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into an AdvanceEpoch
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        advance_epoch_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            advance_epoch_tx_event["kind"],
            advance_epoch_tx_event["stateRootHash"],
            advance_epoch_tx_event["newEpochId"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process an EpochMarker transaction of type AdvanceEpoch. This
        indicates the a new epoch in the transaction log, although
        it is not state-transitioning in the way typical transactions
        are.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Set the expected epoch ID to be the new epoch ID and the
        # expected tx ordinal to be -1, because we immediately increment
        # this by 1, thus setting it to 0, which will be the first
        # tx ordinal of the next epoch
        kwargs["expected_epoch_id"](kwargs["auditor_instance"], self.new_epoch_id)
        kwargs["expected_tx_ordinal"](kwargs["auditor_instance"], -1)

    def repr_json(self):
        return {
            "eventType": EventType.EPOCH_MARKER,
            "requestIndex": self.request_index,
            "kind": self.kind,
            "stateRootHash": self.state_root_hash,
            "newEpochId": self.new_epoch_id,
        }

    def __repr__(self):
        return f"AdvanceEpoch (event): request_index = {self.request_index}; kind: {self.kind}; state_root_hash: {self.state_root_hash}; new_epoch_id: {self.new_epoch_id}"
