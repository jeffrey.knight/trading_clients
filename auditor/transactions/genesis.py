"""
Genesis module
"""
import asyncio
from typing import Dict

from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.identifiers.organic_insurance_fund_identifier import (
    OrganicInsuranceFundIdentifier,
)
from trading_clients.auditor.state.organic_insurance_fund import OrganicInsuranceFund
from trading_clients.auditor.transactions.epoch_marker import EpochMarker
from trading_clients.auditor.transactions.event_types import EventType
from trading_clients.auditor.utils import bytes_array_to_h256


class Genesis(EpochMarker):
    """
    Defines a Genesis
    """

    def __init__(
        self, kind: str, state_root_hash: str, request_index: int,
    ):
        """
        Initialize a Genesis non-transitioning transaction.

        Parameters
        ----------
        kind : str
           Type of EpochMarker (Genesis | AdvanceEpoch)
        state_root_hash : str
           State root hash at time of marker
        request_index : int
            Sequenced request index of transaction
        """

        super().__init__(kind, state_root_hash, request_index)

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a Genesis
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        genesis_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            genesis_tx_event["kind"],
            genesis_tx_event["stateRootHash"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process an EpochMarker transaction of type Genesis. This
        indicates the very first event in the transaction log, although
        it is not state-transitioning in the way typical transactions
        are.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        organic_insurance_fund = OrganicInsuranceFund({})
        organic_insurance_fund_identifier = OrganicInsuranceFundIdentifier()
        organic_insurance_fund_key = organic_insurance_fund_identifier.encoded_key
        smt.update(
            bytes_array_to_h256(organic_insurance_fund_key), organic_insurance_fund
        )

        # Set the expected epoch ID to be 1 and the expected tx ordinal
        # to be -1, because we immediately increment this by 1, thus
        # setting it to 0, which will be the first tx ordinal of the
        # next epoch
        kwargs["expected_epoch_id"](kwargs["auditor_instance"], 1)
        kwargs["expected_tx_ordinal"](kwargs["auditor_instance"], -1)

    def repr_json(self):
        return {
            "eventType": EventType.EPOCH_MARKER,
            "requestIndex": self.request_index,
            "kind": self.kind,
            "stateRootHash": self.state_root_hash,
        }

    def __repr__(self):
        return f"Genesis (event): request_index = {self.request_index}; kind: {self.kind}; state_root_hash: {self.state_root_hash}"
