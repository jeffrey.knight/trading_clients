"""
ItemType module
"""

from enum import Enum


class EventType(int, Enum):
    """
    Defines an event type
    """

    PARTIAL_FILL = 0
    COMPLETE_FILL = 1
    POST_ORDER = 2
    CANCEL = 3
    LIQUIDATION = 4
    STRATEGY_UPDATE = 5
    TRADER_UPDATE = 6
    WITHDRAW = 7
    WITHDRAW_DDX = 8
    PRICE_CHECKPOINT = 9
    PNL_SETTLEMENT = 10
    FUNDING = 11
    TRADE_MINING = 12
    NO_TRANSITION = 13

    FILL = 14
    POST = 15

    MEMBERSHIP_CHANGE = 60
    EPOCH_MARKER = 100
