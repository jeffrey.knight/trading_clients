"""
Outcome module
"""

from decimal import Decimal


class Outcome:
    """
    Defines a Outcome
    """

    def __init__(
        self,
        trader_address: str,
        strategy_id: str,
        fee: Decimal,
        ddx_fee_election: bool,
        realized_pnl: Decimal,
        new_collateral: Decimal,
        new_position_avg_entry_price: Decimal,
        new_position_balance: Decimal,
        position_side: str,
    ):
        """
        Initialize an Outcome (a part of the Fill
        transaction). This holds information including the realized PNL,
        fees paid, the strategy ID and trader address, and whether fees
        are being paid in DDX or not (i.e. USDC).

        Parameters
        ----------
        trader_address : str
           Trader's Ethereum address
        strategy_id: str
           Strategy ID this fill belongs to for the specified trader
        fee: Decimal
           Fees paid for filled trade
        ddx_fee_election: bool
           Whether fees are being paid in DDX (True) or USDC (False)
        realized_pnl: Decimal
           Realized PNL as a result of filled trade
        new_collateral: Decimal
           New free collateral amount as a result of filled trade
        new_position_avg_entry_price: Decimal
           New average entry price for position as a result of filled
           trade
        new_position_balance: Decimal
           New position balance as a result of filled trade
        position_side: str
           New position side as a result of filled trade
        """

        self.trader_address = trader_address
        self.strategy_id = strategy_id
        self.fee = fee
        self.ddx_fee_election = ddx_fee_election
        self.realized_pnl = realized_pnl
        self.new_collateral = new_collateral
        self.new_position_avg_entry_price = new_position_avg_entry_price
        self.new_position_balance = new_position_balance
        self.position_side = position_side

    def repr_json(self):
        return {
            "traderAddress": self.trader_address,
            "strategyId": self.strategy_id,
            "fee": str(self.fee),
            "ddxFeeElection": self.ddx_fee_election,
            "realizedPnl": str(self.realized_pnl),
            "newCollateral": str(self.new_collateral),
            "newPositionAvgEntryPrice": str(self.new_position_avg_entry_price),
            "newPositionBalance": str(self.new_position_balance),
            "positionSide": self.position_side,
        }

    def __str__(self):
        return f"Outcome (event): trader = {self.trader_address}; strategy = {self.strategy_id}; fee: {self.fee}; ddx_fee_election: {self.ddx_fee_election}; realized_pnl: {self.realized_pnl}"
