"""
Liquidation module
"""
import asyncio
from decimal import Decimal
from typing import Dict, List

from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from trading_clients.auditor.state.identifiers.organic_insurance_fund_identifier import (
    OrganicInsuranceFundIdentifier,
)
from trading_clients.auditor.state.identifiers.position_identifier import (
    PositionIdentifier,
)
from trading_clients.auditor.state.identifiers.price_identifier import PriceIdentifier
from trading_clients.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.state.organic_insurance_fund import OrganicInsuranceFund
from trading_clients.auditor.state.position import Position
from trading_clients.auditor.state.price import Price
from trading_clients.auditor.state.strategy import Strategy
from trading_clients.auditor.transactions.adl_outcome import AdlOutcome
from trading_clients.auditor.transactions.cancel import Cancel
from trading_clients.auditor.transactions.event import Event
from trading_clients.auditor.transactions.i128 import I128
from trading_clients.auditor.transactions.liquidated_position import LiquidatedPosition
from trading_clients.auditor.transactions.liquidation_entry import LiquidationEntry
from trading_clients.auditor.transactions.liquidation_fill import LiquidationFill
from trading_clients.auditor.transactions.outcome import Outcome
from trading_clients.auditor.transactions.price_checkpoint import PriceCheckpoint
from trading_clients.auditor.utils import bytes_array_to_h256
from trading_clients.client.asyncio_utils import place_message_in_queue
from trading_clients.client.websocket_message import WebsocketEventType


class Liquidation(Event):
    """
    Defines an Liquidation
    """

    def __init__(
        self, liquidation_entries: List[LiquidationEntry], request_index: int,
    ):
        """
        Initialize a Liquidation transaction. A Liquidation contains
        a list of LiquidationEntry objects.

        Parameters
        ----------
        liquidation_entries : List[LiquidationEntry]
            A list of LiquidationEntry objects.
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.liquidation_entries = liquidation_entries

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a Liquidation
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        liquidation_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            [
                LiquidationEntry(
                    liquidation_entry["traderAddress"],
                    liquidation_entry["strategyId"],
                    [
                        Cancel(
                            canceled_order["symbol"],
                            canceled_order["orderHash"],
                            Decimal(canceled_order["amount"]),
                            raw_tx_log_event["requestIndex"],
                        )
                        for canceled_order in liquidation_entry["canceledOrders"]
                    ],
                    {
                        liquidated_position_key: LiquidatedPosition(
                            PriceCheckpoint(
                                liquidated_position_key,
                                Decimal(liquidated_position_val["price"]["ema"]),
                                liquidated_position_val["price"]["indexPriceHash"],
                                Decimal(liquidated_position_val["price"]["indexPrice"]),
                                liquidated_position_val["price"]["ordinal"],
                                raw_tx_log_event["requestIndex"],
                            ),
                            Decimal(liquidated_position_val["amount"]),
                            [
                                LiquidationFill(
                                    liquidated_position_fill["reason"],
                                    liquidated_position_fill["symbol"],
                                    liquidated_position_fill["indexPriceHash"],
                                    liquidated_position_fill["makerOrderHash"],
                                    Decimal(
                                        liquidated_position_fill[
                                            "makerOrderRemainingAmount"
                                        ]
                                    ),
                                    Decimal(liquidated_position_fill["amount"]),
                                    Decimal(liquidated_position_fill["price"]),
                                    liquidated_position_fill["takerSide"],
                                    Outcome(
                                        liquidated_position_fill["makerOutcome"][
                                            "trader"
                                        ],
                                        liquidated_position_fill["makerOutcome"][
                                            "strategy"
                                        ],
                                        Decimal(
                                            liquidated_position_fill["makerOutcome"][
                                                "fee"
                                            ]
                                        ),
                                        liquidated_position_fill["makerOutcome"][
                                            "ddxFeeElection"
                                        ],
                                        Decimal(
                                            liquidated_position_fill["makerOutcome"][
                                                "realizedPnl"
                                            ]
                                        ),
                                        Decimal(
                                            liquidated_position_fill["makerOutcome"][
                                                "newCollateral"
                                            ]
                                        ),
                                        Decimal(
                                            liquidated_position_fill["makerOutcome"][
                                                "newPositionAvgEntryPrice"
                                            ]
                                        ),
                                        Decimal(
                                            liquidated_position_fill["makerOutcome"][
                                                "newPositionBalance"
                                            ]
                                        ),
                                        liquidated_position_fill["makerOutcome"][
                                            "positionSide"
                                        ],
                                    ),
                                    raw_tx_log_event["timeValue"],
                                    raw_tx_log_event["requestIndex"],
                                )
                                for liquidated_position_fill in liquidated_position_val[
                                    "fills"
                                ]
                            ],
                            [
                                Cancel(
                                    canceled_order["symbol"],
                                    canceled_order["orderHash"],
                                    Decimal(canceled_order["amount"]),
                                    raw_tx_log_event["requestIndex"],
                                )
                                for canceled_order in liquidated_position_val["cancels"]
                            ],
                            Decimal(liquidated_position_val["bankruptcyPrice"]),
                            [
                                AdlOutcome(
                                    adl_outcome["traderAddress"],
                                    adl_outcome["strategyId"],
                                    Decimal(adl_outcome["collateralAmount"]),
                                    Decimal(adl_outcome["newCollateral"]),
                                    Decimal(adl_outcome["positionAmount"]),
                                    Decimal(adl_outcome["newPositionBalance"]),
                                    raw_tx_log_event["requestIndex"],
                                )
                                for adl_outcome in liquidated_position_val[
                                    "adlOutcomes"
                                ]
                            ],
                            Decimal(liquidated_position_val["liquidationSpread"]),
                            raw_tx_log_event["requestIndex"],
                        )
                        for liquidated_position_key, liquidated_position_val in liquidation_entry[
                            "positions"
                        ].items()
                    },
                    raw_tx_log_event["requestIndex"],
                )
                for liquidation_entry in liquidation_tx_event
            ],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a Liquidation transaction. A Liquidation is when a
        trader's account is under-collateralized and forcibly closed.
        Their collateral is removed and positions are closed, either
        against the order book with other market participants, or if
        the insurance fund is insufficiently-capitalized, ADL'd vs
        winning traders. A liquidated trader's open orders are canceled
        and the insurance fund is adjusted, along with the relevant
        Stats leaves for the maker traders taking on the liquidated
        position.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Liquidation transactions
        """

        # Loop through each liquidation entry and process them individually
        for liquidation_entry in self.liquidation_entries:
            # Construct Strategy identifier for the one getting
            # liquidated
            liquidated_strategy_identifier = StrategyIdentifier(
                liquidation_entry.trader_address,
                f"{StrategyIdentifier.generate_strategy_id_hash(liquidation_entry.strategy_id).hex()}",
            )

            # Derive the encoded Strategy key and H256 repr
            liquidated_strategy_key = liquidated_strategy_identifier.encoded_key
            liquidated_strategy_key_h256 = bytes_array_to_h256(liquidated_strategy_key)

            # Essentially zero out the Strategy leaf's free collateral
            # by emptying it out (not the same as the Empty leaf, which
            # you will find on some occasions, meaning we don't remove
            # the leaf entirely)
            liquidated_strategy: Strategy = smt.get(liquidated_strategy_key_h256)
            liquidated_strategy.free_collateral = {}
            smt.update(liquidated_strategy_key_h256, liquidated_strategy)

            # Place the Strategy leaf update message on the queue to
            # consider sending to the Trader
            trader_auditor_queue.put_nowait(
                ItemMessage(
                    liquidated_strategy_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {
                        "t": liquidated_strategy_identifier.topic_string,
                        "c": liquidated_strategy,
                    },
                    liquidation_entry,
                )
            )

            # Loop through the canceled orders to remove them from the
            # SMT
            for cancel_tx in liquidation_entry.canceled_orders:
                cancel_tx.process_tx(
                    smt, trader_auditor_queue, suppress_trader_queue, **kwargs
                )

            # Loop through the positions of the liquidated strategy
            for symbol, liquidated_position in liquidation_entry.positions.items():
                # Construct Position identifier for the liquidated
                # position
                liquidated_position_identifier = PositionIdentifier(
                    symbol,
                    liquidation_entry.trader_address,
                    f"{StrategyIdentifier.generate_strategy_id_hash(liquidation_entry.strategy_id).hex()}",
                )

                # Derive the encoded Position key and H256 repr
                liquidated_position_key = liquidated_position_identifier.encoded_key
                liquidated_position_key_h256 = bytes_array_to_h256(
                    liquidated_position_key
                )

                # Remove Position from the SMT
                smt.update(liquidated_position_key_h256, Empty())

                # Place the Position leaf update message on the
                # queue to consider sending to the Trader
                place_message_in_queue(
                    trader_auditor_queue,
                    suppress_trader_queue,
                    ItemMessage(
                        liquidated_position_identifier.topic_string,
                        WebsocketEventType.UPDATE,
                        {
                            "t": liquidated_position_identifier.topic_string,
                            "c": Empty(),
                        },
                        liquidated_position,
                    ),
                )

                # Loop through each cancel event and process them individually
                for canceled_order in liquidated_position.canceled_orders:
                    canceled_order.process_tx(
                        smt, trader_auditor_queue, suppress_trader_queue, **kwargs
                    )

                # Loop through the resulting liquidation fills that
                # allowed the exchange to close the Position
                for liquidation_fill in liquidated_position.fills:
                    liquidation_fill.process_tx(
                        smt, trader_auditor_queue, suppress_trader_queue, **kwargs
                    )

                for adl_outcome in liquidated_position.adl_outcomes:
                    # Construct Strategy identifier for the ADL'd
                    # Strategy
                    adl_strategy_identifier = StrategyIdentifier(
                        adl_outcome.trader_address,
                        f"{StrategyIdentifier.generate_strategy_id_hash(adl_outcome.strategy_id).hex()}",
                    )

                    # Derive the encoded Strategy key and H256 repr
                    adl_strategy_key = adl_strategy_identifier.encoded_key
                    adl_strategy_key_h256 = bytes_array_to_h256(adl_strategy_key)
                    adl_strategy_leaf: Strategy = smt.get(adl_strategy_key_h256)

                    # Adjust ADL'd Strategy's free collateral in the SMT
                    if adl_outcome.new_collateral == Decimal("0"):
                        adl_strategy_leaf.free_collateral = {}
                    else:
                        adl_strategy_leaf.free_collateral[
                            kwargs["usdc_address"]
                        ] = adl_outcome.new_collateral

                    smt.update(adl_strategy_key_h256, adl_strategy_leaf)

                    # Place the Strategy leaf update message on the queue to
                    # consider sending to the Trader
                    place_message_in_queue(
                        trader_auditor_queue,
                        suppress_trader_queue,
                        ItemMessage(
                            adl_strategy_identifier.topic_string,
                            WebsocketEventType.UPDATE,
                            {
                                "t": adl_strategy_identifier.topic_string,
                                "c": adl_strategy_leaf,
                            },
                            adl_outcome,
                        ),
                    )

                    # Construct Position identifier for the ADL'd
                    # Position
                    adl_position_identifier = PositionIdentifier(
                        symbol,
                        adl_outcome.trader_address,
                        f"{StrategyIdentifier.generate_strategy_id_hash(adl_outcome.strategy_id).hex()}",
                    )

                    # Derive the encoded Position key and H256 repr
                    adl_position_key = adl_position_identifier.encoded_key
                    adl_position_key_h256 = bytes_array_to_h256(adl_position_key)
                    adl_position_leaf: Position = smt.get(adl_position_key_h256)

                    # Adjust ADL'd Position in the SMT
                    if adl_outcome.new_position_balance == Decimal("0"):
                        smt.update(adl_position_key_h256, Empty())
                    else:
                        adl_position_leaf.balance = adl_outcome.new_position_balance
                        smt.update(adl_position_key_h256, adl_position_leaf)

                    # Place the Position leaf update message on the queue to
                    # consider sending to the Trader
                    place_message_in_queue(
                        trader_auditor_queue,
                        suppress_trader_queue,
                        ItemMessage(
                            adl_position_identifier.topic_string,
                            WebsocketEventType.UPDATE,
                            {
                                "t": adl_position_identifier.topic_string,
                                "c": adl_position_leaf,
                            },
                            adl_outcome,
                        ),
                    )

                # Construct OrganicInsuranceFund identifier
                organic_insurance_fund_identifier = OrganicInsuranceFundIdentifier()

                # Derive the OrganicInsuranceFund encoded key and H256
                # repr
                organic_insurance_fund_identifier_key = (
                    organic_insurance_fund_identifier.encoded_key
                )
                organic_insurance_fund_identifier_h256 = bytes_array_to_h256(
                    organic_insurance_fund_identifier_key
                )
                organic_insurance_fund_leaf: OrganicInsuranceFund = smt.get(
                    organic_insurance_fund_identifier_h256
                )

                # Apply liquidation spread
                if (
                    kwargs["usdc_address"]
                    not in organic_insurance_fund_leaf.capitalization
                ):
                    # If there is no entry in the capitalization
                    # mapping for this token address, add it
                    # and initialize with the delta
                    organic_insurance_fund_leaf.capitalization[
                        kwargs["usdc_address"]
                    ] = liquidated_position.liquidation_spread
                else:
                    # If there is already an entry in the
                    # capitalization mapping for this token address,
                    # credit/debit it by the liquidation spread
                    organic_insurance_fund_leaf.capitalization[
                        kwargs["usdc_address"]
                    ] += liquidated_position.liquidation_spread

                # Remove any zero-value keys
                if organic_insurance_fund_leaf.capitalization[
                    kwargs["usdc_address"]
                ] == Decimal("0"):
                    del organic_insurance_fund_leaf.capitalization[
                        kwargs["usdc_address"]
                    ]

                smt.update(
                    organic_insurance_fund_identifier_h256, organic_insurance_fund_leaf
                )

                # Place the OrganicInsuranceFund leaf update message on the queue to
                # consider sending to the Trader
                place_message_in_queue(
                    trader_auditor_queue,
                    suppress_trader_queue,
                    ItemMessage(
                        organic_insurance_fund_identifier.topic_string,
                        WebsocketEventType.UPDATE,
                        {
                            "t": organic_insurance_fund_identifier.topic_string,
                            "c": organic_insurance_fund_leaf,
                        },
                        liquidated_position,
                    ),
                )

                # Construct Price identifier
                price_identifier = PriceIdentifier(
                    symbol, liquidated_position.price_checkpoint.index_price_hash
                )

                # Derive the OrganicInsuranceFund encoded key and H256
                # repr
                price_key = price_identifier.encoded_key
                price_key_h256 = bytes_array_to_h256(price_key)
                price_leaf = Price(
                    liquidated_position.price_checkpoint.index_price,
                    liquidated_position.price_checkpoint.ema,
                    liquidated_position.price_checkpoint.ordinal,
                )

                # Add the new Price leaf to the SMT
                smt.update(price_key_h256, price_leaf)

                # Place the Price leaf update message on the queue to
                # consider sending to the Trader
                place_message_in_queue(
                    trader_auditor_queue,
                    suppress_trader_queue,
                    ItemMessage(
                        price_identifier.topic_string,
                        WebsocketEventType.UPDATE,
                        {"t": price_identifier.topic_string, "c": price_leaf},
                        liquidated_position,
                    ),
                )

                # Update latest price leaves abstraction with the new
                # price checkpoint data
                kwargs["latest_price_leaves"][symbol] = price_leaf

    def __repr__(self):
        return f"Liquidation (event)"
