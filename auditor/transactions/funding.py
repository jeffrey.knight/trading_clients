"""
Funding module
"""
import asyncio
from typing import Dict
from decimal import Decimal

from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.identifiers.price_identifier import PriceIdentifier
from trading_clients.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.transactions.event import Event
from trading_clients.auditor.transactions.event_types import EventType
from trading_clients.auditor.utils import (
    round_to_unit,
    h256_to_bytes_array,
    bytes_array_to_h256,
)
from trading_clients.client.asyncio_utils import place_message_in_queue
from trading_clients.client.websocket_message import WebsocketEventType


class Funding(Event):
    """
    Defines a Funding
    """

    def __init__(
        self,
        settlement_epoch_id: int,
        funding_rates: Dict[str, Decimal],
        time_value: int,
        request_index: int,
    ):
        """
        Initialize a Funding transaction. A Funding is
        when a there is a funding rate distribution.

        Parameters
        ----------
        settlement_epoch_id : int
           The epoch id for the funding event.
        funding_rates: Dict[str, Decimal]
           The funding information (the absolute value and whether
           positive/negative) for each market.
        time_value: int
           ??
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.settlement_epoch_id = settlement_epoch_id
        self.funding_rates = funding_rates
        self.time_value = time_value

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a Funding
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        funding_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            funding_tx_event["settlementEpochId"],
            {k: Decimal(v) for k, v in funding_tx_event["fundingRates"].items()},
            raw_tx_log_event["timeValue"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a Funding transaction. A Funding event consists of
        consists of information relating to funding rate-related
        distributions. All open positions will result in traders
        either paying or receiving a USDC debit/credit to their
        free collateral as a function of the funding rate (given the
        Price leaves in the SMT at this time) and their
        position notional (given the latest mark price).

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Loop through the funding rate symbols and values as specified
        # in the transaction
        for funding_rate_symbol, funding_rate_value in self.funding_rates.items():
            funding_rate = kwargs["get_funding_rate"](funding_rate_symbol)

            if round_to_unit(funding_rate_value) != round_to_unit(funding_rate):
                # Ensure funding rate given price checkpoints matches
                # what transaction log says the funding rate is
                raise Exception(
                    f"funding rate tx log ({funding_rate_value} != funding_rate ({funding_rate})"
                )

            if funding_rate != Decimal("0"):
                # If funding rate is non-zero, handle funding payments

                # Get position leaves for symbol
                position_leaves_for_symbol = kwargs["get_position_leaves_for_symbol"](
                    funding_rate_symbol
                )

                # Obtain latest mark price
                mark_price = kwargs["latest_price_leaves"][
                    funding_rate_symbol
                ].mark_price

                # Loop through each open position
                for (
                    position_key_h256,
                    position_leaf_value,
                ) in position_leaves_for_symbol:
                    # Compute the funding payment for the trader. When
                    # the funding rate is positive, long traders will
                    # pay and short traders will receive payments. When
                    # the funding rate is negative, long traders will
                    # receive payments and short traders will pay.
                    funding_delta = (
                        (
                            Decimal("-1.0")
                            if position_leaf_value.side == "Long"
                            else Decimal("1.0")
                        )
                        * funding_rate
                        * position_leaf_value.balance
                        * mark_price
                    )

                    # Construct a StrategyIdentifier and corresponding encoded
                    # key
                    strategy_identifier = StrategyIdentifier.decode_position_key_into_cls(
                        h256_to_bytes_array(position_key_h256)
                    )
                    strategy_key = strategy_identifier.encoded_key
                    strategy_key_h256 = bytes_array_to_h256(strategy_key)

                    # Get the Strategy leaf given the key from above
                    strategy_leaf = smt.get(strategy_key_h256)

                    # Credit/debit the trader's Strategy leaf by the
                    # funding delta from above
                    strategy_leaf.free_collateral[
                        kwargs["usdc_address"]
                    ] = round_to_unit(
                        strategy_leaf.free_collateral[kwargs["usdc_address"]]
                        + funding_delta
                    )

                    # Update the SMT with the H256 repr of the key and
                    # the Strategy leaf
                    smt.update(strategy_key_h256, strategy_leaf)

                    # Place the Strategy leaf update message on the
                    # queue to consider sending to the Trader
                    place_message_in_queue(
                        trader_auditor_queue,
                        suppress_trader_queue,
                        ItemMessage(
                            strategy_identifier.topic_string,
                            WebsocketEventType.UPDATE,
                            {
                                "t": strategy_identifier.topic_string,
                                "c": strategy_leaf,
                            },
                            self,
                        ),
                    )

            price_leaves_for_symbol = kwargs["get_price_leaves_for_symbol"](
                funding_rate_symbol
            )
            for price_key_h256, price_leaf_value in price_leaves_for_symbol:
                smt.update(price_key_h256, Empty())

                # Derive StatsIdentifier given the key
                price_identifier = PriceIdentifier.decode_key_into_cls(
                    h256_to_bytes_array(price_key_h256)
                )
                # Place the Price leaf update message on the
                # queue to consider sending to the Trader
                place_message_in_queue(
                    trader_auditor_queue,
                    suppress_trader_queue,
                    ItemMessage(
                        price_identifier.topic_string,
                        WebsocketEventType.UPDATE,
                        {"t": price_identifier.topic_string, "c": Empty()},
                        self,
                    ),
                )

    def repr_json(self):
        return {
            "eventType": EventType.FUNDING,
            "requestIndex": self.request_index,
            "settlementEpochId": self.settlement_epoch_id,
            "fundingRates": self.funding_rates,
            "timeValue": self.time_value,
        }

    def __repr__(self):
        return f"Funding (event): request_index = {self.request_index}; settlement_epoch_id = {self.settlement_epoch_id}; funding_rates = {self.funding_rates}; time_value = {self.time_value}"
