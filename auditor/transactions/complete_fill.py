"""
CompleteFill module
"""
import asyncio
from decimal import Decimal
from typing import Dict, List

from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.transactions.cancel import Cancel
from trading_clients.auditor.transactions.event import Event
from trading_clients.auditor.transactions.outcome import Outcome
from trading_clients.auditor.transactions.trade_fill import TradeFill


class CompleteFill(Event):
    """
    Defines an CompleteFill
    """

    def __init__(
        self, fills: List[TradeFill], canceled_orders: List[Cancel], request_index: int,
    ):
        """
        Initialize a CompleteFill transaction. A CompleteFill is a scenario
        where the taker order has been completely filled across 1 or more
        maker orders along with any canceled maker orders.

        Parameters
        ----------
        fills : List[TradeFill]
            A list of Fill objects.
        canceled_orders: List[Cancel]
            Canceled maker orders as a result of the filled order
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.fills = fills
        self.canceled_orders = canceled_orders

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a CompleteFill
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        fills_tx_event = raw_tx_log_event["event"]["c"][0]
        cancel_orders_tx_event = raw_tx_log_event["event"]["c"][1]

        return cls(
            [
                TradeFill(
                    fill["reason"],
                    fill["symbol"],
                    fill["takerOrderHash"],
                    fill["makerOrderHash"],
                    Decimal(fill["makerOrderRemainingAmount"]),
                    Decimal(fill["amount"]),
                    Decimal(fill["price"]),
                    fill["takerSide"],
                    Outcome(
                        fill["makerOutcome"]["trader"],
                        fill["makerOutcome"]["strategy"],
                        Decimal(fill["makerOutcome"]["fee"]),
                        fill["makerOutcome"]["ddxFeeElection"],
                        Decimal(fill["makerOutcome"]["realizedPnl"]),
                        Decimal(fill["makerOutcome"]["newCollateral"]),
                        Decimal(fill["makerOutcome"]["newPositionAvgEntryPrice"]),
                        Decimal(fill["makerOutcome"]["newPositionBalance"]),
                        fill["makerOutcome"]["positionSide"],
                    ),
                    Outcome(
                        fill["takerOutcome"]["trader"],
                        fill["takerOutcome"]["strategy"],
                        Decimal(fill["takerOutcome"]["fee"]),
                        fill["takerOutcome"]["ddxFeeElection"],
                        Decimal(fill["takerOutcome"]["realizedPnl"]),
                        Decimal(fill["takerOutcome"]["newCollateral"]),
                        Decimal(fill["takerOutcome"]["newPositionAvgEntryPrice"]),
                        Decimal(fill["takerOutcome"]["newPositionBalance"]),
                        fill["takerOutcome"]["positionSide"],
                    ),
                    raw_tx_log_event["timeValue"],
                    raw_tx_log_event["requestIndex"],
                )
                for fill in fills_tx_event
            ],
            [
                Cancel(
                    canceled_order["symbol"],
                    canceled_order["orderHash"],
                    Decimal(canceled_order["amount"]),
                    raw_tx_log_event["requestIndex"],
                )
                for canceled_order in cancel_orders_tx_event
            ],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a CompleteFill transaction. A CompleteFill consists of
        Fill objects, which will adjust the maker BookOrder leaf in the
        SMT, while also adjusting the Strategy, Position, and Trader
        leaves corresponding to both the maker and the taker.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to CompleteFill transactions
        """

        # Loop through each cancel event and process them individually
        for canceled_order in self.canceled_orders:
            canceled_order.process_tx(
                smt, trader_auditor_queue, suppress_trader_queue, **kwargs
            )

        # Loop through each fill event and process them individually
        for fill in self.fills:
            fill.process_tx(smt, trader_auditor_queue, suppress_trader_queue, **kwargs)

    def __repr__(self):
        return f"CompleteFill (event): fills = {self.fills}, canceled_orders = {self.canceled_orders}"
