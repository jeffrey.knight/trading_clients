"""
Strategy Update module
"""
import asyncio
from decimal import Decimal
from typing import Dict

from trading_clients.auditor.smt.h256 import H256
from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from trading_clients.auditor.state.identifiers.trader_identifier import TraderIdentifier
from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.state.strategy import Strategy
from trading_clients.auditor.state.trader import Trader
from trading_clients.auditor.transactions.event import Event
from trading_clients.auditor.transactions.event_message import EventMessage
from trading_clients.auditor.transactions.event_types import EventType
from trading_clients.auditor.transactions.identifiers.strategy_update_identifier import (
    StrategyUpdateIdentifier,
)
from trading_clients.auditor.utils import bytes_array_to_h256
from trading_clients.client.asyncio_utils import place_message_in_queue
from trading_clients.client.websocket_message import WebsocketEventType


class StrategyUpdate(Event):
    """
    Defines a Strategy Update
    """

    def __init__(
        self,
        trader_address: str,
        collateral_address: str,
        strategy_id: str,
        amount: Decimal,
        update_type: int,
        tx_hash: str,
        request_index: int,
    ):
        """
        Initialize a StrategyUpdate transaction. A StrategyUpdate is an
        update to a trader's strategy (such as depositing or withdrawing
        collateral).

        Parameters
        ----------
        trader_address : str
           Trader's Ethereum address this strategy belongs to
        collateral_address: str
           Collateral's Ethereum address a deposit/withdrawal has been
           made with
        strategy_id: str
           Strategy ID for the given trader this event belongs to
        amount: Decimal
           The amount of collateral deposited or withdrawn
        update_type: int
           Update type (Deposit=0, Withdraw=1)
        tx_hash: str
           The Ethereum transaction's hash
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.trader_address = trader_address
        self.collateral_address = collateral_address
        self.strategy_id = strategy_id
        self.amount = amount
        self.update_type = update_type
        self.tx_hash = tx_hash

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a StrategyUpdate
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        strategy_update_tx_event = raw_tx_log_event["event"]["c"]

        def update_type_to_int(update_type: str):
            """
            Convert 'Deposit' and 'Withdraw' to 0 and 1 integer values.

            Parameters
            ----------
            update_type : str
                Either 'Deposit' or 'Withdraw'
            """

            return 0 if update_type == "Deposit" else 1

        return cls(
            strategy_update_tx_event["trader"],
            strategy_update_tx_event["collateralAddress"],
            strategy_update_tx_event["strategyId"],
            Decimal(strategy_update_tx_event["amount"]),
            update_type_to_int(strategy_update_tx_event["updateType"]),
            strategy_update_tx_event["txHash"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a StrategyUpdate transaction. A StrategyUpdate consists
        of information relating to updates for a trader's strategy, such
        as when their free or frozen collateral has changed due to a
        deposit or withdrawal. This will update the Strategy leaf in the
        SMT.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Place the StrategyUpdate event message on the queue to
        # consider sending to the Trader
        strategy_update_identifier = StrategyUpdateIdentifier.decode_tx_into_cls(self)
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            EventMessage(
                strategy_update_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": strategy_update_identifier.topic_string, "c": self,},
            ),
        )

        # Construct a StrategyIdentifier and corresponding encoded
        # key
        strategy_identifier = StrategyIdentifier(
            self.trader_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(self.strategy_id).hex()}",
        )
        strategy_key = strategy_identifier.encoded_key
        strategy_key_h256 = bytes_array_to_h256(strategy_key)

        # Get the Strategy leaf given the key above
        strategy_leaf = smt.get(strategy_key_h256)
        if self.update_type == 0:
            # If StrategyUpdate is of deposit type

            if strategy_leaf == H256.zero():
                # If we haven't yet seen the Strategy leaf, create a new
                # one
                strategy_leaf = Strategy(
                    self.strategy_id,
                    {self.collateral_address: self.amount},
                    {},
                    20,
                    False,
                )

                # Construct a TraderIdentifier and corresponding encoded
                # key
                trader_identifier = TraderIdentifier(self.trader_address)
                trader_key = trader_identifier.encoded_key
                trader_key_h256 = bytes_array_to_h256(trader_key)

                # Get the Trader leaf given the key above
                trader_leaf = smt.get(strategy_key_h256)
                if trader_leaf == H256.zero():
                    # Initialize a new Trader Leaf
                    trader_leaf = Trader(
                        Decimal("0"),
                        Decimal("0"),
                        "0x0000000000000000000000000000000000000000",
                    )

                    # Update the SMT with the H256 repr of the key and the
                    # Trader leaf
                    smt.update(trader_key_h256, trader_leaf)

                    # Place the Trader leaf update message on the queue to
                    # consider sending to the Trader
                    place_message_in_queue(
                        trader_auditor_queue,
                        suppress_trader_queue,
                        ItemMessage(
                            trader_identifier.topic_string,
                            WebsocketEventType.UPDATE,
                            {"t": trader_identifier.topic_string, "c": trader_leaf},
                            self,
                        ),
                    )
            else:
                # Adjust the existing strategy leaf by incrementing the
                # free collateral by the amount in the deposit event
                if self.collateral_address not in strategy_leaf.free_collateral:
                    strategy_leaf.free_collateral[self.collateral_address] = self.amount
                else:
                    strategy_leaf.free_collateral[
                        self.collateral_address
                    ] += self.amount

            # Update the SMT with the H256 repr of the key and the
            # Strategy leaf
            smt.update(strategy_key_h256, strategy_leaf)

            # Place the Strategy leaf update message on the queue to
            # consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    strategy_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {"t": strategy_identifier.topic_string, "c": strategy_leaf,},
                    self,
                ),
            )
        else:
            # If StrategyUpdate is of withdrawal type
            if (
                strategy_leaf == H256.zero()
                or isinstance(strategy_leaf, Empty)
                or strategy_leaf.frozen_collateral[self.collateral_address]
                < self.amount
            ):
                raise Exception(
                    "Strategy leaf either non-existent or insufficiently capitalized to facilitate withdrawal"
                )

            # Adjust the existing strategy leaf by decrementing the
            # free collateral by the amount in the withdrawal event
            strategy_leaf.frozen_collateral[self.collateral_address] -= self.amount
            if strategy_leaf.frozen_collateral[self.collateral_address] == Decimal("0"):
                del strategy_leaf.frozen_collateral[self.collateral_address]

            # Update the SMT with the H256 repr of the key and the
            # Strategy leaf
            smt.update(strategy_key_h256, strategy_leaf)

            # Place the Strategy leaf update message on the queue to
            # consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    strategy_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {"t": strategy_identifier.topic_string, "c": strategy_leaf},
                    self,
                ),
            )

    def repr_json(self):
        return {
            "eventType": EventType.STRATEGY_UPDATE,
            "requestIndex": self.request_index,
            "traderAddress": self.trader_address,
            "collateralAddress": self.collateral_address,
            "strategyId": self.strategy_id,
            "amount": str(self.amount),
            "updateType": self.update_type,
            "txHash": self.tx_hash,
        }

    def __repr__(self):
        return (
            f"Strategy Update (event): "
            f"request_index = {self.request_index}; "
            f"trader_address = {self.trader_address}; "
            f"collateral_address = {self.collateral_address}; "
            f"strategy_id: {self.strategy_id}; "
            f"amount: {self.amount}; "
            f"update_type: {self.update_type}; "
            f"tx_hash: {self.tx_hash}"
        )
