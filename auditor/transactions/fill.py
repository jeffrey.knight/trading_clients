"""
Fill module
"""
import asyncio
from decimal import Decimal
from typing import Dict

from trading_clients.auditor.smt.h256 import H256
from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.identifiers.organic_insurance_fund_identifier import (
    OrganicInsuranceFundIdentifier,
)
from trading_clients.auditor.state.identifiers.position_identifier import (
    PositionIdentifier,
)
from trading_clients.auditor.state.identifiers.stats_identifier import StatsIdentifier
from trading_clients.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.state.organic_insurance_fund import OrganicInsuranceFund
from trading_clients.auditor.state.position import Position
from trading_clients.auditor.state.stats import Stats
from trading_clients.auditor.transactions.event import Event
from trading_clients.auditor.transactions.outcome import Outcome
from trading_clients.auditor.utils import bytes_array_to_h256
from trading_clients.client.asyncio_utils import place_message_in_queue
from trading_clients.client.websocket_message import WebsocketEventType


class Fill(Event):
    """
    Defines a Fill
    """

    def __init__(
        self,
        reason: str,
        symbol: str,
        maker_order_hash: str,
        maker_order_remaining_amount: Decimal,
        amount: Decimal,
        price: Decimal,
        taker_side: str,
        maker_outcome: Outcome,
        request_index: int,
    ):
        self.request_index = request_index
        self.reason = reason
        self.symbol = symbol
        self.maker_order_hash = maker_order_hash
        self.maker_order_remaining_amount = maker_order_remaining_amount
        self.amount = amount
        self.price = price
        self.taker_side = taker_side
        self.maker_outcome = maker_outcome

    @property
    def taker_is_bid(self):
        return self.taker_side == "Bid"

    def adjust_for_maker_taker(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        maker_book_order_time_value: int,
        is_maker: bool,
        usdc_address: str,
    ):
        """
        Make some adjustments to the SMT based on whether we are
        considering the maker or the taker component of the Trade.
        In this method, we will be adjusting the the Strategy,
        Position, Trader, and Stats leaves.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        maker_book_order_time_value: int
            Maker book order time value
        is_maker : bool
            Whether outcome is for the maker or taker
        usdc_address : int
            USDC ERC-20 token contract address
        """

        if not is_maker and self.reason != "Trade":
            raise Exception("No taker for Liquidation")

        # Determine outcome based on whether we are considering maker or
        # taker
        outcome = self.maker_outcome if is_maker else self.taker_outcome

        # Construct a PositionIdentifier and corresponding encoded
        # key
        position_identifier = PositionIdentifier(
            self.symbol,
            outcome.trader_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(outcome.strategy_id).hex()}",
        )
        position_key = position_identifier.encoded_key
        position_key_h256 = bytes_array_to_h256(position_key)

        # Get the Position leaf using the key from above
        position_leaf = smt.get(position_key_h256)

        if position_leaf == H256.zero():
            # If Position leaf doesn't exist in the tree, we need
            # to create/add a new one
            position_leaf = Position(outcome.position_side, self.amount, self.price)

            # Update the SMT with the H256 repr of the key and the
            # Position leaf
            smt.update(position_key_h256, position_leaf)

            # Place the Position leaf update message on the queue to
            # consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    position_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {"t": position_identifier.topic_string, "c": position_leaf,},
                    self,
                ),
            )
        else:
            # If Position leaf exists in the tree, we need
            # to modify the existing one
            if outcome.position_side == "None":
                # If we have closed the position, update the SMT
                # with the H256 repr of the key and the Empty
                # leaf, effectively removing it from the SMT
                smt.update(position_key_h256, Empty())

                # Place the Position leaf update message on the
                # queue to consider sending to the Trader
                place_message_in_queue(
                    trader_auditor_queue,
                    suppress_trader_queue,
                    ItemMessage(
                        position_identifier.topic_string,
                        WebsocketEventType.UPDATE,
                        {"t": position_identifier.topic_string, "c": Empty(),},
                        self,
                    ),
                )
            else:
                # Adjust the Position leaf's side, average entry
                # price, and balance
                position_leaf.side = outcome.position_side
                position_leaf.avg_entry_price = outcome.new_position_avg_entry_price
                position_leaf.balance = outcome.new_position_balance

                # Update the SMT with the H256 repr of the key and
                # the Position leaf
                smt.update(position_key_h256, position_leaf)

                # Place the Position leaf update message on the
                # queue to consider sending to the Trader
                place_message_in_queue(
                    trader_auditor_queue,
                    suppress_trader_queue,
                    ItemMessage(
                        position_identifier.topic_string,
                        WebsocketEventType.UPDATE,
                        {"t": position_identifier.topic_string, "c": position_leaf,},
                        self,
                    ),
                )

        # Construct a StrategyIdentifier and corresponding encoded
        # key
        strategy_identifier = StrategyIdentifier(
            outcome.trader_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(outcome.strategy_id).hex()}",
        )
        strategy_key = strategy_identifier.encoded_key
        strategy_key_h256 = bytes_array_to_h256(strategy_key)

        # Get the Strategy leaf given the key from above
        strategy_leaf = smt.get(strategy_key_h256)
        if not outcome.ddx_fee_election:
            # If we are not paying with DDX, modify the strategy's
            # free collateral to the transaction log entry's new
            # collateral value (which will include adjustments made
            # due to realized pnl and fees)
            strategy_leaf.free_collateral[usdc_address] = outcome.new_collateral
        else:
            raise Exception("DDX fees not supported yet")

        # Update the SMT with the H256 repr of the key and the
        # Strategy leaf
        smt.update(strategy_key_h256, strategy_leaf)

        # Place the Strategy leaf update message on the queue to
        # consider sending to the Trader
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            ItemMessage(
                strategy_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": strategy_identifier.topic_string, "c": strategy_leaf,},
                self,
            ),
        )

        if self.time_value > maker_book_order_time_value + 1:
            # Construct a StatsIdentifier and corresponding encoded
            # key
            stats_identifier = StatsIdentifier(outcome.trader_address)
            stats_key = stats_identifier.encoded_key
            stats_key_h256 = bytes_array_to_h256(stats_key)

            # Get the Stats leaf given the key from above
            stats_leaf = smt.get(stats_key_h256)
            if stats_leaf == H256.zero():
                # If Stats leaf doesn't exist in the tree, we need
                # to create/add a new one
                if is_maker:
                    # Initialize the trader's maker volume
                    stats_leaf = Stats(self.amount * self.price, Decimal("0"))
                else:
                    # Initialize the trader's taker volume
                    stats_leaf = Stats(Decimal("0"), self.amount * self.price)
            else:
                # If Stats leaf does exist, we update the existing leaf
                if is_maker:
                    # Increment the trader's maker volume
                    stats_leaf.maker_volume += self.amount * self.price
                else:
                    # Increment the trader's taker volume
                    stats_leaf.taker_volume += self.amount * self.price

            # Update the SMT with the H256 repr of the key and the
            # Stats leaf
            smt.update(stats_key_h256, stats_leaf)

            # Place the Stats leaf update message on the queue to
            # consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    stats_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {"t": stats_identifier.topic_string, "c": stats_leaf},
                    self,
                ),
            )

        if not is_maker:
            if not outcome.ddx_fee_election:
                # This is fairly opinionated to the initialized
                # system parameters in that it assumes that fees are
                # only paid by takers (i.e. maker fee sched = 0%)

                # Construct OrganicInsuranceFund identifier
                organic_insurance_fund_identifier = OrganicInsuranceFundIdentifier()

                # Derive the OrganicInsuranceFund encoded key and H256
                # repr
                organic_insurance_fund_identifier_key = (
                    organic_insurance_fund_identifier.encoded_key
                )
                organic_insurance_fund_identifier_h256 = bytes_array_to_h256(
                    organic_insurance_fund_identifier_key
                )
                organic_insurance_fund_leaf: OrganicInsuranceFund = smt.get(
                    organic_insurance_fund_identifier_h256
                )

                if usdc_address not in organic_insurance_fund_leaf.capitalization:
                    # If there is no entry in the capitalization
                    # mapping for this token address, add it
                    # and initialize with the delta
                    organic_insurance_fund_leaf.capitalization[
                        usdc_address
                    ] = outcome.fee
                else:
                    # If there is already an entry in the
                    # capitalization mapping for this token address,
                    # credit/debit it by the delta
                    organic_insurance_fund_leaf.capitalization[
                        usdc_address
                    ] += outcome.fee

                smt.update(
                    organic_insurance_fund_identifier_h256, organic_insurance_fund_leaf
                )

                # Place the OrganicInsuranceFund leaf update message on the queue to
                # consider sending to the Trader
                place_message_in_queue(
                    trader_auditor_queue,
                    suppress_trader_queue,
                    ItemMessage(
                        organic_insurance_fund_identifier.topic_string,
                        WebsocketEventType.UPDATE,
                        {
                            "t": organic_insurance_fund_identifier.topic_string,
                            "c": organic_insurance_fund_leaf,
                        },
                        self,
                    ),
                )

    @classmethod
    def decode_event_trigger_into_cls(cls, event_trigger: Dict):
        raise NotImplementedError

    def repr_json(self):
        raise NotImplementedError
