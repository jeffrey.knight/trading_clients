"""
Withdraw module
"""
import asyncio
from decimal import Decimal
from typing import Dict

from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.state.strategy import Strategy
from trading_clients.auditor.transactions.event import Event
from trading_clients.auditor.transactions.event_types import EventType
from trading_clients.auditor.utils import bytes_array_to_h256
from trading_clients.client.asyncio_utils import place_message_in_queue
from trading_clients.client.websocket_message import WebsocketEventType


class Withdraw(Event):
    """
    Defines a Withdraw Update
    """

    def __init__(
        self,
        signer_address: str,
        trader_address: str,
        strategy_id: str,
        collateral_address: str,
        amount: Decimal,
        request_index: int,
    ):
        """
        Initialize a Withdraw transaction. A Withdraw is when
        a withdrawal of collateral is signaled).

        Parameters
        ----------
        signer_address : str
           Signer's Ethereum address withdrawal is taking place from
        trader_address: str
           Trader address DDX is being withdrawn to
        amount: Decimal
           The amount of DDX being withdrawn
        strategy_id: str
           Cross-margined strategy ID for which this withdrawal applies
        collateral_address: str
           Collateral ERC-20 token address being withdrawn
        request_index : int
           Sequenced request index of transaction
        """

        self.request_index = request_index
        self.signer_address = signer_address
        self.trader_address = trader_address
        self.strategy_id = strategy_id
        self.collateral_address = collateral_address
        self.amount = amount

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a Withdraw
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        withdraw_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            withdraw_tx_event["signerAddress"],
            withdraw_tx_event["traderAddress"],
            withdraw_tx_event["strategy"],
            withdraw_tx_event["currency"],
            Decimal(withdraw_tx_event["amount"]),
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a Withdraw transaction. A Withdraw event consists
        of consists of information relating to withdrawal of collateral.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Construct a StrategyIdentifier for the withdrawal signer (the
        # address funds are being withdrawn from) and corresponding
        # encoded key
        signer_identifier = StrategyIdentifier(
            self.signer_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(self.strategy_id).hex()}",
        )
        signer_key = signer_identifier.encoded_key
        signer_key_h256 = bytes_array_to_h256(signer_key)

        # Get the Trader leaf given the key above
        signer_leaf: Strategy = smt.get(signer_key_h256)

        # Decrement the free balance by the withdrawn amount
        signer_leaf.free_collateral[self.collateral_address] -= self.amount
        if signer_leaf.free_collateral[self.collateral_address] == Decimal("0"):
            del signer_leaf.free_collateral[self.collateral_address]

        # Update the SMT with the H256 repr of the key and
        # the Strategy leaf for the signer
        smt.update(signer_key_h256, signer_leaf)

        # Place the Signer Strategy leaf update message on the queue to
        # consider sending to the Trader
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            ItemMessage(
                signer_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": signer_identifier.topic_string, "c": signer_leaf},
                self,
            ),
        )

        # Construct a StrategyIdentifier for the withdrawal signer (the
        # address funds are being withdrawn from) and corresponding
        # encoded key
        trader_identifier = StrategyIdentifier(
            self.trader_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(self.strategy_id).hex()}",
        )
        trader_key = trader_identifier.encoded_key
        trader_key_h256 = bytes_array_to_h256(trader_key)

        # Get the Trader leaf given the key above
        trader_leaf: Strategy = smt.get(trader_key_h256)

        # Increment the frozen balance by the withdrawn amount
        if self.collateral_address not in trader_leaf.frozen_collateral:
            trader_leaf.frozen_collateral[self.collateral_address] = self.amount
        else:
            trader_leaf.frozen_collateral[self.collateral_address] += self.amount

        # Update the SMT with the H256 repr of the key and
        # the Strategy leaf for the trader
        smt.update(trader_key_h256, trader_leaf)

        # Place the Trader Strategy leaf update message on the queue to
        # consider sending to the Trader
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            ItemMessage(
                trader_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": trader_identifier.topic_string, "c": signer_leaf},
                self,
            ),
        )

    def repr_json(self):
        return {
            "eventType": EventType.WITHDRAW,
            "requestIndex": self.request_index,
            "signerAddress": self.signer_address,
            "traderAddress": self.trader_address,
            "strategyId": self.strategy_id,
            "collateralAddress": self.collateral_address,
            "amount": str(self.amount),
        }

    def __repr__(self):
        return (
            f"Withdraw(event): "
            f"request_index = {self.request_index}; "
            f"signer_address = {self.signer_address}; "
            f"trader_address = {self.trader_address}; "
            f"strategy_id = {self.strategy_id}; "
            f"collateral_address = {self.collateral_address}; "
            f"amount: {self.amount};"
        )
