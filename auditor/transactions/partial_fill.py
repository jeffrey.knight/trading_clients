"""
PartialFill module
"""
import asyncio
from typing import Dict, List
from decimal import Decimal

from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.transactions.cancel import Cancel
from trading_clients.auditor.transactions.event import Event
from trading_clients.auditor.transactions.post import Post
from trading_clients.auditor.transactions.outcome import Outcome
from trading_clients.auditor.transactions.trade_fill import TradeFill


class PartialFill(Event):
    """
    Defines an PartialFill
    """

    def __init__(
        self,
        fills: List[TradeFill],
        post: Post,
        canceled_orders: List[Cancel],
        request_index: int,
    ):
        """
        Initialize a PartialFill transaction. A PartialFill is a scenario
        where the taker order has been partially filled across 1 or more
        maker orders and thus has a remaining order that enters the book
        along with any canceled maker orders.

        Parameters
        ----------
        fills : List[TradeFill]
            A list of Fill objects.
        post: Post
            A Post object
        canceled_orders: List[Cancel]
            Canceled maker orders as a result of the filled order
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.fills = fills
        self.post = post
        self.canceled_orders = canceled_orders

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a PartialFill
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        post_tx_event = raw_tx_log_event["event"]["c"][0]
        fills_tx_event = raw_tx_log_event["event"]["c"][1]
        cancel_orders_tx_event = raw_tx_log_event["event"]["c"][2]

        return cls(
            [
                TradeFill(
                    fill["reason"],
                    fill["symbol"],
                    fill["takerOrderHash"],
                    fill["makerOrderHash"],
                    Decimal(fill["makerOrderRemainingAmount"]),
                    Decimal(fill["amount"]),
                    Decimal(fill["price"]),
                    fill["takerSide"],
                    Outcome(
                        fill["makerOutcome"]["trader"],
                        fill["makerOutcome"]["strategy"],
                        Decimal(fill["makerOutcome"]["fee"]),
                        fill["makerOutcome"]["ddxFeeElection"],
                        Decimal(fill["makerOutcome"]["realizedPnl"]),
                        Decimal(fill["makerOutcome"]["newCollateral"]),
                        Decimal(fill["makerOutcome"]["newPositionAvgEntryPrice"]),
                        Decimal(fill["makerOutcome"]["newPositionBalance"]),
                        fill["makerOutcome"]["positionSide"],
                    ),
                    Outcome(
                        fill["takerOutcome"]["trader"],
                        fill["takerOutcome"]["strategy"],
                        Decimal(fill["takerOutcome"]["fee"]),
                        fill["takerOutcome"]["ddxFeeElection"],
                        Decimal(fill["takerOutcome"]["realizedPnl"]),
                        Decimal(fill["takerOutcome"]["newCollateral"]),
                        Decimal(fill["takerOutcome"]["newPositionAvgEntryPrice"]),
                        Decimal(fill["takerOutcome"]["newPositionBalance"]),
                        fill["takerOutcome"]["positionSide"],
                    ),
                    raw_tx_log_event["timeValue"],
                    raw_tx_log_event["requestIndex"],
                )
                for fill in fills_tx_event
            ],
            Post(
                post_tx_event["symbol"],
                post_tx_event["orderHash"],
                post_tx_event["side"],
                Decimal(post_tx_event["amount"]),
                Decimal(post_tx_event["price"]),
                post_tx_event["traderAddress"],
                post_tx_event["strategyId"],
                post_tx_event["bookOrdinal"],
                raw_tx_log_event["timeValue"],
                raw_tx_log_event["requestIndex"],
            ),
            [
                Cancel(
                    canceled_order["symbol"],
                    canceled_order["orderHash"],
                    Decimal(canceled_order["amount"]),
                    raw_tx_log_event["requestIndex"],
                )
                for canceled_order in cancel_orders_tx_event
            ],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a PartialFill transaction. A PartialFill consists of
        Fill objects, which will adjust the maker BookOrder leaf in the
        SMT, while also adjusting the Strategy, Position, and Trader
        leaves corresponding to both the maker and the taker. It also
        consists of a Post object, which will be a BookOrder consisting
        of what's left of the partially matched order.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to PartialFill transactions
        """

        # Loop through each fill event and process them individually
        for fill in self.fills:
            fill.process_tx(smt, trader_auditor_queue, suppress_trader_queue, **kwargs)

        # Process the remaining post event
        self.post.process_tx(smt, trader_auditor_queue, suppress_trader_queue, **kwargs)

    def __repr__(self):
        return f"PartialFill (event): request_index = {self.request_index}; fills = {self.fills}; post = {self.post}; canceled_orders = {self.canceled_orders}"
