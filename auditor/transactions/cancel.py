"""
Cancel module
"""
import asyncio
from typing import Dict
from decimal import Decimal

from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.transactions.event import Event
from trading_clients.auditor.transactions.event_message import EventMessage
from trading_clients.auditor.transactions.event_types import EventType
from trading_clients.auditor.transactions.identifiers.cancel_identifier import (
    CancelIdentifier,
)
from trading_clients.auditor.utils import bytes_array_to_h256
from trading_clients.client.asyncio_utils import place_message_in_queue
from trading_clients.client.websocket_message import WebsocketEventType


class Cancel(Event):
    """
    Defines a Cancel
    """

    def __init__(
        self, symbol: str, order_hash: str, amount: Decimal, request_index: int,
    ):
        """
        Initialize a Cancel transaction. A Cancel is when an existing
        order is canceled and removed from the order book.

        Parameters
        ----------
        symbol : str
           The symbol for the market this order is for.
        order_hash: str
           Hexstr representation of the EIP-712 hash of the order
        amount: Decimal
           Amount/size of order
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.symbol = symbol
        self.order_hash = order_hash
        self.amount = amount

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a Cancel
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        cancel_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            cancel_tx_event["symbol"],
            cancel_tx_event["orderHash"],
            Decimal(cancel_tx_event["amount"]),
            raw_tx_log_event["requestIndex"],
        )

    @classmethod
    def decode_event_trigger_into_cls(cls, event_trigger: Dict):
        """
        Decode a event trigger (dict) into a Fill
        instance.

        Parameters
        ----------
        event_trigger : Dict
            Event trigger being processed
        """

        return cls(
            event_trigger["symbol"],
            event_trigger["orderHash"],
            Decimal(event_trigger["amount"]),
            event_trigger["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a Cancel transaction. We will need to delete a
        BookOrder leaf with this information.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Place the Cancel event message on the queue to
        # consider sending to the Trader
        cancel_identifier = CancelIdentifier.decode_tx_into_cls(self)
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            EventMessage(
                cancel_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": cancel_identifier.topic_string, "c": self},
            ),
        )

        # Construct a BookOrderIdentifier and corresponding encoded key
        book_order_identifier = BookOrderIdentifier(
            self.symbol, self.order_hash, None, None,
        )
        book_order_key = book_order_identifier.encoded_key
        book_order_key_h256 = bytes_array_to_h256(book_order_key)

        # Apply the cancel transaction to the local order book
        # abstraction
        kwargs["order_books"][self.symbol].apply_cancel_tx_to_book(
            book_order_key_h256, self
        )

        book_order_leaf = smt.get(book_order_key_h256)

        # Update the SMT with the H256 repr of the key and the Empty
        # leaf, effectively removing it from the SMT
        smt.update(book_order_key_h256, Empty())

        # Place the BookOrder leaf update message on the queue to
        # consider sending to the Trader
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            ItemMessage(
                BookOrderIdentifier.decode_key_into_cls(
                    book_order_key, book_order_leaf
                ).topic_string,
                WebsocketEventType.UPDATE,
                {
                    "t": BookOrderIdentifier.decode_key_into_cls(
                        book_order_key, book_order_leaf
                    ).topic_string,
                    "c": Empty(),
                },
                self,
            ),
        )

    def repr_json(self):
        return {
            "eventType": EventType.CANCEL,
            "requestIndex": self.request_index,
            "symbol": self.symbol,
            "orderHash": self.order_hash,
            "amount": str(self.amount),
        }

    def __repr__(self):
        return f"Cancel (event): request_index = {self.request_index}; symbol = {self.symbol}; order_hash = {self.order_hash}; amount: {self.amount}"
