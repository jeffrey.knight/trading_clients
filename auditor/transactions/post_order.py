"""
PostOrder module
"""
import asyncio
from typing import Dict, List
from decimal import Decimal

from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.book_order import BookOrder
from trading_clients.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from trading_clients.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.transactions.cancel import Cancel
from trading_clients.auditor.transactions.event import Event
from trading_clients.auditor.transactions.event_message import EventMessage
from trading_clients.auditor.transactions.event_types import EventType
from trading_clients.auditor.transactions.identifiers.post_identifier import (
    PostIdentifier,
)
from trading_clients.auditor.shared.order_book import OrderBook
from trading_clients.auditor.transactions.post import Post
from trading_clients.auditor.utils import bytes_array_to_h256
from trading_clients.client.asyncio_utils import place_message_in_queue
from trading_clients.client.websocket_message import WebsocketEventType


class PostOrder(Event):
    """
    Defines a PostOrder
    """

    def __init__(
        self, post: Post, canceled_orders: List[Cancel], request_index: int,
    ):
        """
        Initialize a PostOrder transaction. A PostOrder is an order that enters
        the order book along with any canceled maker orders.

        Parameters
        ----------
        post : Post
           The posted order.
        canceled_orders: List[Cancel]
           Canceled maker orders as a result of the posted order
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.post = post
        self.canceled_orders = canceled_orders

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a PostOrder
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        post_tx_event = raw_tx_log_event["event"]["c"][0]
        cancel_orders_tx_event = raw_tx_log_event["event"]["c"][1]

        return cls(
            Post(
                post_tx_event["symbol"],
                post_tx_event["orderHash"],
                post_tx_event["side"],
                Decimal(post_tx_event["amount"]),
                Decimal(post_tx_event["price"]),
                post_tx_event["traderAddress"],
                post_tx_event["strategyId"],
                post_tx_event["bookOrdinal"],
                raw_tx_log_event["timeValue"],
                raw_tx_log_event["requestIndex"],
            ),
            [
                Cancel(
                    canceled_order["symbol"],
                    canceled_order["orderHash"],
                    Decimal(canceled_order["amount"]),
                    raw_tx_log_event["requestIndex"],
                )
                for canceled_order in cancel_orders_tx_event
            ],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a PostOrder transaction. We will need to create a new
        BookOrder leaf with this information.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to PostOrder transactions
        """

        # Loop through each cancel event and process them individually
        for canceled_order in self.canceled_orders:
            canceled_order.process_tx(
                smt, trader_auditor_queue, suppress_trader_queue, **kwargs
            )

        # Process the post event
        self.post.process_tx(smt, trader_auditor_queue, suppress_trader_queue, **kwargs)

    def __repr__(self):
        return f"PostOrder (event): request_index = {self.request_index}; post = {self.post}; canceled_orders = {self.canceled_orders}"
