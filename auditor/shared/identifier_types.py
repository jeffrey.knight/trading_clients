from enum import Enum


class IdentifierTypes(str, Enum):
    """
    Defines an identifier type
    """

    TRADER = "TRADER"
    STRATEGY = "STRATEGY"
    POSITION = "POSITION"
    BOOK_ORDER = "BOOK_ORDER"
    PRICE = "PRICE"
    STATS = "STATS"
    ORGANIC_INSURANCE_FUND = "ORGANIC_INSURANCE_FUND"
    MEMBER = "MEMBER"

    POST = "POST"
    CANCEL = "CANCEL"
    STRATEGY_UPDATE = "STRATEGY_UPDATE"
    TRADER_UPDATE = "TRADER_UPDATE"
    PRICE_CHECKPOINT = "PRICE_CHECKPOINT"
    FILL = "FILL"
