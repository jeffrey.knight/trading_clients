"""
DerivaDEX Order Book (L2)
"""

from collections import deque
from typing import Dict, Optional, List
from decimal import Decimal
import numpy as np

from trading_clients.auditor.smt.h256 import H256
from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.book_order import BookOrder
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.transactions.cancel import Cancel
from trading_clients.auditor.transactions.fill import Fill
from trading_clients.auditor.transactions.post import Post


class OrderBook:
    def __init__(self, smt: SparseMerkleTree) -> None:
        """
        Initialize an order book. Each order book is tied to a given
        market, and contains both L2 and L3 information. An L2 order
        book is an aggregate view of the market, providing information
        regarding unique price levels and total quantities at those
        price levels. L3 order books maintain order-by-order, granular
        data. Users could query the SMT to obtain order book data
        (by querying the BookOrder leaves), however this local order
        book allows for faster and more intuitive querying for use
        by any trader clients.
        """

        # Maintain a mapping of price level to FIFO order queue (L3).
        # It's worth noting that we don't actually store the orders
        # themselves as that would be wasteful given that all of that
        # data already resides in the SMT. However, we do store the
        # BookOrder keys, thus we can query the SMT with these keys
        # to always have an up-to-date L3 order book.
        self.bids_l3: Dict[Decimal, Dict[bytes, BookOrder]] = {}
        self.asks_l3: Dict[Decimal, Dict[bytes, BookOrder]] = {}

        # Maintain a mapping of price level to aggregate quantity (L2).
        self.bids_l2: Dict[Decimal, Decimal] = {}
        self.asks_l2: Dict[Decimal, Decimal] = {}

        self.key_to_price: Dict[bytes, Decimal] = {}

    @property
    def best_bid_px(self) -> Optional[Decimal]:
        """
        Property returning the best available (highest) bid price.
        """

        return max(list(self.bids_l2.keys())) if self.bids_l2 else None

    @property
    def best_ask_px(self) -> Optional[Decimal]:
        """
        Property returning the best available (lowest) ask price.
        """

        return min(list(self.asks_l2.keys())) if self.asks_l2 else None

    @property
    def mid_px(self) -> Optional[Decimal]:
        """
        Property returning the mid price for the market.
        """

        if self.bids_l2 and self.asks_l2:
            return np.mean([self.best_bid_px, self.best_ask_px])
        elif self.bids_l2:
            return self.best_bid_px
        elif self.asks_l2:
            return self.best_ask_px
        return None

    @property
    def order_book_l2(self) -> Dict:
        """
        Property returning the L2 order book.
        """

        return {"bids": self.bids_l2, "asks": self.asks_l2}

    @property
    def order_book_l3(self) -> Dict:
        """
        Property returning the L3 order book. Keep in mind, these aren't
        the orders themselves, but rather the book order keys. These can
        be used to query into the SMT to retrieve the full order data.
        """

        return {"bids": self.bids_l3, "asks": self.asks_l3}

    def process_book_order_partial(self, book_order_message: ItemMessage) -> None:
        """
        Process a BookOrder leaf from the state snapshot. This will
        only really need to be done one time upon connecting, when
        there is a state snapshot sent up front from the WebSocket
        Trader API. The book order keys and leaves data can be sent
        to this method to initialize the local L2 and L3 order books
        accordingly.

        Parameters
        ----------
        book_order_key : H256
            H256-representation of the book order leaf key
        book_order : BookOrder
            The book order's data
        """

        # Sort partial by book ordinal to then enter in the L3 order
        # book data structure at the appropriate price level
        sorted_book_order_partial = sorted(
            book_order_message.message_content["item_data"],
            key=lambda book_order: book_order["bookOrdinal"],
        )

        for book_order_item in sorted_book_order_partial:
            book_order_identifier = BookOrderIdentifier.decode_topic_string_into_cls(
                book_order_item["t"]
            )
            book_order = BookOrder.decode_value_into_cls(book_order_item["c"])

            sided_book_l2 = self.bids_l2 if book_order.side == 0 else self.asks_l2
            sided_book_l3 = self.bids_l3 if book_order.side == 0 else self.asks_l3

            if book_order.price not in sided_book_l2:
                sided_book_l2[book_order.price] = book_order.amount
                sided_book_l3[book_order.price] = {
                    book_order_identifier.encoded_key: book_order
                }
            else:
                sided_book_l2[book_order.price] += book_order.amount
                sided_book_l3[book_order.price][
                    book_order_identifier.encoded_key
                ] = book_order

            self.key_to_price[book_order_identifier.encoded_key] = book_order.price

    def process_book_order_update(self, book_order_message: ItemMessage) -> None:
        """
        Process a BookOrder leaf from the state snapshot. This will
        only really need to be done one time upon connecting, when
        there is a state snapshot sent up front from the WebSocket
        Trader API. The book order keys and leaves data can be sent
        to this method to initialize the local L2 and L3 order books
        accordingly.

        Parameters
        ----------
        book_order_key : H256
            H256-representation of the book order leaf key
        book_order : BookOrder
            The book order's data
        """

        book_order_item = book_order_message.message_content
        book_order_identifier = BookOrderIdentifier.decode_topic_string_into_cls(
            book_order_item["t"]
        )
        book_order_encoded_key = book_order_identifier.encoded_key
        book_order = BookOrder.decode_value_into_cls(book_order_item["c"])

        sided_book_l2 = self.bids_l2 if book_order.side == 0 else self.asks_l2
        sided_book_l3 = self.bids_l3 if book_order.side == 0 else self.asks_l3

        if isinstance(book_order, Empty):
            book_order_price = self.key_to_price[book_order_encoded_key]
            sided_book_l2 = (
                self.bids_l2 if book_order_price in self.bids_l2 else self.asks_l2
            )
            sided_book_l3 = (
                self.bids_l3 if book_order_price in self.bids_l3 else self.asks_l3
            )

            sided_book_l2

        if book_order.price not in sided_book_l2:
            sided_book_l2[book_order.price] = book_order.amount
            sided_book_l3[book_order.price] = {
                book_order_identifier.encoded_key: book_order
            }
        else:
            sided_book_l2[book_order.price] += book_order.amount
            sided_book_l3[book_order.price][
                book_order_identifier.encoded_key
            ] = book_order

    def __repr__(self):
        return f"Order book (L2): bids = {self.bids}; asks = {self.asks}"
