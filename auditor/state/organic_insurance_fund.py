"""
Price module
"""
from typing import Dict
from eth_abi import encode_single, decode_single
from web3.auto import w3

from trading_clients.auditor.state.abi_constant import AbiConstant
from trading_clients.auditor.state.item import Item
from trading_clients.auditor.state.item_types import ItemType
from trading_clients.auditor.utils import (
    to_unit_amount,
    bytes_array_to_h256,
    to_base_unit_amount_list,
)


class OrganicInsuranceFund(Item):
    """
    Defines a OrganicInsuranceFund
    """

    def __init__(
        self, capitalization: Dict,
    ):
        """
        Initialize an OrganicInsuranceFund leaf. An InsuranceFund
        contains information pertaining to the insurance fund's
        capitalization.

        Parameters
        ----------
        capitalization : Dict
           Dictionary ({collateral_address: collateral_value})
           indicating the capitalization of the fund
        """

        self.item_type = ItemType.INSURANCE_FUND
        self.capitalization = capitalization

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the Strategy.
        """

        # Scale collateral amounts to DDX grains
        return encode_single(
            AbiConstant.ORGANIC_INSURANCE_FUND,
            [
                [
                    self.item_type,
                    [
                        list(self.capitalization.keys()),
                        to_base_unit_amount_list(
                            list(self.capitalization.values()), 18
                        ),
                    ],
                ]
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Price.
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        Price.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a Strategu leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded OrganicInsuranceFund from the
           state snapshot
        """

        (
            (item_type, (capitalization_tokens, capitalization_amounts,),),
        ) = decode_single(
            AbiConstant.ORGANIC_INSURANCE_FUND, w3.toBytes(hexstr=abi_encoded_value),
        )

        # Scale collateral amounts from DDX grains
        return cls(
            {
                k: to_unit_amount(v, 18)
                for k, v in zip(
                    list(capitalization_tokens), list(capitalization_amounts)
                )
            },
        )

    def __repr__(self):
        return f"OrganicInsuranceFund (state): item_type = {self.item_type}; capitalization = {self.capitalization}"
