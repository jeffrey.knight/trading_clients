"""
PriceSource module
"""
from decimal import Decimal
from typing import Dict, List
from web3.auto import w3
from eth_abi import encode_single, decode_single

from trading_clients.auditor.state.abi_constant import AbiConstant
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.item import Item
from trading_clients.auditor.state.item_types import ItemType
from trading_clients.auditor.state.weighted_price_source import WeightedPriceSource
from trading_clients.auditor.utils import (
    bytes_array_to_h256,
    member_want_role_to_int,
    member_int_to_want_role,
    to_base_unit_amount,
)


class PriceSource(Item):
    """
    Defines a PriceSource
    """

    def __init__(
        self,
        host_name: str,
        headers: str,
        quote_symbol: str,
        price_path: str,
        port: int,
        is_lower_cased: bool,
    ):
        """
        Initialize a PriceSource leaf. A PriceSource contains
        information pertaining to price source.

        Parameters
        ----------
        host_name : str
           The host name
        headers: Decimal
           The maximum order notional
        quote_symbol: Decimal
           The max taker price deviation
        price_path: Decimal
           The minimum order size
        port: List[WeightedPriceSource]
           The sources information
        is_lower_cased: List[WeightedPriceSource]
           The sources information
        """

        self.item_type = ItemType.PRICE_SOURCE
        self.host_name = host_name
        self.headers = headers
        self.quote_symbol = quote_symbol
        self.price_path = price_path
        self.port = port
        self.is_lower_cased = is_lower_cased

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the
        PriceSource.
        """

        return encode_single(
            AbiConstant.PRICE_SOURCE,
            [
                [
                    self.item_type,
                    [
                        self.host_name,
                        self.headers,
                        self.quote_symbol,
                        self.price_path,
                        self.port,
                        self.is_lower_cased,
                    ],
                ]
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Price
        Source.
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        PriceSource.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a PriceSource leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded PriceSource leaf from the
           state snapshot
        """

        (
            (
                item_type,
                (host_name, headers, quote_symbol, price_path, port, is_lower_cased,),
            ),
        ) = decode_single(
            AbiConstant.PRICE_SOURCE, w3.toBytes(hexstr=abi_encoded_value),
        )

        return cls(host_name, headers, quote_symbol, price_path, port, is_lower_cased,)

    @classmethod
    def decode_value_into_cls(cls, raw_leaf: Dict):
        """
        Decode a raw leaf (dict) into a PriceSource
        instance.

        Parameters
        ----------
        raw_leaf : Dict
            Raw leaf being processed
        """

        if not raw_leaf:
            return Empty()

        return cls(
            raw_leaf["hostName"],
            raw_leaf["headers"],
            raw_leaf["quoteSymbol"],
            raw_leaf["pricePath"],
            raw_leaf["port"],
            raw_leaf["isLowerCased"],
        )

    def repr_json(self):
        return {
            "hostName": self.host_name,
            "headers": self.headers,
            "quoteSymbol": self.quote_symbol,
            "pricePath": self.price_path,
            "port": self.port,
            "isLowerCased": self.is_lower_cased,
        }

    def __repr__(self):
        return f"PriceSource (state): item_type = {self.item_type}; host_name = {self.host_name}; headers = {self.headers}; quote_symbol: {self.quote_symbol}; price_path: {self.price_path}; port: {self.port}; is_lower_cased: {self.is_lower_cased}"
