"""
ItemType module
"""

from enum import Enum


class ItemType(int, Enum):
    """
    Defines an item type
    """

    TRADER = 0
    STRATEGY = 1
    POSITION = 2
    BOOK_ORDER = 3
    PRICE = 4
    INSURANCE_FUND = 5
    STATS = 6
    MEMBER = 7
    MARKET_SPECS = 8
    PRICE_SOURCE = 9
    EMPTY = 10
