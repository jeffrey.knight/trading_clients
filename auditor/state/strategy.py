"""
Strategy module
"""
from decimal import Decimal

from eth_abi.utils.padding import zpad32_right
from eth_abi import encode_single, decode_single
from web3.auto import w3
from typing import Dict, List

from trading_clients.auditor.state.abi_constant import AbiConstant
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.item import Item
from trading_clients.auditor.state.item_types import ItemType
from trading_clients.auditor.utils import (
    to_base_unit_amount_list,
    to_unit_amount,
    bytes_array_to_h256,
)


class Strategy(Item):
    """
    Defines a Strategy
    """

    def __init__(
        self,
        strategy_id: str,
        free_collateral: Dict,
        frozen_collateral: Dict,
        max_leverage: int,
        frozen: bool,
    ):
        """
        Initialize a Strategy leaf. A Strategy contains information
        pertaining to a trader's cross-margined strategy, such as their
        free and frozen collaterals and max leverage.

        Parameters
        ----------
        strategy_id: str
            Identifier for strategy
        free_collateral : Dict
           Dictionary ({collateral_address: collateral_value})
           indicating the collateral available for trading
        frozen_collateral : Dict
           Dictionary ({collateral_address: collateral_value})
           indicating the collateral available for on-chain withdrawal
        max_leverage: int
           Max leverage the strategy can take
        frozen: bool
           Whether the strategy is frozen or not (relevant for
           tokenization)
        """

        self.item_type = ItemType.STRATEGY
        self.strategy_id = strategy_id
        self.free_collateral = free_collateral
        self.frozen_collateral = frozen_collateral
        self.max_leverage = max_leverage
        self.frozen = frozen

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the Strategy.
        """

        # Scale collateral amounts to DDX grains
        return encode_single(
            AbiConstant.STRATEGY,
            [
                [
                    self.item_type,
                    [
                        len(self.strategy_id).to_bytes(1, byteorder="little")
                        + encode_single("bytes32", self.strategy_id.encode("utf8"))[
                            :-1
                        ],
                        [
                            list(self.free_collateral.keys()),
                            to_base_unit_amount_list(
                                list(self.free_collateral.values()), 18
                            ),
                        ],
                        [
                            list(self.frozen_collateral.keys()),
                            to_base_unit_amount_list(
                                list(self.frozen_collateral.values()), 18
                            ),
                        ],
                        self.max_leverage,
                        self.frozen,
                    ],
                ]
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Strategy.
        """
        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        Strategy.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a Strategu leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded Strategy from the
           state snapshot
        """

        (
            (
                item_type,
                (
                    strategy_id,
                    (free_collateral_tokens, free_collateral_amounts),
                    (frozen_collateral_tokens, frozen_collateral_amounts),
                    max_leverage,
                    frozen,
                ),
            ),
        ) = decode_single(AbiConstant.STRATEGY, w3.toBytes(hexstr=abi_encoded_value),)

        # Scale collateral amounts from DDX grains
        return cls(
            strategy_id[1 : 1 + strategy_id[0]].decode("utf8"),
            {
                k: to_unit_amount(v, 18)
                for k, v in zip(
                    list(free_collateral_tokens), list(free_collateral_amounts)
                )
            },
            {
                k: to_unit_amount(v, 18)
                for k, v in zip(
                    list(frozen_collateral_tokens), list(frozen_collateral_amounts)
                )
            },
            max_leverage,
            frozen,
        )

    @classmethod
    def decode_value_into_cls(cls, raw_leaf: Dict):
        """
        Decode a raw leaf (dict) into a Strategy
        instance.

        Parameters
        ----------
        raw_leaf : Dict
            Raw leaf being processed
        """

        if not raw_leaf:
            return Empty()

        return cls(
            raw_leaf["strategyId"],
            {k: Decimal(v) for k, v in raw_leaf["freeCollateral"].items()},
            {k: Decimal(v) for k, v in raw_leaf["frozenCollateral"].items()},
            raw_leaf["maxLeverage"],
            raw_leaf["frozen"],
        )

    def repr_json(self):
        return {
            "strategyId": self.strategy_id,
            "freeCollateral": {k: str(v) for k, v in self.free_collateral.items()},
            "frozenCollateral": {k: str(v) for k, v in self.frozen_collateral.items()},
            "maxLeverage": self.max_leverage,
            "frozen": self.frozen,
        }

    def __repr__(self):
        return f"Strategy (state): item_type = {self.item_type}; strategy_id = {self.strategy_id}; free_collateral = {self.free_collateral}; frozen_collateral = {self.frozen_collateral}; max_leverage: {self.max_leverage}; frozen: {self.frozen}"
