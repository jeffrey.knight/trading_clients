"""
Item module
"""


class Item:
    """
    Defines an Item
    """

    @property
    def abi_encoded_value(self):
        raise NotImplementedError()

    @property
    def hash_value(self):
        raise NotImplementedError()

    @property
    def as_h256(self):
        raise NotImplementedError()

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        raise NotImplementedError()
