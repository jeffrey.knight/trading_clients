"""
Empty module
"""
from trading_clients.auditor.smt.h256 import H256
from trading_clients.auditor.state.item import Item
from trading_clients.auditor.state.item_types import ItemType


class Empty(Item):
    """
    Defines Empty
    """

    def __init__(self):
        """
        Initialize an Empty leaf. An Empty leaf is input to the SMT in
        scenarios where a BookOrder now has nothing left or a Position
        leaf has 0 balance.
        """

        self.item_type = ItemType.EMPTY

    @property
    def abi_encoded_value(self):
        return

    @property
    def hash_value(self):
        return

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        Empty.
        """

        return H256.zero()

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        pass

    def repr_json(self):
        return {}

    def __repr__(self):
        return f"Empty (state): item_type = {self.item_type}"
