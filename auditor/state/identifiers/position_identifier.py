"""
PositionIdentifier module
"""

from typing import Optional

from trading_clients.auditor.shared.identifier import Identifier
from trading_clients.auditor.shared.identifier_types import IdentifierTypes
from trading_clients.auditor.smt.h256 import H256
from trading_clients.auditor.state.item_types import ItemType
from trading_clients.auditor.utils import (
    is_valid_none_list,
    pack_bytes,
    unpack_bytes,
    bytes_array_to_h256,
)


class PositionIdentifier(Identifier):
    """
    Defines an PositionIdentifier.
    """

    def __init__(
        self,
        symbol: Optional[str],
        trader_address: Optional[str],
        abbrev_strategy_id_hash: Optional[str],
    ):
        """
        Initialize a PositionIdentifier. Identifiers are used for a
        variety of purposes, most importantly to store identifying
        information for a particular leaf in the DerivaDEX SMT, or for
        a subset of leaves. This wrapper makes it convenient to generate
        topic strings for API subscriptions and key encodings.

        Parameters
        ----------
        symbol : str
            Market symbol
        trader_address : str
            Trader's Ethereum address prefixed with the blockchain
            discriminant
        abbrev_strategy_id_hash : str
            First 4 bytes of the hash of the trader's strategy ID
        """

        # Ensure that the list is valid
        if not is_valid_none_list([symbol, trader_address, abbrev_strategy_id_hash]):
            raise Exception("Invalid PositionIdentifier generation")

        self.symbol = symbol
        self.trader_address = trader_address
        self.abbrev_strategy_id_hash = abbrev_strategy_id_hash

    def encompasses_identifier(self, identifier) -> bool:
        """
        Check whether this Identifier encompasses / is a
        superset of another PositionIdentifier.

        Parameters
        ----------
        identifier : PositionIdentifier
            Another PositionIdentifier to compare against
        """

        if type(identifier).__name__ != "PositionIdentifier":
            # If the identifier we are comparing with isn't even of the
            # same type
            return False

        # Check varying degrees of encompassing depending on how
        # granular this Identifier is
        if self.abbrev_strategy_id_hash is not None:
            return (
                self.symbol == identifier.symbol
                and self.trader_address == identifier.trader_address
                and self.abbrev_strategy_id_hash == identifier.abbrev_strategy_id_hash
            )
        elif self.trader_address is not None:
            return (
                self.symbol == identifier.symbol
                and self.trader_address == identifier.trader_address
            )
        elif self.symbol is not None:
            return self.symbol == identifier.symbol
        return True

    @property
    def topic_string(self):
        """
        Derive topic string for Identifier.
        """

        return f"{'/'.join(filter(None, ['STATE', IdentifierTypes.POSITION, self.symbol, self.trader_address, self.abbrev_strategy_id_hash]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a PositionIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.POSITION:
            raise Exception(
                f"Invalid topic: must be of {IdentifierTypes.POSITION} type"
            )

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        trader_address = safe_list_get(topic_parts, 3, None)
        return cls(
            safe_list_get(topic_parts, 2, None),
            trader_address.lower() if trader_address is not None else trader_address,
            safe_list_get(topic_parts, 4, None),
        )

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a position key into a PositionIdentifier.

        Parameters
        ----------
        leaf_key : bytes
            Position leaf key
        leaf_value
            Leaf value
        """

        symbol = unpack_bytes(leaf_key[1:7])
        trader_address = f"0x{leaf_key[7:28].hex()}"
        abbrev_strategy_id_hash = f"0x{leaf_key[28:].hex()}"

        return cls(symbol, trader_address, abbrev_strategy_id_hash)

    @property
    def encoded_key(self) -> bytes:
        """
        Derive encoded key (bytes representation), which may be a full
        32-byte value if fully-granular, or a prefix to help capture
        a subset of leaves.
        """

        if self.abbrev_strategy_id_hash is not None:
            return (
                ItemType.POSITION.to_bytes(1, byteorder="little")
                + pack_bytes(self.symbol)
                + bytes.fromhex(self.trader_address[2:])
                + bytes.fromhex(self.abbrev_strategy_id_hash[2:])
            )
        elif self.trader_address is not None:
            return (
                ItemType.POSITION.to_bytes(1, byteorder="little")
                + pack_bytes(self.symbol)
                + bytes.fromhex(self.trader_address[2:])
            )
        elif self.symbol is not None:
            return ItemType.POSITION.to_bytes(1, byteorder="little") + pack_bytes(
                self.symbol
            )
        return ItemType.POSITION.to_bytes(1, byteorder="little")

    @property
    def as_h256(self) -> H256:
        """
        Derive an H256 representation of the encoded key, which can
        only be done in the case where the Identifier is fully-granular.
        """

        encoded_key = self.encoded_key
        if len(encoded_key) != 32:
            raise Exception("Must be 32-bytes to convert to H256")
        return bytes_array_to_h256(encoded_key)

    @property
    def is_max_granular_key(self):
        return not None in [
            self.symbol,
            self.trader_address,
            self.abbrev_strategy_id_hash,
        ]
