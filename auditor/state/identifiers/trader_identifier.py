"""
TraderIdentifier module
"""

from eth_abi.utils.padding import zpad32_right

from trading_clients.auditor.shared.identifier import Identifier
from trading_clients.auditor.shared.identifier_types import IdentifierTypes
from trading_clients.auditor.smt.h256 import H256
from trading_clients.auditor.state.item_types import ItemType
from trading_clients.auditor.utils import is_valid_none_list, bytes_array_to_h256


class TraderIdentifier(Identifier):
    """
    Defines an TraderIdentifier.
    """

    def __init__(self, trader_address: str):
        """
        Initialize a TraderIdentifier. Identifiers are used for a
        variety of purposes, most importantly to store identifying
        information for a particular leaf in the DerivaDEX SMT, or for
        a subset of leaves. This wrapper makes it convenient to generate
        topic strings for API subscriptions and key encodings.

        Parameters
        ----------
        trader_address : str
            Trader's Ethereum address prefixed with the blockchain
            discriminant
        """

        # Ensure that the list is valid
        if not is_valid_none_list([trader_address]):
            raise Exception("Invalid TraderIdentifier generation")

        self.trader_address = trader_address

    def encompasses_identifier(self, identifier):
        """
        Check whether this Identifier encompasses / is a
        superset of another TraderIdentifier.

        Parameters
        ----------
        identifier : TraderIdentifier
            Another TraderIdentifier to compare against
        """

        if type(identifier).__name__ != "TraderIdentifier":
            # If the identifier we are comparing with isn't even of the
            # same type
            return False

        # Check varying degrees of encompassing depending on how
        # granular this Identifier is
        if self.trader_address is not None:
            return self.trader_address == identifier.trader_address
        return True

    @property
    def topic_string(self):
        """
        Derive topic string for Identifier.
        """

        return f"{'/'.join(filter(None, ['STATE', IdentifierTypes.TRADER, self.trader_address]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a TraderIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.TRADER:
            raise Exception(f"Invalid topic: must be of {IdentifierTypes.TRADER} type")

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        trader_address = safe_list_get(topic_parts, 2, None)
        return cls(
            trader_address.lower() if trader_address is not None else trader_address,
        )

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a strategy key into a TraderIdentifier.

        Parameters
        ----------
        leaf_key : bytes
            Strategy leaf key
        leaf_value
            Leaf value
        """

        trader_address = f"0x{leaf_key[1:22].hex()}"

        return cls(trader_address)

    @property
    def encoded_key(self) -> bytes:
        """
        Derive encoded key (bytes representation), which may be a full
        32-byte value if fully-granular, or a prefix to help capture
        a subset of leaves.
        """

        if self.trader_address is not None:
            return zpad32_right(
                ItemType.TRADER.to_bytes(1, byteorder="little")
                + bytes.fromhex(self.trader_address[2:])
            )
        return ItemType.TRADER.to_bytes(1, byteorder="little")

    @property
    def as_h256(self) -> H256:
        """
        Derive an H256 representation of the encoded key, which can
        only be done in the case where the Identifier is fully-granular.
        """

        encoded_key = self.encoded_key
        if len(encoded_key) != 32:
            raise Exception("Must be 32-bytes to convert to H256")
        return bytes_array_to_h256(encoded_key)

    @classmethod
    def decode_stats_key_into_cls(cls, stats_key: bytes):
        """
        A convenience method to obtain the TraderIdentifier/key from a
        Stats leaf's key.

        Parameters
        ----------
        stats_key : bytes
            Stats leaf's key to derive a Trader key
        """

        trader_address = f"0x{stats_key[1:22].hex()}"

        return cls(trader_address)

    @property
    def is_max_granular_key(self):
        return not None in [
            self.trader_address,
        ]
