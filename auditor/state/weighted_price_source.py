"""
WeightedPriceSource module
"""
from decimal import Decimal


class WeightedPriceSource:
    """
    Defines a WeightedPriceSource
    """

    def __init__(
        self, source_name: str, weight: Decimal,
    ):
        """
        Initialize a WeightedPriceSource. A WeightedPriceSource contains
        information pertaining to a source name and its weight.

        Parameters
        ----------
        source_name : str
           The source name
        weight: Decimal
           The weight
        """

        self.source_name = source_name
        self.weight = weight

    def repr_json(self):
        return {
            "sourceName": self.source_name,
            "weight": self.weight,
        }

    def __repr__(self):
        return f"WeightedPriceSource (state): source_name = {self.source_name}; weight = {self.weight}"
