"""
MarketSpecs module
"""
from decimal import Decimal
from typing import Dict, List
from web3.auto import w3
from eth_abi import encode_single, decode_single

from trading_clients.auditor.state.abi_constant import AbiConstant
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.item import Item
from trading_clients.auditor.state.item_types import ItemType
from trading_clients.auditor.state.weighted_price_source import WeightedPriceSource
from trading_clients.auditor.utils import (
    bytes_array_to_h256,
    to_base_unit_amount,
    to_unit_amount,
)


class MarketSpecs(Item):
    """
    Defines a MarketSpecs
    """

    def __init__(
        self,
        tick_size: Decimal,
        max_order_notional: Decimal,
        max_taker_price_deviation: Decimal,
        min_order_size: Decimal,
        sources: List[WeightedPriceSource],
    ):
        """
        Initialize a MarketSpecs leaf. A MarketSpecs contains
        information pertaining to the specifications for a particular
        market.

        Parameters
        ----------
        tick_size : Decimal
           The market's tick size
        max_order_notional: Decimal
           The maximum order notional
        max_taker_price_deviation: Decimal
           The max taker price deviation
        min_order_size: Decimal
           The minimum order size
        sources: List[WeightedPriceSource]
           The sources information
        """

        self.item_type = ItemType.MARKET_SPECS
        self.tick_size = tick_size
        self.max_order_notional = max_order_notional
        self.max_taker_price_deviation = max_taker_price_deviation
        self.min_order_size = min_order_size
        self.sources = sources

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the
        MarketSpecs.
        """

        return encode_single(
            AbiConstant.MARKET_SPECS,
            [
                [
                    self.item_type,
                    [
                        to_base_unit_amount(self.tick_size, 18),
                        to_base_unit_amount(self.max_order_notional, 18),
                        to_base_unit_amount(self.max_taker_price_deviation, 18),
                        to_base_unit_amount(self.min_order_size, 18),
                        [
                            (source.source_name, to_base_unit_amount(source.weight, 18))
                            for source in self.sources
                        ],
                    ],
                ]
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded MarketSpecs
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        MarketSpecs.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a MarketSpecs leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded MarketSpecs leaf from the
           state snapshot
        """

        (
            (
                item_type,
                (
                    tick_size,
                    max_order_notional,
                    max_taker_price_deviation,
                    min_order_size,
                    sources,
                ),
            ),
        ) = decode_single(
            AbiConstant.MARKET_SPECS, w3.toBytes(hexstr=abi_encoded_value),
        )

        return cls(
            to_unit_amount(tick_size, 18),
            to_unit_amount(max_order_notional, 18),
            to_unit_amount(max_taker_price_deviation, 18),
            to_unit_amount(min_order_size, 18),
            [
                WeightedPriceSource(source[0], to_unit_amount(source[1], 18))
                for source in sources
            ],
        )

    @classmethod
    def decode_value_into_cls(cls, raw_leaf: Dict):
        """
        Decode a raw leaf (dict) into a MarketSpecs
        instance.

        Parameters
        ----------
        raw_leaf : Dict
            Raw leaf being processed
        """

        if not raw_leaf:
            return Empty()

        return cls(
            raw_leaf["tickSize"],
            raw_leaf["maxOrderNotional"],
            raw_leaf["maxTakerPriceDeviation"],
            raw_leaf["minOrderSize"],
            [
                WeightedPriceSource(source[0], source[1])
                for source in raw_leaf["sources"]
            ],
        )

    def repr_json(self):
        return {
            "tickSize": self.tick_size,
            "maxOrderNotional": self.max_order_notional,
            "maxTakerPriceDeviation": self.max_taker_price_deviation,
            "minOrderSize": self.min_order_size,
            "sources": self.sources,
        }

    def __repr__(self):
        return f"MarketSpecs (state): item_type = {self.item_type}; tick_size = {self.tick_size}; max_order_notional = {self.max_order_notional}; max_taker_price_deviation: {self.max_taker_price_deviation}; min_order_size: {self.min_order_size}; sources: {self.sources}"
