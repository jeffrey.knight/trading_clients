"""
Strategy module
"""
from typing import Dict

from eth_abi.utils.padding import zpad32_right
from eth_abi import encode_single, decode_single
from web3.auto import w3
from decimal import Decimal

from trading_clients.auditor.state.abi_constant import AbiConstant
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.item import Item
from trading_clients.auditor.state.item_types import ItemType
from trading_clients.auditor.utils import (
    to_base_unit_amount,
    to_unit_amount,
    bytes_array_to_h256,
)


class Trader(Item):
    """
    Defines a Trader
    """

    def __init__(
        self,
        free_ddx_balance: Decimal,
        frozen_ddx_balance: Decimal,
        referral_address: str,
    ):
        """
        Initialize a Trader leaf. A Trader contains information
        pertaining to a trader's free and frozen DDX balances, on-chain
        wallet contract address, and referral address (the person who
        referred them, if applicable).

        Parameters
        ----------
        free_ddx_balance : Decimal
           DDX collateral available for trading/staking/fees
        frozen_ddx_balance : Decimal
           DDX collateral available for on-chain withdrawal
        referral_address: str
           Referral address pertaining to the Ethereum address who
           referred this trader (if applicable)
        """

        self.item_type = ItemType.TRADER
        self.free_ddx_balance = free_ddx_balance
        self.frozen_ddx_balance = frozen_ddx_balance
        self.referral_address = referral_address

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the Strategy.
        """

        # Scale collateral amounts to DDX grains
        return encode_single(
            AbiConstant.TRADER,
            [
                self.item_type,
                [
                    to_base_unit_amount(self.free_ddx_balance, 18),
                    to_base_unit_amount(self.frozen_ddx_balance, 18),
                    self.referral_address,
                ],
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Trader.
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        Trader.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a Trader leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded Price Trader from the
           state snapshot
        """

        (
            item_type,
            (free_ddx_balance, frozen_ddx_balance, referral_address),
        ) = decode_single(AbiConstant.TRADER, w3.toBytes(hexstr=abi_encoded_value),)

        # Scale collateral amounts from DDX grains
        return cls(
            to_unit_amount(free_ddx_balance, 18),
            to_unit_amount(frozen_ddx_balance, 18),
            referral_address,
        )

    @classmethod
    def decode_value_into_cls(cls, raw_leaf: Dict):
        """
        Decode a raw leaf (dict) into a Trader
        instance.

        Parameters
        ----------
        raw_leaf : Dict
            Raw leaf being processed
        """

        if not raw_leaf:
            return Empty()

        return cls(
            Decimal(raw_leaf["freeDDXBalance"]),
            Decimal(raw_leaf["frozenDDXBalance"]),
            raw_leaf["referralAddress"],
        )

    def repr_json(self):
        return {
            "freeDDXBalance": str(self.free_ddx_balance),
            "frozenDDXBalance": str(self.frozen_ddx_balance),
            "referralAddress": self.referral_address,
        }

    def __repr__(self):
        return f"Trader (state): item_type = {self.item_type}; free_ddx_balance = {self.free_ddx_balance}; frozen_ddx_balance = {self.frozen_ddx_balance}; referral_address: {self.referral_address}"
