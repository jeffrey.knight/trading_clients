"""
Utils module
"""

from decimal import Decimal, ROUND_DOWN


def round_to_unit(val, tick_size):
    return val.quantize(tick_size, rounding=ROUND_DOWN)


def get_deployment_from_staging_env(staging_env: str):
    if staging_env in ["fuzz", "pre"]:
        return "geth"
    elif staging_env == "test":
        return "snapshot"
    elif staging_env == "api":
        return "beta"

    # alpha || beta
    return staging_env
