import json
import jsonschema
import sys
import typing
import subprocess
import yaml
import base64


def add_config_line(f: typing.TextIO, contents: str, tabs: int):
    formatting_string = ""
    for _ in range(tabs):
        formatting_string += "    "
    f.write(formatting_string + contents + "\n")


# JSON schema objects that define the schema for the
# datastructures in the provided config object. This
# makes validating the config extremely easy.
auditor_schema = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "image": {"type": "string"},
        "auditorConfig": {
            "type": "object",
            "properties": {
                "logLevel": {"type": "string"},
                "stagingEnv": {"type": "string"},
            },
            "minProperties": 2,
        },
        "isKube": {"type": "boolean"},
    },
    "minProperties": 4,
}
mm_schema = {
    "type": "object",
    "properties": {
        "auditorName": {"type": "string"},
        "image": {"type": "string"},
        "mmCount": {"type": "number"},
        "mmConfig": {
            "type": "object",
            "properties": {
                "symbol": {"type": "string"},
                "depositMinimum": {"type": "number"},
                "depositAmount": {"type": "number"},
                "levelsToQuote": {"type": "number"},
                "logLevel": {"type": "string"},
                "priceOffset": {"type": "number"},
                "quantityPerLevel": {"type": "number"},
                "refPXDeviationToReplaceOrders": {"type": "number"},
                "sleepRate": {"type": "number"},
                "tradingStrategy": {"type": "string"},
            },
            "minProperties": 10,
        },
        "ethereumConfig": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "address": {"type": "string"},
                    "mnemonic": {"type": "string"},
                    "privateKey": {"type": "string"},
                    "rpcUrl": {"type": "string"},
                },
                "oneOf": [
                    {"required": ["address", "mnemonic", "rpcUrl"]},
                    {"required": ["address", "privateKey", "rpcUrl"]},
                ],
            },
            "minItems": 1,
            "uniqueItems": True,
        },
    },
    "minProperties": 5,
}
mt_schema = {
    "type": "object",
    "properties": {
        "auditorName": {"type": "string"},
        "image": {"type": "string"},
        "mtCount": {"type": "number"},
        "mtConfig": {
            "type": "object",
            "properties": {
                "depositMinimum": {"type": "number"},
                "depositAmount": {"type": "number"},
                "logLevel": {"type": "string"},
                "longLikelihood": {"type": "number"},
                "quantity": {"type": "number"},
                "sleepRate": {"type": "number"},
            },
            "minProperties": 6,
        },
        "ethereumConfig": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "address": {"type": "string"},
                    "mnemonic": {"type": "string"},
                    "privateKey": {"type": "string"},
                    "rpcUrl": {"type": "string"},
                },
                "oneOf": [
                    {"required": ["address", "mnemonic", "rpcUrl"]},
                    {"required": ["address", "privateKey", "rpcUrl"]},
                ],
            },
            "minItems": 1,
            "uniqueItems": True,
        },
    },
    "minProperties": 5,
}

# Validate the command line arguments.
if len(sys.argv) != 3 or sys.argv[1] != "--config":
    print("usage: compose_generator.py --config <path-to-config-file>")
    sys.exit(0)

config_path = sys.argv[2]
print(f"Generating compose file for config path {config_path}...")
with open(config_path) as f:
    config = json.load(f)

# Validate the auditor schema against the following JSON schema and
# add the auditor deployments to a dictionary of valid auditor deployments.
auditors = {}
if "auditorDeployments" not in config or len(config["auditorDeployments"]) == 0:
    raise Exception(f"[Error] At least one auditor deployment must be provided.")
for c in config["auditorDeployments"]:
    # Validate the deployment config.
    if c["name"] in auditors:
        raise Exception("[Error] Auditor name " + c["name"] + " duplicated in config.")
    jsonschema.validate(instance=c, schema=auditor_schema)

    # Add the config to the list of known auditors.
    auditors[c["name"]] = {
        "image": c["image"],
        "auditorConfig": c["auditorConfig"],
        "isKube": c["isKube"],
    }

# Validate the mm schema against the following JSON schema and
# add the mm deployments to a dictionary of valid auditor deployments.
mm = {}
if "mmDeployments" not in config:
    raise Exception(
        '[Error] The "mmDeployments" field was not specified in the config.'
    )
if len(config["mmDeployments"]) > 0:
    for c in config["mmDeployments"]:
        # Validate the deployment config.
        jsonschema.validate(instance=c, schema=mm_schema)
        if c["mmCount"] <= 0 or c["mmCount"] != len(c["ethereumConfigs"]):
            raise Exception(
                f"[Error] Malformed ethereum configs for mm " + c["auditorName"] + "."
            )
        # Add the config to the list of known auditors.
        if c["auditorName"] in mm:
            mm[c["auditorName"]] += [
                {
                    "image": c["image"],
                    "mmCount": c["mmCount"],
                    "mmConfig": c["mmConfig"],
                    "ethereumConfigs": c["ethereumConfigs"],
                }
            ]
        else:
            mm[c["auditorName"]] = [
                {
                    "image": c["image"],
                    "mmCount": c["mmCount"],
                    "mmConfig": c["mmConfig"],
                    "ethereumConfigs": c["ethereumConfigs"],
                }
            ]

# Validate the mt schema against the following JSON schema and
# add the mt deployments to a dictionary of valid auditor deployments.
mt = {}
if "mtDeployments" not in config:
    raise Exception(
        '[Error] The "mtDeployments" field was not specified in the config.'
    )
if len(config["mtDeployments"]) > 0:
    for c in config["mtDeployments"]:
        # Validate the deployment config.
        jsonschema.validate(instance=c, schema=mt_schema)
        if c["mtCount"] <= 0 or c["mtCount"] != len(c["ethereumConfigs"]):
            raise Exception(
                f"[Error] Malformed ethereum configs for mt " + c["auditorName"] + "."
            )
        # Add the config to the list of known auditors.
        if c["auditorName"] in mt:
            mt[c["auditorName"]] += [
                {
                    "image": c["image"],
                    "mtCount": c["mtCount"],
                    "mtConfig": c["mtConfig"],
                    "ethereumConfigs": c["ethereumConfigs"],
                }
            ]
        else:
            mt[c["auditorName"]] = [
                {
                    "image": c["image"],
                    "mtCount": c["mtCount"],
                    "mtConfig": c["mtConfig"],
                    "ethereumConfigs": c["ethereumConfigs"],
                }
            ]

with open("docker-compose.generated.yml", "w") as docker_compose_file, open(
    ".env.generated", "w"
) as env_file:
    # Write the prelude to the docker-compose file.
    add_config_line(docker_compose_file, "---", 0)
    add_config_line(docker_compose_file, "version: '3'", 0)
    add_config_line(docker_compose_file, "", 0)
    add_config_line(docker_compose_file, "services:", 0)

    # Write the auditor service definitions that are needed in the docker-compose
    # file and write the appropriate environment variable declarations in the env
    # file.
    has_written_line = False
    for name in auditors:
        add_config_line(docker_compose_file, f"{name}:", 1)
        add_config_line(docker_compose_file, f"image: " + auditors[name]["image"], 2)
        add_config_line(docker_compose_file, "environment:", 2)
        add_config_line(docker_compose_file, f"- PYTHON_LOG=${{PYTHON_LOG_{name}}}", 3)
        add_config_line(
            docker_compose_file, f"- STAGING_ENV=${{STAGING_ENV_{name}}}", 3
        )
        if auditors[name]["isKube"]:
            add_config_line(docker_compose_file, "restart: always", 2)
            add_config_line(docker_compose_file, "ports:", 2)
            add_config_line(docker_compose_file, '- "80:8080"', 3)
            add_config_line(docker_compose_file, "env_file: .env.generated", 2)

        if has_written_line:
            add_config_line(
                env_file, f"\n# Environment variables for auditor {name}", 0
            )
        else:
            add_config_line(env_file, f"# Environment variables for auditor {name}", 0)
        add_config_line(
            env_file,
            f"PYTHON_LOG_{name}=" + auditors[name]["auditorConfig"]["logLevel"],
            0,
        )
        add_config_line(
            env_file,
            f"STAGING_ENV_{name}=" + auditors[name]["auditorConfig"]["stagingEnv"],
            0,
        )

    # Write the market making service definitions that are needed in the
    # docker-compose file and write the appropriate environment variable
    # declarations in the env file.
    for name in mm:
        # Write the environment variables that will be stable across market making bots.
        add_config_line(
            env_file,
            f"\n# Environment variables for market maker swarms targeting {name}",
            0,
        )
        add_config_line(
            env_file,
            f"STAGING_ENV_{name}=" + auditors[name]["auditorConfig"]["stagingEnv"],
            0,
        )

        for i in range(len(mm[name])):
            symbol = str(mm[name][i]["mmConfig"]["symbol"])
            # Write the environment variables that will be stable across market making bots.
            add_config_line(
                env_file,
                f"\n## Environment variables for market maker swarm {i} targeting {name}",
                0,
            )
            add_config_line(
                env_file,
                f"PYTHON_LOG_mm_{name}_{i}=" + mm[name][i]["mmConfig"]["logLevel"],
                0,
            )
            add_config_line(
                env_file,
                f"DEPOSIT_MINIMUM_mm_{name}_{i}="
                + str(mm[name][i]["mmConfig"]["depositMinimum"]),
                0,
            )
            add_config_line(
                env_file,
                f"DEPOSIT_AMOUNT_mm_{name}_{i}="
                + str(mm[name][i]["mmConfig"]["depositAmount"]),
                0,
            )
            add_config_line(
                env_file, f"SYMBOL_mm_{name}_{i}=" + symbol, 0,
            )
            add_config_line(
                env_file,
                f"LEVELS_TO_QUOTE_{name}_{i}="
                + str(mm[name][i]["mmConfig"]["levelsToQuote"]),
                0,
            )
            add_config_line(
                env_file,
                f"PRICE_OFFSET_{name}_{i}="
                + str(mm[name][i]["mmConfig"]["priceOffset"]),
                0,
            )
            add_config_line(
                env_file,
                f"QUANTITY_PER_LEVEL_{name}_{i}="
                + str(mm[name][i]["mmConfig"]["quantityPerLevel"]),
                0,
            )
            add_config_line(
                env_file,
                f"REF_PX_DEVIATION_TO_REPLACE_ORDERS_{name}_{i}="
                + str(mm[name][i]["mmConfig"]["refPXDeviationToReplaceOrders"]),
                0,
            )
            add_config_line(
                env_file,
                f"SLEEP_RATE_mm_{name}_{i}="
                + str(mm[name][i]["mmConfig"]["sleepRate"]),
                0,
            )
            add_config_line(
                env_file,
                f"TRADING_STRATEGY_{name}_{i}="
                + mm[name][i]["mmConfig"]["tradingStrategy"],
                0,
            )
            # TODO: Add these as config fields so market making bots can target different markets.
            add_config_line(env_file, f"ORDER_BOOK_SYMBOLS_mm_{name}_{i}=[{symbol}]", 0)
            add_config_line(
                env_file,
                f"PRICE_CHECKPOINT_TOPICS_mm_{name}_{i}=[TX_LOG/PRICE_CHECKPOINT/{symbol}/]",
                0,
            )

            for j in range(mm[name][i]["mmCount"]):
                # Write the environment variables that are static across all market making bots.
                add_config_line(docker_compose_file, f"mm-{name}-{i}-{j}:", 1)
                add_config_line(docker_compose_file, f"depends_on:", 2)
                add_config_line(docker_compose_file, f"- {name}", 3)
                add_config_line(
                    docker_compose_file, f"image: " + mm[name][i]["image"] + "", 2
                )
                add_config_line(docker_compose_file, f"environment:", 2)
                add_config_line(docker_compose_file, f"- AUDITOR_HOST={name}", 3)
                add_config_line(
                    docker_compose_file, f"- STAGING_ENV=${{STAGING_ENV_{name}}}", 3
                )
                add_config_line(
                    docker_compose_file,
                    f"- PYTHON_LOG=${{PYTHON_LOG_mm_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file, f"- SYMBOL=${{SYMBOL_mm_{name}_{i}}}", 3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- DEPOSIT_MINIMUM=${{DEPOSIT_MINIMUM_mm_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- DEPOSIT_AMOUNT=${{DEPOSIT_AMOUNT_mm_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- LEVELS_TO_QUOTE=${{LEVELS_TO_QUOTE_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- PRICE_OFFSET=${{PRICE_OFFSET_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- QUANTITY_PER_LEVEL=${{QUANTITY_PER_LEVEL_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- REF_PX_DEVIATION_TO_REPLACE_ORDERS=${{REF_PX_DEVIATION_TO_REPLACE_ORDERS_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- SLEEP_RATE=${{SLEEP_RATE_mm_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- TRADING_STRATEGY=${{TRADING_STRATEGY_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- ORDER_BOOK_SYMBOLS=${{ORDER_BOOK_SYMBOLS_mm_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- PRICE_CHECKPOINT_TOPICS=${{PRICE_CHECKPOINT_TOPICS_mm_{name}_{i}}}",
                    3,
                )

                # Write the environment variables that change depending on the node count.
                add_config_line(
                    docker_compose_file,
                    f"- BOOK_ORDER_TOPICS=${{BOOK_ORDER_TOPICS_mm_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- FILL_TOPICS=${{FILL_TOPICS_mm_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- POSITION_TOPICS=${{POSITION_TOPICS_mm_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- STRATEGY_TOPICS=${{STRATEGY_TOPICS_mm_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- TRADER_TOPICS=${{TRADER_TOPICS_mm_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- MNEMONIC=${{MNEMONIC_mm_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- PRIVATE_KEY=${{PRIVATE_KEY_mm_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file, f"- RPC_URL=${{RPC_URL_mm_{name}_{i}_{j}}}", 3
                )

                # Write the environment variable definitions that change depending on the
                # node count.
                address = mm[name][i]["ethereumConfigs"][j]["address"]
                add_config_line(
                    env_file,
                    f"\n## Environment variables for node {j} of market maker swarm {i} targeting {name}",
                    0,
                )
                add_config_line(
                    env_file,
                    f"BOOK_ORDER_TOPICS_mm_{name}_{i}_{j}=[STATE/BOOK_ORDER/{symbol}/{address}/0x2576ebd1/]",
                    0,
                )
                add_config_line(
                    env_file,
                    f"FILL_TOPICS_mm_{name}_{i}_{j}=[TX_LOG/FILL/{symbol}/{address}/0x2576ebd1/]",
                    0,
                )
                add_config_line(
                    env_file,
                    f"POSITION_TOPICS_mm_{name}_{i}_{j}=[STATE/POSITION/{symbol}/{address}/0x2576ebd1/]",
                    0,
                )
                add_config_line(
                    env_file,
                    f"STRATEGY_TOPICS_mm_{name}_{i}_{j}=[STATE/STRATEGY/{address}/0x2576ebd1/]",
                    0,
                )
                add_config_line(
                    env_file,
                    f"TRADER_TOPICS_mm_{name}_{i}_{j}=[STATE/TRADER/{address}/]",
                    0,
                )
                add_config_line(
                    env_file,
                    f"RPC_URL_mm_{name}_{i}_{j}="
                    + mm[name][i]["ethereumConfigs"][j]["rpcUrl"],
                    0,
                )
                if "privateKey" in mm[name][i]["ethereumConfigs"][j]:
                    add_config_line(
                        env_file,
                        f"PRIVATE_KEY_mm_{name}_{i}_{j}="
                        + mm[name][i]["ethereumConfigs"][j]["privateKey"],
                        0,
                    )
                if "mnemonic" in mm[name][i]["ethereumConfigs"][j]:
                    add_config_line(
                        env_file,
                        f"MNEMONIC_mm_{name}_{i}_{j}="
                        + mm[name][i]["ethereumConfigs"][j]["mnemonic"],
                        0,
                    )

    # Write the mt service definitions that are needed in the
    # docker-compose file and write the appropriate environment variable
    # declarations in the env file.
    for name in mt:
        # Write the environment variables that will be stable across market taking bots.
        add_config_line(
            env_file,
            f"\n# Environment variables for market taker swarms targeting {name}",
            0,
        )
        add_config_line(
            env_file,
            f"STAGING_ENV_{name}=" + auditors[name]["auditorConfig"]["stagingEnv"],
            0,
        )

        for i in range(len(mt[name])):
            # Write the environment variables that will be stable across market taking bots.
            add_config_line(
                env_file,
                f"\n## Environment variables for market taker swarm {i} targeting {name}",
                0,
            )
            add_config_line(
                env_file,
                f"PYTHON_LOG_mt_{name}_{i}=" + mt[name][i]["mtConfig"]["logLevel"],
                0,
            )
            add_config_line(
                env_file,
                f"DEPOSIT_MINIMUM_mt_{name}_{i}="
                + str(mt[name][i]["mtConfig"]["depositMinimum"]),
                0,
            )
            add_config_line(
                env_file,
                f"DEPOSIT_AMOUNT_mt_{name}_{i}="
                + str(mt[name][i]["mtConfig"]["depositAmount"]),
                0,
            )
            add_config_line(
                env_file,
                f"LONG_LIKELIHOOD_{name}_{i}="
                + str(mt[name][i]["mtConfig"]["longLikelihood"]),
                0,
            )
            add_config_line(
                env_file,
                f"QUANTITY_{name}_{i}=" + str(mt[name][i]["mtConfig"]["quantity"]),
                0,
            )
            add_config_line(
                env_file,
                f"SLEEP_RATE_mt_{name}_{i}="
                + str(mt[name][i]["mtConfig"]["sleepRate"]),
                0,
            )
            # TODO: Add these as config fields so market taking bots can target different markets.
            add_config_line(env_file, f"ORDER_BOOK_SYMBOLS_mt_{name}_{i}=[ETHPERP]", 0)
            add_config_line(
                env_file,
                f"PRICE_CHECKPOINT_TOPICS_mt_{name}_{i}=[TX_LOG/PRICE_CHECKPOINT/ETHPERP/]",
                0,
            )

            for j in range(mt[name][i]["mtCount"]):
                # Write the environment variables that are static across all market taking bots.
                add_config_line(docker_compose_file, f"mt-{name}-{i}-{j}:", 1)
                add_config_line(docker_compose_file, f"depends_on:", 2)
                add_config_line(docker_compose_file, f"- {name}", 3)
                add_config_line(
                    docker_compose_file, f"image: " + mt[name][i]["image"] + "", 2
                )
                add_config_line(docker_compose_file, f"environment:", 2)
                add_config_line(docker_compose_file, f"- AUDITOR_HOST={name}", 3)
                add_config_line(
                    docker_compose_file, f"- STAGING_ENV=${{STAGING_ENV_{name}}}", 3
                )
                add_config_line(
                    docker_compose_file,
                    f"- PYTHON_LOG=${{PYTHON_LOG_mt_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- DEPOSIT_MINIMUM=${{DEPOSIT_MINIMUM_mt_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- DEPOSIT_AMOUNT=${{DEPOSIT_AMOUNT_mt_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- LONG_LIKELIHOOD=${{LONG_LIKELIHOOD_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file, f"- QUANTITY=${{QUANTITY_{name}_{i}}}", 3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- SLEEP_RATE=${{SLEEP_RATE_mt_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- ORDER_BOOK_SYMBOLS=${{ORDER_BOOK_SYMBOLS_mt_{name}_{i}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- PRICE_CHECKPOINT_TOPICS=${{PRICE_CHECKPOINT_TOPICS_mt_{name}_{i}}}",
                    3,
                )

                # Write the environment variables that change depending on the node count.
                add_config_line(
                    docker_compose_file,
                    f"- BOOK_ORDER_TOPICS=${{BOOK_ORDER_TOPICS_mt_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- FILL_TOPICS=${{FILL_TOPICS_mt_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- POSITION_TOPICS=${{POSITION_TOPICS_mt_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- STRATEGY_TOPICS=${{STRATEGY_TOPICS_mt_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- TRADER_TOPICS=${{TRADER_TOPICS_mt_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- MNEMONIC=${{MNEMONIC_mt_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file,
                    f"- PRIVATE_KEY=${{PRIVATE_KEY_mt_{name}_{i}_{j}}}",
                    3,
                )
                add_config_line(
                    docker_compose_file, f"- RPC_URL=${{RPC_URL_mt_{name}_{i}_{j}}}", 3
                )

                # Write the environment variable definitions that change depending on the
                # node count.
                address = mt[name][i]["ethereumConfigs"][j]["address"]
                add_config_line(
                    env_file,
                    f"\n## Environment variables for node {j} of market taker swarm {i} targeting {name}",
                    0,
                )
                add_config_line(
                    env_file,
                    f"BOOK_ORDER_TOPICS_mt_{name}_{i}_{j}=[STATE/BOOK_ORDER/ETHPERP/{address}/0x2576ebd1/]",
                    0,
                )
                add_config_line(
                    env_file,
                    f"FILL_TOPICS_mt_{name}_{i}_{j}=[TX_LOG/FILL/ETHPERP/{address}/0x2576ebd1/]",
                    0,
                )
                add_config_line(
                    env_file,
                    f"POSITION_TOPICS_mt_{name}_{i}_{j}=[STATE/POSITION/ETHPERP/{address}/0x2576ebd1/]",
                    0,
                )
                add_config_line(
                    env_file,
                    f"STRATEGY_TOPICS_mt_{name}_{i}_{j}=[STATE/STRATEGY/{address}/0x2576ebd1/]",
                    0,
                )
                add_config_line(
                    env_file,
                    f"TRADER_TOPICS_mt_{name}_{i}_{j}=[STATE/TRADER/{address}/]",
                    0,
                )
                add_config_line(
                    env_file,
                    f"RPC_URL_mt_{name}_{i}_{j}="
                    + mt[name][i]["ethereumConfigs"][j]["rpcUrl"],
                    0,
                )
                if "privateKey" in mt[name][i]["ethereumConfigs"][j]:
                    add_config_line(
                        env_file,
                        f"PRIVATE_KEY_mt_{name}_{i}_{j}="
                        + mt[name][i]["ethereumConfigs"][j]["privateKey"],
                        0,
                    )
                if "mnemonic" in mt[name][i]["ethereumConfigs"][j]:
                    add_config_line(
                        env_file,
                        f"MNEMONIC_mt_{name}_{i}_{j}="
                        + mt[name][i]["ethereumConfigs"][j]["mnemonic"],
                        0,
                    )

if config["auditorDeployments"] and config["auditorDeployments"][0]["isKube"]:
    if len(config["auditorDeployments"]) != 1:
        raise Exception(
            "Kubernetes deployment must have only one auditor deployment specified"
        )

    compose_file_path = "docker-compose.generated.yml"
    print(f"Generating auditor kube files for compose-file {compose_file_path}...")

    # Run kompose and capture the output
    kompose_output = subprocess.run(
        ["kompose", "convert", "-f", compose_file_path, "--stdout"], capture_output=True
    ).stdout

    # Load output into YAML object
    yaml_output = yaml.safe_load(kompose_output)

    # Loop through the various Kube item definitions
    for item in yaml_output["items"]:
        if item["kind"] == "Service":
            # Write the auditor service as is
            with open("auditor-service.yaml", "w") as auditor_service_file:
                yaml.safe_dump(item, auditor_service_file, default_flow_style=False)
        elif item["kind"] == "ConfigMap":
            # Write the env config map with some modifications discussed below
            with open(
                "env-generated-configmap.yaml", "w"
            ) as env_generated_configmap_file:
                # YAML auto converts the hex values, so we convert them back

                # The private key needs to be saved as a secret in base64-
                # encoded form and removed from the original env config map
                # accordingly
                private_key_auditor = hex(item["data"].pop("PRIVATE_KEY_auditor"))
                with open("secret.yaml", "w") as secret_file:
                    # Create a Kube Secret file warehousing the private
                    # key
                    base64_encoded_auditor_private_key = base64.b64encode(
                        private_key_auditor.encode("utf8")
                    ).decode("utf8")
                    secret_data = {
                        "apiVersion": "v1",
                        "kind": "Secret",
                        "metadata": {"name": "ddx-auditor-secret"},
                        "data": {"PRIVATE_KEY": base64_encoded_auditor_private_key},
                    }
                    yaml.safe_dump(secret_data, secret_file, default_flow_style=False)
                yaml.safe_dump(
                    item, env_generated_configmap_file, default_flow_style=False
                )
        elif item["kind"] == "Deployment":
            # Write the deployment with some modifications discussed below
            with open("auditor-deployment.yaml", "w") as auditor_service_file:
                # Remove any "name: <ENV_VAR>_auditor" entries
                item["spec"]["template"]["spec"]["containers"][0]["env"] = [
                    env_entry
                    for env_entry in item["spec"]["template"]["spec"]["containers"][0][
                        "env"
                    ]
                    if len(env_entry) != 1
                ]
                for env_entry in item["spec"]["template"]["spec"]["containers"][0][
                    "env"
                ]:
                    # Drop the "_auditor" suffix for the entry names
                    env_entry["name"] = env_entry["name"].partition("_auditor")[0]

                    # We need to change the private key mapping to reference
                    # the Secret created above instead of the Env config map
                    if env_entry["name"] == "PRIVATE_KEY":
                        env_entry["valueFrom"] = {
                            "secretKeyRef": {
                                "key": "PRIVATE_KEY",
                                "name": "ddx-auditor-secret",
                            }
                        }
                yaml.safe_dump(item, auditor_service_file, default_flow_style=False)
