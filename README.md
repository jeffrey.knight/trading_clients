# DerivaDEX Trading Client Tooling

This repository is under active development and currently includes:

- An auditor client
- Sample strategies

Please use these materials in conjunction with the API documentation: http://api.derivadex.io/

# Deploying the auditor and trading Bot

Since docker infrastructure has been created to run the DerivaDEX auditor, deploying
infrastructure locally or to a cloud provider can be accomplished easily. This tutorial
shows how an auditor and a sample market making bot can be deployed using `docker-compose`
and `docker-machine`. If you would like to run a different market making bot with
the same workflow, simply edit the `docker-compose.yml` file so that it points to
a different image for the `mm` service. In the same vein, multiple market making
bots can be run simultaneously by replicating the `mm` service in the `docker-compose`
file.


## Configuring auditors and market making bots

Since deploying auditors and market makers is an important component of testing
the DerivaDEX system, we have created a tool that dramatically improves the ease
of deploying large sets of nodes. Since auditors and market making bots both
utilize Ethereum credentials, you'll need to specify Ethereum configurations for
both types of deployments. The market maker Ethereum configurations do not need
to correspond to the corresponding auditor's Ethereum configurations (and will
not when deploying more than one market maker). To utilize this tooling, follow
the steps below:

1. Copy the `.config.template.json` file into a location of your choosing (we'll
refer to this file path as `$CONFIG`).
   - Set `rpcUrl` to an exposed Ethereum JSON RPC provider (a node URL, an Infura
or Alchemy URL, etc.). If you don't have access to an Ethereum provider, you can
get access to a URL with the free tier of [Infura](https://infura.io/).
   - Ensure to specify a `mnemonic` or a `privateKey` in the `ethereumConfig` and
`ethereumConfigs` sections -- if both a `privateKey` and a `mnemonic` are provided, the command will fail.
   - Ensure that the length of the `ethereumConfigs` array matches the `mmCount`
value for all market maker deployments.
   - A common pattern traders have requested is to host the auditor by itself in a Kubernetes cluster,
so there is a special boolean flag here if this is an approach you would like to take
as well. This generates slightly different files to accommodate Kubernetes deployments. This is discussed
in the [section](#kubernetes-deployment) below.

2. Generate a docker compose file and environment variables file from the config
by running the command `python compose_generator.py --config $CONFIG` from the root
of the repo. Make sure to replace `$CONFIG` with your config!

These two steps will generate files that are used by the commands below. In the
event that you change the configuration file, the old generated files will be
overwritten. If this occurs, make sure to use the `--remove-orphans` flag when
tearing the container down.

### Making changes to the auditor or market making bot

If you want to make changes to the Auditor or the Market Making bot and continue
to use the docker scripts to manage deployments, you'll need to follow these steps
after making your desired edits:

1. Rebuild the images with the following command:

    ```
    # Build docker images
    docker build -t ddx-auditor -f Dockerfile.auditor .
    docker build -t ddx-mm -f Dockerfile.mm .
    ```

2. Once you build these images, update the `image` fields in the
`$CONFIG` config (refer to the above steps) to point to the locally built images
(`ddx-auditor` and `ddx-mm` in this case since these names were the arguments of the `-t`
option of `docker build`)

3. Re-run the `compose_generator.py` command to propagate the config files changes

## Deploying locally

After completing the steps above, you can deploy the DerivaDEX auditor(s) and
sample marketing making bot(s) with the following command:

```bash
./compose-generated.bash up -d
```

## Deploying to a remote machine

Before we can continue, you will need to set up an account with the cloud hosting
provider of your choice, and retrieve your access token/key/secret. We will use
them to create a new machine with name `ddx-auditor`. Docker has great documentation
on doing all of that for several different host providers. For the following, you'll
want to make use of the `docker-machine` command.
If you don't have `docker-machine` set up (tends to be the case with the newer Docker Desktop distributions),
you can follow these [quick instructions](#https://github.com/docker/machine/releases).

### [AWS](https://docs.docker.com/machine/examples/aws/)

These docs are fairly outdated/not applicable (in particular Step 1), so we recommend you instead navigate to
[AWS Management Console](#https://aws.amazon.com/) > IAM > Users > Add Users.
At the end of this, you will receive an ID and a secret key, which you can use to
populate a credentials file located at `~/.aws/credentials` of the form:

```
[default]
aws_access_key_id = AKIAUFP3REHUWAIOP6NK
aws_secret_access_key = *******
```

You can then proceed with Step 2 onwards, but instead of naming the machine `docker-sandbox`
as in those examples, let's name ours `ddx-auditor` as
shown below.

You can provision an EC2 instance with the following command:

```bash
docker-machine create --driver amazonec2 --amazonec2-region us-west-1 ddx-auditor
```
** If this results in a pre-check subnet failure, you can usually get around this
by specifying a different region, such as `us-east-1`

This command will spin up a new instance on your cloud provider, pre-installed
with Docker so that it's ready-to-use with the the `docker-machine` command.
Once the command completes, let's make sure the machine exists:

```bash
docker-machine ls
```

You should see something like:

```bash
ddx-auditor     -        amazonec2   Running   tcp://162.31.121.332:2376            v18.09.7
```

Now comes the Docker Machine magic. By running the following commands, we can
ask Docker Machine to let us execute any Docker command in our local shell AS IF
we were executing them directly on the `ddx-auditor` machine:

```bash
eval $(docker-machine env ddx-auditor)
```

We are now ready to spin up our DerivaDEX auditor(s) and trading bot(s).
To accomplish this, execute the docker compose file by running the command
./compose-generated.bash up -d`.

You can tear down the deployment by running the command `./compose-generated.bash down`.
If you regenerate configuration files with `compose_generator.py`, you can remove
any orphan containers with the `./compose-generated.bash down --remove-orphans`.

If you want to inspect the logs for the auditor and mm services, you need to do the following:

```bash
docker logs -f <auditor-container-name>
docker logs -f <mm-container-name>
```

If you need to see the IP address of the auditor, you can use the following command:

```bash
docker-machine ip ddx-auditor
```

### [DigitalOcean](https://docs.docker.com/machine/examples/ocean/)

### [Azure](https://docs.docker.com/cloud/aci-integration/)

### Kubernetes deployment

A very common pattern often requested is to deploy the auditor as a service running on
a remote Kubernetes cluster. We can make great use of the Docker Compose workflow we've set
up here, but need to define Kubernetes objects for the Compose definitions we have using `kompose`.
The steps have largely been taken from [Digital Ocean's guide](https://www.digitalocean.com/community/tutorials/how-to-migrate-a-docker-compose-workflow-to-kubernetes), but we outline them here as well
for convenience:

1. As discussed [above](#configuring-auditors-and-market-making-bots), first
ensure that you are passing in a config file where `auditorDeployments.isKube` is `true`, and
also set `mmDeployments` to be an empty list (`[]`).

2. Set up a Kubernetes cluster using [Digital Ocean](https://www.digitalocean.com/products/kubernetes/), [AWS](#https://docs.aws.amazon.com/eks/latest/userguide/getting-started-eksctl.html), or another provider

3. Install the Kubernetes command line tool [`kubectl`](#https://kubernetes.io/docs/reference/kubectl/) with these [instructions](https://kubernetes.io/docs/tasks/tools/)

4. Install the Docker Compose to Kubernetes migration tool `kompose` with these [instructions](https://github.com/kubernetes/kompose/blob/master/README.md#installation)

5. Run the `compose-generator.py` as discussed in the above [section](#configuring-auditors-and-market-making-bots) with:
`python compose_generator.py --config $CONFIG`. Be sure that the config file has no more than 1 auditor deployment
and that the `isKube` flag is set to `true`.
   
6. Create Kubernetes objects with: `kubectl create -f auditor-service.yaml,auditor-deployment.yaml,env-generated-configmap.yaml,secret.yaml`. This step
could take up to 10 minutes at times.

7. View active pods with: `kubectl get pods` and follow the logs with `kubectl logs -f <pod_name>`

You should have a DerivaDEX auditor successfully running on a remote Kubernetes cluster now with a secure private key!


# Building and publishing docker images (for contributors)

The docker images can be rebuilt by utilizing the dockerfiles in the root of this repo.
Make sure to update `$TAG` with the appropriate version number.

```bash
# Update this with the appropriate tag!
TAG=1.0.0

# Build the images
docker build -t ddx-auditor -f Dockerfile.auditor .
docker build -t ddx-mm -f Dockerfile.mm .
docker build -t ddx-mt -f Dockerfile.mt .

# Tag the images
docker tag ddx-auditor derivadex/ddx-auditor:$TAG
docker tag ddx-mm derivadex/ddx-mm:$TAG
docker tag ddx-mt derivadex/ddx-mt:$TAG

# Push the tags to dockerhub
docker push derivadex/ddx-auditor:$TAG
docker push derivadex/ddx-mm:$TAG
docker push derivadex/ddx-mt:$TAG
```
