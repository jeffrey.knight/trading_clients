#!/bin/bash

docker-compose --env-file .env.generated -f docker-compose.generated.yml "$@"
