#!/bin/bash --login

set -euo pipefail

CONFIG=./.env

# Clear and recreate the config file.
if [[ -f "$CONFIG" ]]
then
    rm "$CONFIG"
fi
touch "$CONFIG"

# Write the provided parameters to the config file.
echo "[deployment-environment-specs]" >> "$CONFIG"
echo "staging-env=$STAGING_ENV" >> "$CONFIG"

# Execute the auditor with the provided parameters.
PYTHON_LOG="$PYTHON_LOG" python ./auditor/auditor_driver.py --config "$CONFIG"
