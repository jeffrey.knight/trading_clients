"""
Sample DerivaDEX market making and Auditor bot
"""

import asyncio
import logging
import os
import random
from typing import List, Optional

import configargparse
from decimal import Decimal

from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from trading_clients.auditor.state.identifiers.position_identifier import (
    PositionIdentifier,
)
from trading_clients.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from trading_clients.auditor.state.identifiers.trader_identifier import TraderIdentifier
from trading_clients.auditor.transactions.identifiers.fill_identifier import (
    FillIdentifier,
)
from trading_clients.auditor.transactions.identifiers.price_checkpoint_identifier import (
    PriceCheckpointIdentifier,
)
from trading_clients.auditor.transactions.price_checkpoint import PriceCheckpoint
from trading_clients.contract_addresses import contract_addresses
from trading_clients.helpers.cancel_intent import CancelIntent
from trading_clients.client.derivadex_client import DerivaDEXClient
from trading_clients.helpers.order_intent import OrderIntent
from trading_clients.helpers.utils import round_to_unit, get_deployment_from_staging_env

fmt_str = "[%(asctime)s] %(levelname)s @ line %(lineno)d: %(message)s"
log_level = os.environ.get("PYTHON_LOG").upper() if "PYTHON_LOG" in os.environ else 100
logging.basicConfig(level=log_level, format=fmt_str)
logger = logging.getLogger(__name__)


def create_taker_market_order_intent(
    derivadex_client: DerivaDEXClient, symbol: str, side: str, quantity: Decimal,
) -> OrderIntent:
    """
    Create a market order

    Parameters
    ----------
    derivadex_client : DerivaDEXClient
        DerivaDEX client wrapper
    symbol : str
        Market symbol
    side : str
        Order side - "Bid" or "Ask"
    quantity : Decimal
        Size to submit

    Returns
    -------
    OrderIntent
        Signed order intent to be placed
    """

    quantity = round_to_unit(
        Decimal(str(random.uniform(0.1, float(quantity * Decimal("2"))))),
        Decimal("0.1"),
    )

    quantity = Decimal("20")

    return OrderIntent(
        f"0x00{derivadex_client.web3_account.address[2:]}",
        symbol,
        "main",
        side,
        "Market",
        f"0x{derivadex_client.get_encoded_nonce()}",
        quantity,
        Decimal("0"),
        Decimal("0"),
    )


async def place_order(
    derivadex_client: DerivaDEXClient,
    symbol: str,
    long_likelihood: Decimal,
    quantity: Decimal,
):
    """
    Place new orders around a base price

    Parameters
    ----------
    derivadex_client : DerivaDEXClient
        DerivaDEX client wrapper
    symbol : str
        Market symbol
    long_likelihood : Decimal
        Specifies the likelihood of going long [0=never, 1=always]
    quantity : Decimal
        Size to submit
    """

    side = "Bid" if random.random() < long_likelihood else "Ask"
    order_intent = create_taker_market_order_intent(
        derivadex_client, symbol, side, quantity
    )

    logging.info(
        f"ORDER: posting market order ({order_intent.side}) order for ({order_intent.amount})"
    )
    await derivadex_client.create_orders([order_intent])


async def deposit_if_necessary(
    derivadex_client: DerivaDEXClient,
    collateral_address: str,
    deposit_minimum: Decimal,
    deposit_amount: Decimal,
):
    strategy_leaf = derivadex_client.strategy_tracker.strategies[
        (
            f"0x00{derivadex_client.web3_account.address.lower()[2:]}",
            f"{StrategyIdentifier.generate_strategy_id_hash('main').hex()}",
        )
    ]

    deposited = False
    while (
        isinstance(strategy_leaf, Empty)
        or collateral_address not in strategy_leaf.free_collateral
        or strategy_leaf.free_collateral[collateral_address] < deposit_minimum
    ):
        if not deposited:
            await derivadex_client.deposit(collateral_address, deposit_amount)
            deposited = True

        logging.info("DEPOSIT: must deposit sufficient capital")
        await asyncio.sleep(5)
        strategy_leaf = derivadex_client.strategy_tracker.strategies[
            (
                f"0x00{derivadex_client.web3_account.address.lower()[2:]}",
                f"{StrategyIdentifier.generate_strategy_id_hash('main').hex()}",
            )
        ]
    if deposited:
        logging.info("DEPOSIT: sufficient collateral has been deposited")


async def main(
    symbol: str,
    long_likelihood: Decimal,
    quantity: Decimal,
    deposit_minimum: Decimal,
    deposit_amount: Decimal,
    sleep_rate: int,
    auditor_host: str,
    rpc_url: str,
    staging_env: str,
    order_book_symbols: List[str],
    trader_identifiers: List[TraderIdentifier],
    strategy_identifiers: List[StrategyIdentifier],
    position_identifiers: List[PositionIdentifier],
    book_order_identifiers: List[BookOrderIdentifier],
    price_checkpoint_identifiers: List[PriceCheckpointIdentifier],
    fill_identifiers: List[FillIdentifier],
    private_key: str = None,
    mnemonic: str = None,
):
    """
    Main entry point for sample market making strategy

    Parameters
    ----------
    strategy : str
        Trading strategy type (MAKER | TAKER | TEST | FUZZED)
    symbol : str
        Market symbol
    long_likelihood : Decimal
        Specifies the likelihood of going long [0=never, 1=always]
    quantity : Decimal
        Size to submit
    deposit_minimum : int
        Minimum collateral before a new deposit will be made
    deposit_amount : int
        Collateral amount deposited automatically when appropriate
    sleep_rate : int
        Refresh rate (s)
    auditor_host : str
        Host URL for the auditor
    rpc_url : str
        RPC URL for ETH connections
    staging_env : str
        Staging environment (test || pre || alpha || beta)
    order_book_symbols : List[str]
        Market symbols to track order books
    trader_identifiers : List[TraderIdentifier]
        Trader identifiers to track Trader data
    strategy_identifiers : List[StrategyIdentifier]
        Strategy identifiers to track Strategy data
    position_identifiers : List[PositionIdentifier]
        Position identifiers to track Position data
    book_order_identifiers : List[BookOrderIdentifier]
        BookOrder identifiers to track BookOrder data
    price_checkpoint_identifiers : List[PriceCheckpointIdentifier]
        PriceCheckpoint identifiers to track PriceCheckpoint txs
    fill_identifiers : List[FillIdentifier]
        FillIdentifier identifiers to track Fill txs
    private_key : str
        Ethereum private key for user, corresponding to the address
        used to deposit funds and sign messages
    mnemonic : str
        Ethereum mnemonic for user, corresponding to the address
        used to deposit funds and sign messages

    """

    deployment = get_deployment_from_staging_env(staging_env)
    chain_id = contract_addresses[deployment]["chainId"]
    verifying_contract = contract_addresses[deployment]["addresses"]["derivaDEXAddress"]
    collateral_address = contract_addresses[deployment]["addresses"]["usdcAddress"]

    derivadex_client = DerivaDEXClient(
        auditor_host,
        rpc_url,
        staging_env,
        chain_id,
        verifying_contract,
        order_book_symbols,
        trader_identifiers,
        strategy_identifiers,
        position_identifiers,
        book_order_identifiers,
        price_checkpoint_identifiers,
        fill_identifiers,
        private_key,
        mnemonic,
    )

    await derivadex_client.start_network()
    await asyncio.sleep(5.0)

    # Sample REST query
    await derivadex_client.get_strategy(
        f"0x00{derivadex_client.web3_account.address.lower()[2:]}", "main",
    )

    # Sample WS data feeds
    # print("OrderBooks", derivadex_client.order_book_tracker.order_books)
    # print("Traders", derivadex_client.trader_tracker.traders)
    # print("Strategies", derivadex_client.strategy_tracker.strategies)
    # print("Positions", derivadex_client.position_tracker.positions)
    # print("BookOrders", derivadex_client.book_order_tracker.book_orders)
    # print("PriceCheckpoints", derivadex_client.price_checkpoint_tracker.latest_price_checkpoints)

    while True:
        await deposit_if_necessary(
            derivadex_client, collateral_address, deposit_minimum, deposit_amount
        )

        latest_price_checkpoint = derivadex_client.price_checkpoint_tracker.latest_price_checkpoints[
            symbol
        ]
        if latest_price_checkpoint:
            await place_order(
                derivadex_client, symbol, long_likelihood, quantity,
            )

        await asyncio.sleep(0.5)


if __name__ == "__main__":
    p = configargparse.ArgParser()
    p.add_argument(
        "-c",
        "--config",
        required=True,
        is_config_file_arg=True,
        help="config file path",
    )
    p.add_argument(
        "-l", "--long-likelihood", required=True, help="long likelihood [0, 1]",
    )
    p.add_argument(
        "-q", "--quantity", required=True, help="size to submit",
    )
    p.add_argument(
        "-d",
        "--deposit-minimum",
        required=True,
        help="minimum collateral before a new deposit will be made",
    )
    p.add_argument(
        "-da",
        "--deposit-amount",
        required=True,
        help="collateral amount to deposit automatically when appropriate",
    )
    p.add_argument(
        "-s", "--sleep-rate", required=True, help="sleep rate (s)",
    )
    p.add_argument(
        "-z", "--auditor-host", required=True, help="Host URL for the auditor",
    )
    p.add_argument(
        "-u", "--rpc-url", required=True, help="RPC URL for ETH connections",
    )
    p.add_argument(
        "-e",
        "--staging-env",
        required=True,
        help="staging environment (test || pre || alpha || beta)",
    )
    p.add_argument(
        "-k",
        "--private-key",
        help="Ethereum private key for user, corresponding to the address used to deposit funds and sign messages",
    )
    p.add_argument(
        "-m",
        "--mnemonic",
        help="Ethereum mnemonic for user, corresponding to the address used to deposit funds and sign messages",
    )
    p.add_argument(
        "-ot", "--order-book-symbols", help="Order book symbols", action="append"
    )
    p.add_argument("-tt", "--trader-topics", help="Trader topics", action="append")
    p.add_argument("-st", "--strategy-topics", help="Strategy topics", action="append")
    p.add_argument("-pt", "--position-topics", help="Position topics", action="append")
    p.add_argument(
        "-bt", "--book-order-topics", help="BookOrder topics", action="append"
    )
    p.add_argument(
        "-pct",
        "--price-checkpoint-topics",
        help="PriceCheckpoint topics",
        action="append",
    )
    p.add_argument(
        "-ft", "--fill-topics", help="Fill topics", action="append",
    )

    options = p.parse_args()

    print(options)
    print("----------")
    print(p.format_help())
    print("----------")
    print(p.format_values())

    if sum([val is None for val in [options.private_key, options.mnemonic]]) != 1:
        raise Exception(
            "Must set only one of either private_key and mnemonic, but not both"
        )

    trader_identifiers = [
        TraderIdentifier.decode_topic_string_into_cls(trader_topic)
        for trader_topic in options.trader_topics
    ]
    strategy_identifiers = [
        StrategyIdentifier.decode_topic_string_into_cls(strategy_topic)
        for strategy_topic in options.strategy_topics
    ]
    position_identifiers = [
        PositionIdentifier.decode_topic_string_into_cls(position_topic)
        for position_topic in options.position_topics
    ]
    book_order_identifiers = [
        BookOrderIdentifier.decode_topic_string_into_cls(book_order_topic)
        for book_order_topic in options.book_order_topics
    ]
    price_checkpoint_identifiers = [
        PriceCheckpointIdentifier.decode_topic_string_into_cls(price_checkpoint_topic)
        for price_checkpoint_topic in options.price_checkpoint_topics
    ]
    fill_identifiers = [
        FillIdentifier.decode_topic_string_into_cls(fill_topic)
        for fill_topic in options.fill_topics
    ]

    asyncio.run(
        main(
            "ETHPERP",
            Decimal(options.long_likelihood),
            Decimal(options.quantity),
            Decimal(options.deposit_minimum),
            Decimal(options.deposit_amount),
            int(options.sleep_rate),
            options.auditor_host,
            options.rpc_url,
            options.staging_env,
            options.order_book_symbols,
            trader_identifiers,
            strategy_identifiers,
            position_identifiers,
            book_order_identifiers,
            price_checkpoint_identifiers,
            fill_identifiers,
            options.private_key,
            options.mnemonic,
        )
    )
