# Setup

## From source

To run the market maker, follow these steps:

1. If you don't already have it, we recommend setting up Anaconda/Python(>3) on your machine

2. Initialize and activate a Conda environment (located at the root level of the repo) from which you will run the market maker: `conda env create -f environment.yml && conda activate derivadex`

3. Set your PYTHONPATH: `export PYTHONPATH="${PYTHONPATH}:/your/full/path/upto/but/not/including/trading_clients"`

4. Navigate to the `sample_strategies` subdirectory and create an `.env-mm` file using the template: `cp .env.template .env-mm`

5. Run the Auditor with: `PYTHON_LOG=verbose python sample_strategies/sample_mm_auditor_bot.py --config ".env-mm"`

You will immediately see logging messages (should be lots of green) with state initialized and transactions streaming through successfully.

## Using docker

A dockerized version of the market making bot can be run standalone by following
the commands below. For an example of running the bot as a docker-compose
service locally or on a remote machine, refer to the root-level README.md.

```
# Download the ddx-auditor docker image
docker pull derivadex/ddx-mm:1.0.8

# Run the docker container with a configuration file. We connect to the `auditor-net`
# network, which is a user defined network that the running auditor should be using.
docker run --name "ddx-mm" -d --env-file .env --network auditor-net derivadex/ddx-mm:1.0.8
```
