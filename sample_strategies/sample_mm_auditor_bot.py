"""
Sample DerivaDEX market making and Auditor bot
"""

import asyncio
import logging
import os
import random
from typing import List, Optional

import configargparse
from decimal import Decimal

from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from trading_clients.auditor.state.identifiers.position_identifier import (
    PositionIdentifier,
)
from trading_clients.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from trading_clients.auditor.state.identifiers.trader_identifier import TraderIdentifier
from trading_clients.auditor.transactions.identifiers.fill_identifier import (
    FillIdentifier,
)
from trading_clients.auditor.transactions.identifiers.price_checkpoint_identifier import (
    PriceCheckpointIdentifier,
)
from trading_clients.auditor.transactions.price_checkpoint import PriceCheckpoint
from trading_clients.contract_addresses import contract_addresses
from trading_clients.helpers.cancel_intent import CancelIntent
from trading_clients.client.derivadex_client import DerivaDEXClient
from trading_clients.helpers.order_intent import OrderIntent
from trading_clients.helpers.utils import round_to_unit, get_deployment_from_staging_env

fmt_str = "[%(asctime)s] %(levelname)s @ line %(lineno)d: %(message)s"
log_level = os.environ.get("PYTHON_LOG").upper() if "PYTHON_LOG" in os.environ else 100
logging.basicConfig(level=log_level, format=fmt_str)
logger = logging.getLogger(__name__)


def create_maker_limit_order_intent(
    derivadex_client: DerivaDEXClient,
    symbol: str,
    base_px: Decimal,
    side: str,
    price_offset_index: int,
    price_offset: Decimal,
    quantity_per_level: Decimal,
) -> OrderIntent:
    """
    Create a maker limit order

    Parameters
    ----------
    derivadex_client : DerivaDEXClient
        DerivaDEX client wrapper
    symbol : str
        Market symbol
    base_px : Decimal
        Base price to adjust with offsets
    side : str
        Order side - "Bid" or "Ask"
    price_offset_index : int
        Level offset to determine price adjustment to base price
    price_offset : Decimal
        Specifies price between quoted levels
    quantity_per_level : Decimal
        Size to post for each quoted level

    Returns
    -------
    OrderIntent
        Signed order intent to be placed
    """

    price = round_to_unit(
        base_px * (Decimal("1") - price_offset * (price_offset_index + Decimal("1")))
        if side == "Bid"
        else base_px
        * (Decimal("1") + price_offset * (price_offset_index + Decimal("1"))),
        Decimal("0.1") if symbol == "ETHPERP" else Decimal("1"),
    )

    quantity = round_to_unit(
        Decimal(
            str(
                random.uniform(
                    float(quantity_per_level - quantity_per_level / Decimal("2")),
                    float(quantity_per_level + quantity_per_level / Decimal("2")),
                )
            )
        ),
        Decimal("0.1"),
    )

    return OrderIntent(
        f"0x00{derivadex_client.web3_account.address[2:]}",
        symbol,
        "main",
        side,
        "Limit",
        f"0x{derivadex_client.get_encoded_nonce()}",
        quantity,
        price,
        Decimal("0"),
    )


async def should_revisit_orders(
    price_checkpoint: PriceCheckpoint,
    ref_px_deviation_to_replace_orders: Decimal,
    ref_mark_px: Optional[Decimal] = None,
) -> bool:
    """
    Checks whether bot should revisit/refresh orders

    Parameters
    ----------
    price_checkpoint : Decimal
        Latest mark price for market
    ref_px_deviation_to_replace_orders : Decimal
        Specifies how much the reference price must change to
        refresh orders
    ref_mark_px : Decimal
        Reference mark price for market

    Returns
    -------
    bool
        Whether bot should refresh orders
    """

    mark_price = price_checkpoint.mark_price
    if (
        ref_mark_px is None
        or abs((mark_price - ref_mark_px) / ref_mark_px)
        > ref_px_deviation_to_replace_orders
    ):
        logging.info(
            f"REFRESH: mark_price ({mark_price}) drifted from ref_mark_px ({ref_mark_px})"
        )
        return True
    return False


async def cancel_orders(derivadex_client: DerivaDEXClient):
    """
    Cancel active open orders

    Parameters
    ----------
    derivadex_client : DerivaDEXClient
        DerivaDEX client wrapper
    """

    # Create CancelIntents to submit to the API
    cancel_intents = []
    for (
        symbol,
        _,
        _,
        order_hash,
    ) in derivadex_client.book_order_tracker.book_orders.keys():
        logging.info(f"CANCEL: canceling order with hash ({order_hash})")
        cancel_intents.append(
            CancelIntent(
                symbol, order_hash, f"0x{derivadex_client.get_encoded_nonce()}",
            )
        )

    await derivadex_client.cancel_orders(cancel_intents)


async def place_orders(
    derivadex_client: DerivaDEXClient,
    symbol: str,
    base_px: Decimal,
    levels_to_quote: int,
    price_offset: Decimal,
    quantity_per_level: Decimal,
):
    """
    Place new orders around a base price

    Parameters
    ----------
    derivadex_client : DerivaDEXClient
        DerivaDEX client wrapper
    symbol : str
        Market symbol
    base_px : Decimal
        Base price to adjust with offsets
    levels_to_quote : int
        Number of levels on either side to quote
    price_offset : Decimal
        Specifies price between quoted levels
    quantity_per_level : Decimal
        Size to post for each quoted level
    """

    order_intents = []

    # Iterate through the levels to quote
    for level in range(levels_to_quote):
        # Create a new bid order intent for level
        bid_order_intent = create_maker_limit_order_intent(
            derivadex_client,
            symbol,
            base_px,
            "Bid",
            level,
            price_offset,
            quantity_per_level,
        )
        logging.info(
            f"POST: posting ({bid_order_intent.side}) order for ({bid_order_intent.amount}) @ ({bid_order_intent.price})"
        )
        order_intents.append(bid_order_intent)

        # Create a new ask order intent for level
        ask_order_intent = create_maker_limit_order_intent(
            derivadex_client,
            symbol,
            base_px,
            "Ask",
            level,
            price_offset,
            quantity_per_level,
        )
        logging.info(
            f"POST: posting ({ask_order_intent.side}) order for ({ask_order_intent.amount}) @ ({ask_order_intent.price})"
        )
        order_intents.append(ask_order_intent)

    # Submit created orders to the Auditor API for placement
    await derivadex_client.create_orders(order_intents)


async def deposit_if_necessary(
    derivadex_client: DerivaDEXClient,
    collateral_address: str,
    ddx_address: str,
    deposit_minimum: Decimal,
    deposit_amount: Decimal,
    faucet_address: str,
    faucet_private_key: str,
):
    strategy_leaf = derivadex_client.strategy_tracker.strategies[
        (
            f"0x00{derivadex_client.web3_account.address.lower()[2:]}",
            f"{StrategyIdentifier.generate_strategy_id_hash('main').hex()}",
        )
    ]

    deposited = False
    while (
        isinstance(strategy_leaf, Empty)
        or collateral_address not in strategy_leaf.free_collateral
        or strategy_leaf.free_collateral[collateral_address] < deposit_minimum
    ):
        if not deposited:
            await derivadex_client.mint(
                ddx_address, faucet_address, faucet_private_key, Decimal("100")
            )
            await asyncio.sleep(5)
            await derivadex_client.approve(collateral_address, deposit_amount)
            await asyncio.sleep(5)
            await derivadex_client.deposit(collateral_address, deposit_amount)
            await asyncio.sleep(5)
            await derivadex_client.approve_ddx(ddx_address, Decimal("100"))
            await asyncio.sleep(5)
            await derivadex_client.deposit_ddx(Decimal("100"))
            await asyncio.sleep(5)
            deposited = True

        logging.info("DEPOSIT: must deposit sufficient capital")
        await asyncio.sleep(5)
        strategy_leaf = derivadex_client.strategy_tracker.strategies[
            (
                f"0x00{derivadex_client.web3_account.address.lower()[2:]}",
                f"{StrategyIdentifier.generate_strategy_id_hash('main').hex()}",
            )
        ]
    if deposited:
        logging.info("DEPOSIT: sufficient collateral has been deposited")


async def main(
    strategy: str,
    symbol: str,
    ref_px_deviation_to_replace_orders: Decimal,
    price_offset: Decimal,
    levels_to_quote: int,
    quantity_per_level: Decimal,
    deposit_minimum: Decimal,
    deposit_amount: Decimal,
    sleep_rate: int,
    auditor_host: str,
    rpc_url: str,
    staging_env: str,
    order_book_symbols: List[str],
    trader_identifiers: List[TraderIdentifier],
    strategy_identifiers: List[StrategyIdentifier],
    position_identifiers: List[PositionIdentifier],
    book_order_identifiers: List[BookOrderIdentifier],
    price_checkpoint_identifiers: List[PriceCheckpointIdentifier],
    fill_identifiers: List[FillIdentifier],
    private_key: str = None,
    mnemonic: str = None,
):
    """
    Main entry point for sample market making strategy

    Parameters
    ----------
    strategy : str
        Trading strategy type (MAKER | TAKER | TEST | FUZZED)
    symbol : str
        Market symbol
    ref_px_deviation_to_replace_orders : Decimal
        Specifies how much the reference price must change to
        refresh orders
    price_offset : Decimal
        Specifies price between quoted levels
    levels_to_quote : int
        Number of levels on either side to quote
    quantity_per_level : Decimal
        Size to post for each quoted level
    deposit_minimum : int
        Minimum collateral before a new deposit will be made
    deposit_amount : int
        Collateral amount deposited automatically when appropriate
    sleep_rate : int
        Refresh rate (s)
    auditor_host : str
        Host URL for the auditor
    rpc_url : str
        RPC URL for ETH connections
    staging_env : str
        Staging environment (test || pre || alpha || beta)
    order_book_symbols : List[str]
        Market symbols to track order books
    trader_identifiers : List[TraderIdentifier]
        Trader identifiers to track Trader data
    strategy_identifiers : List[StrategyIdentifier]
        Strategy identifiers to track Strategy data
    position_identifiers : List[PositionIdentifier]
        Position identifiers to track Position data
    book_order_identifiers : List[BookOrderIdentifier]
        BookOrder identifiers to track BookOrder data
    price_checkpoint_identifiers : List[PriceCheckpointIdentifier]
        PriceCheckpoint identifiers to track PriceCheckpoint txs
    fill_identifiers : List[FillIdentifier]
        FillIdentifier identifiers to track Fill txs
    private_key : str
        Ethereum private key for user, corresponding to the address
        used to deposit funds and sign messages
    mnemonic : str
        Ethereum mnemonic for user, corresponding to the address
        used to deposit funds and sign messages

    """

    deployment = get_deployment_from_staging_env(staging_env)
    chain_id = contract_addresses[deployment]["chainId"]
    verifying_contract = contract_addresses[deployment]["addresses"]["derivaDEXAddress"]
    collateral_address = contract_addresses[deployment]["addresses"]["usdcAddress"]
    ddx_address = contract_addresses[deployment]["addresses"]["ddxAddress"]
    faucet_address = contract_addresses[deployment]["faucet"]["address"]
    faucet_private_key = contract_addresses[deployment]["faucet"]["privateKey"]
    derivadex_client = DerivaDEXClient(
        auditor_host,
        rpc_url,
        staging_env,
        chain_id,
        verifying_contract,
        order_book_symbols,
        trader_identifiers,
        strategy_identifiers,
        position_identifiers,
        book_order_identifiers,
        price_checkpoint_identifiers,
        fill_identifiers,
        private_key,
        mnemonic,
    )

    await derivadex_client.start_network()
    await asyncio.sleep(5.0)

    ref_mark_px = None

    # Sample REST query
    await derivadex_client.get_strategy(
        f"0x00{derivadex_client.web3_account.address.lower()[2:]}", "main",
    )

    # Sample WS data feeds
    # print("OrderBooks", derivadex_client.order_book_tracker.order_books)
    # print("Traders", derivadex_client.trader_tracker.traders)
    # print("Strategies", derivadex_client.strategy_tracker.strategies)
    # print("Positions", derivadex_client.position_tracker.positions)
    # print("BookOrders", derivadex_client.book_order_tracker.book_orders)
    # print("PriceCheckpoints", derivadex_client.price_checkpoint_tracker.latest_price_checkpoints)

    while True:
        await deposit_if_necessary(
            derivadex_client,
            collateral_address,
            ddx_address,
            deposit_minimum,
            deposit_amount,
            faucet_address,
            faucet_private_key,
        )

        latest_price_checkpoint = derivadex_client.price_checkpoint_tracker.latest_price_checkpoints[
            symbol
        ]
        if latest_price_checkpoint and await should_revisit_orders(
            latest_price_checkpoint, ref_px_deviation_to_replace_orders, ref_mark_px
        ):
            ref_mark_px = latest_price_checkpoint.mark_price

            await cancel_orders(derivadex_client)

            # Random 20pt offset in either direction to trigger matches
            if strategy == "MAKER":
                base_px = latest_price_checkpoint.index_price
            elif strategy == "TAKER":
                base_px = Decimal(
                    str(
                        random.uniform(
                            float(latest_price_checkpoint.index_price - Decimal("20")),
                            float(latest_price_checkpoint.index_price + Decimal("20")),
                        )
                    )
                )
            else:
                base_px = Decimal(
                    str(
                        random.uniform(
                            float(latest_price_checkpoint.index_price + Decimal("50")),
                            float(latest_price_checkpoint.index_price + Decimal("100")),
                        )
                    )
                )

            # Black out period, bot stops providing liquidity
            if strategy == "FUZZED" and random.random() < 0.2:
                logging.info(f"BLACK_OUT PERIOD...")
                await asyncio.sleep(1.0)

            await place_orders(
                derivadex_client,
                symbol,
                base_px,
                # latest_price_checkpoint.index_price,
                levels_to_quote,
                price_offset,
                quantity_per_level,
            )

        await asyncio.sleep(sleep_rate)


if __name__ == "__main__":
    p = configargparse.ArgParser()
    p.add_argument(
        "-c",
        "--config",
        required=True,
        is_config_file_arg=True,
        help="config file path",
    )
    p.add_argument(
        "-sb", "--symbol", required=True, help="market symbol",
    )
    p.add_argument(
        "-r",
        "--ref-px-deviation-to-replace-orders",
        required=True,
        help="specifies how much the reference price must change to refresh orders",
    )
    p.add_argument(
        "-p",
        "--price-offset",
        required=True,
        help="price offset between adjacent orders",
    )
    p.add_argument(
        "-l",
        "--levels-to-quote",
        required=True,
        help="number of levels on either side to quote",
    )
    p.add_argument(
        "-q",
        "--quantity-per-level",
        required=True,
        help="size to post for each quoted level",
    )
    p.add_argument(
        "-d",
        "--deposit-minimum",
        required=True,
        help="minimum collateral before a new deposit will be made",
    )
    p.add_argument(
        "-da",
        "--deposit-amount",
        required=True,
        help="collateral amount to deposit automatically when appropriate",
    )
    p.add_argument(
        "-s", "--sleep-rate", required=True, help="sleep rate (s)",
    )
    p.add_argument(
        "-z", "--auditor-host", required=True, help="Host URL for the auditor",
    )
    p.add_argument(
        "-u", "--rpc-url", required=True, help="RPC URL for ETH connections",
    )
    p.add_argument(
        "-e",
        "--staging-env",
        required=True,
        help="staging environment (test || pre || alpha || beta)",
    )
    p.add_argument(
        "-ts",
        "--trading-strategy",
        required=True,
        help='market making strategy ("PRODUCTION" or "TEST")',
    )
    p.add_argument(
        "-k",
        "--private-key",
        help="Ethereum private key for user, corresponding to the address used to deposit funds and sign messages",
    )
    p.add_argument(
        "-m",
        "--mnemonic",
        help="Ethereum mnemonic for user, corresponding to the address used to deposit funds and sign messages",
    )
    p.add_argument(
        "-ot", "--order-book-symbols", help="Order book symbols", action="append"
    )
    p.add_argument("-tt", "--trader-topics", help="Trader topics", action="append")
    p.add_argument("-st", "--strategy-topics", help="Strategy topics", action="append")
    p.add_argument("-pt", "--position-topics", help="Position topics", action="append")
    p.add_argument(
        "-bt", "--book-order-topics", help="BookOrder topics", action="append"
    )
    p.add_argument(
        "-pct",
        "--price-checkpoint-topics",
        help="PriceCheckpoint topics",
        action="append",
    )
    p.add_argument(
        "-ft", "--fill-topics", help="Fill topics", action="append",
    )

    options = p.parse_args()

    print(options)
    print("----------")
    print(p.format_help())
    print("----------")
    print(p.format_values())

    if sum([val is None for val in [options.private_key, options.mnemonic]]) != 1:
        raise Exception(
            "Must set only one of either private_key and mnemonic, but not both"
        )

    if options.trading_strategy not in ["MAKER", "TAKER", "TEST", "FUZZED"]:
        raise Exception(
            'Trading strategy must be one of ["MAKER", "TAKER", "TEST", "FUZZED"]'
        )

    trader_identifiers = [
        TraderIdentifier.decode_topic_string_into_cls(trader_topic)
        for trader_topic in options.trader_topics
    ]
    strategy_identifiers = [
        StrategyIdentifier.decode_topic_string_into_cls(strategy_topic)
        for strategy_topic in options.strategy_topics
    ]
    position_identifiers = [
        PositionIdentifier.decode_topic_string_into_cls(position_topic)
        for position_topic in options.position_topics
    ]
    book_order_identifiers = [
        BookOrderIdentifier.decode_topic_string_into_cls(book_order_topic)
        for book_order_topic in options.book_order_topics
    ]
    price_checkpoint_identifiers = [
        PriceCheckpointIdentifier.decode_topic_string_into_cls(price_checkpoint_topic)
        for price_checkpoint_topic in options.price_checkpoint_topics
    ]
    fill_identifiers = [
        FillIdentifier.decode_topic_string_into_cls(fill_topic)
        for fill_topic in options.fill_topics
    ]

    asyncio.run(
        main(
            options.trading_strategy,
            options.symbol,
            Decimal(options.ref_px_deviation_to_replace_orders),
            Decimal(options.price_offset),
            int(options.levels_to_quote),
            Decimal(options.quantity_per_level),
            Decimal(options.deposit_minimum),
            Decimal(options.deposit_amount),
            int(options.sleep_rate),
            options.auditor_host,
            options.rpc_url,
            options.staging_env,
            options.order_book_symbols,
            trader_identifiers,
            strategy_identifiers,
            position_identifiers,
            book_order_identifiers,
            price_checkpoint_identifiers,
            fill_identifiers,
            options.private_key,
            options.mnemonic,
        )
    )
