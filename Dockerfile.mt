FROM continuumio/miniconda3:4.10.3

EXPOSE 8765 8766

# Install required GCC and Linux headers dependencies required to build the
# conda environment.
RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc libc-dev musl-dev

# Copy the source code into the image.
COPY ./ /src/trading_clients
WORKDIR /src/trading_clients/

# Get the application specific environment variables.
ENV PYTHONPATH="${PYTHONPATH}:/src/"
ENV STAGING_ENV=$STAGING_ENV \
    RPC_URL=$RPC_URL \
    PRIVATE_KEY=$PRIVATE_KEY \
    MNEMONIC=$MNEMONIC \
    PYTHON_LOG=$PYTHON_LOG \
    DEPOSIT_MINIMUM=$DEPOSIT_MINIMUM \
    DEPOSIT_AMOUNT=$DEPOSIT_AMOUNT \
    LONG_LIKELIHOOD=LONG_LIKELIHOOD \
    QUANTITY=QUANTITY \
    SLEEP_RATE=$SLEEP_RATE \
    ORDER_BOOK_SYMBOLS=$ORDER_BOOK_SYMBOLS \
    TRADER_TOPICS=$TRADER_TOPICS \
    STRATEGY_TOPICS=$STRATEGY_TOPICS \
    POSITION_TOPICS=$POSITION_TOPICS \
    BOOK_ORDER_TOPICS=$BOOK_ORDER_TOPICS \
    PRICE_CHECKPOINT_TOPICS=$PRICE_CHECKPOINT_TOPICS \
    FILL_TOPICS=$FILL_TOPICS

# Create a conda profile for the auditor's environment
RUN conda update -n base -c defaults conda \
    && conda env create -f environment.yml

# Make RUN commands use the new environment:
RUN echo "conda activate derivadex" >> ~/.bashrc

# Run the market taking bot.
RUN chmod a+rx ./run-mt-bot.bash
ENTRYPOINT ["./run-mt-bot.bash"]
