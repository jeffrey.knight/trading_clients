#!/bin/bash --login

set -euo pipefail

CONFIG=./.env

# Clear and recreate the config file.
if [[ -f "$CONFIG" ]]
then
    rm "$CONFIG"
fi
touch "$CONFIG"

# Write the market maker specs to the config file.
echo "[mm-specs]" >> "$CONFIG"
echo "symbol=$SYMBOL" >> "$CONFIG"
echo "ref-px-deviation-to-replace-orders=$REF_PX_DEVIATION_TO_REPLACE_ORDERS" >> "$CONFIG"
echo "price-offset=$PRICE_OFFSET" >> "$CONFIG"
echo "levels-to-quote=$LEVELS_TO_QUOTE" >> "$CONFIG"
echo "quantity-per-level=$QUANTITY_PER_LEVEL" >> "$CONFIG"
echo "deposit-minimum=$DEPOSIT_MINIMUM" >> "$CONFIG"
echo "deposit-amount=$DEPOSIT_AMOUNT" >> "$CONFIG"
echo "sleep-rate=$SLEEP_RATE" >> "$CONFIG"
echo "order-book-symbols=$ORDER_BOOK_SYMBOLS" >> "$CONFIG"
echo "trader-topics=$TRADER_TOPICS" >> "$CONFIG"
echo "strategy-topics=$STRATEGY_TOPICS" >> "$CONFIG"
echo "position-topics=$POSITION_TOPICS" >> "$CONFIG"
echo "book-order-topics=$BOOK_ORDER_TOPICS" >> "$CONFIG"
echo "price-checkpoint-topics=$PRICE_CHECKPOINT_TOPICS" >> "$CONFIG"
echo "fill-topics=$FILL_TOPICS" >> "$CONFIG"

echo "[deployment-environment-specs]" >> "$CONFIG"
echo "auditor-host=$AUDITOR_HOST" >> "$CONFIG"
echo "rpc-url=$RPC_URL" >> "$CONFIG"
echo "staging-env=$STAGING_ENV" >> "$CONFIG"
echo "trading-strategy=$TRADING_STRATEGY" >> "$CONFIG"
if [[ -n $PRIVATE_KEY ]]
then
    echo "private-key=$PRIVATE_KEY" >> "$CONFIG"
fi
if [[ -n $MNEMONIC ]]
then
    echo "mnemonic=$MNEMONIC" >> "$CONFIG"
fi

# Execute the auditor with the provided parameters.
PYTHON_LOG="$PYTHON_LOG" python ./sample_strategies/sample_mm_auditor_bot.py --config "$CONFIG"
