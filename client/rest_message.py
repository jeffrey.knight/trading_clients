"""
RestMessage module
"""

from enum import Enum


class RestMessageType(str, Enum):
    GET = "Get"
    SUCCESS = "Success"
    ERROR = "Error"


class RestMessage:
    def __init__(self, message_type: [RestMessageType, str], message_content):
        self.message_type = message_type
        self.message_content = message_content

    def repr_json(self):
        return {"t": self.message_type, "c": self.message_content}
