"""
DerivaDEX signature utils
"""

from eth_abi import encode_single
from eth_abi.utils.padding import zpad32_right
from eth_utils.crypto import keccak
from decimal import Decimal, ROUND_DOWN

from trading_clients.helpers.cancel_intent import CancelIntent
from trading_clients.helpers.order_intent import OrderIntent

eip191_header = b"\x19\x01"


def compute_eip712_domain_struct_hash(chain_id: int, verifying_contract: str):
    return keccak(
        keccak(
            b"EIP712Domain("
            + b"string name,"
            + b"string version,"
            + b"uint256 chainId,"
            + b"address verifyingContract"
            + b")"
        )
        + keccak(b"DerivaDEX")
        + keccak(b"1")
        + encode_single("uint256", chain_id)
        + encode_single("address", verifying_contract)
    )


def compute_eip712_create_order_hash(
    chain_id: int, verifying_contract: str, order_intent: OrderIntent
) -> str:
    # keccak-256 hash of the encoded schema for the place order command
    eip712_order_params_schema_hash = keccak(
        b"OrderParams("
        + b"address traderAddress,"
        + b"bytes32 symbol,"
        + b"bytes32 strategy,"
        + b"uint256 side,"
        + b"uint256 orderType,"
        + b"bytes32 nonce,"
        + b"uint256 amount,"
        + b"uint256 price,"
        + b"uint256 stopPrice"
        + b")"
    )

    # Ensure decimal value has no more than 18 decimals of precision
    def round_to_unit(val):
        return val.quantize(Decimal(".000000000000000001"), rounding=ROUND_DOWN)

    # Scale up to DDX grains format (i.e. multiply by 1e18)
    def to_base_unit_amount(val, decimals):
        return int(round_to_unit(val) * 10 ** decimals)

    # Convert order side string to int representation
    def order_side_to_int(order_side: str) -> int:
        if order_side == "Bid":
            return 0
        return 1

    # Convert order type string to int representation
    def order_type_to_int(order_type: str) -> int:
        if order_type == "Limit":
            return 0
        elif order_type == "Market":
            return 1
        return 2

    eip712_message_struct_hash = keccak(
        eip712_order_params_schema_hash
        + encode_single("address", f"0x{order_intent.trader_address[4:]}")
        + zpad32_right(
            len(order_intent.symbol).to_bytes(1, byteorder="little")
            + order_intent.symbol.encode("utf8")
        )
        + zpad32_right(
            len(order_intent.strategy).to_bytes(1, byteorder="little")
            + order_intent.strategy.encode("utf8")
        )
        + encode_single("uint256", order_side_to_int(order_intent.side))
        + encode_single("uint256", order_type_to_int(order_intent.order_type))
        + encode_single("bytes32", bytes.fromhex(order_intent.nonce[2:]))
        + encode_single("uint256", to_base_unit_amount(order_intent.amount, 18))
        + encode_single("uint256", to_base_unit_amount(order_intent.price, 18))
        + encode_single("uint256", to_base_unit_amount(order_intent.stop_price, 18))
    )

    # Converting bytes result to a hexadecimal string
    return keccak(
        eip191_header
        + compute_eip712_domain_struct_hash(chain_id, verifying_contract)
        + eip712_message_struct_hash
    ).hex()


def compute_eip712_cancel_order_hash(
    chain_id: int, verifying_contract: str, cancel_intent: CancelIntent
) -> str:
    # keccak-256 hash of the encoded schema for the place order command
    eip712_cancel_params_schema_hash = keccak(
        b"CancelOrderParams("
        + b"bytes32 symbol,"
        + b"bytes32 orderHash,"
        + b"bytes32 nonce"
        + b")"
    )

    eip712_message_struct_hash = keccak(
        eip712_cancel_params_schema_hash
        + zpad32_right(
            len(cancel_intent.symbol).to_bytes(1, byteorder="little")
            + cancel_intent.symbol.encode("utf8")
        )
        + encode_single("bytes32", bytes.fromhex(cancel_intent.order_hash[2:]))
        + encode_single("bytes32", bytes.fromhex(cancel_intent.nonce[2:]))
    )

    # Converting bytes result to a hexadecimal string
    return keccak(
        eip191_header
        + compute_eip712_domain_struct_hash(chain_id, verifying_contract)
        + eip712_message_struct_hash
    ).hex()
