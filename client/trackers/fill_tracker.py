"""
FillTracker module
"""

import asyncio
from typing import Optional, List
import websockets
import simplejson as json

from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.transactions.event_message import EventMessage
from trading_clients.auditor.transactions.fill import Fill
from trading_clients.auditor.transactions.identifiers.fill_identifier import (
    FillIdentifier,
)
from trading_clients.auditor.transactions.trade_fill import TradeFill
from trading_clients.auditor.utils import ComplexOutputEncoder
from trading_clients.client.asyncio_utils import empty_queue
from trading_clients.client.trackers.tracker import Tracker
from trading_clients.client.websocket_message import (
    WebsocketMessage,
    WebsocketMessageType,
)


class FillTracker(Tracker):
    """
    Defines a FillTracker
    """

    def __init__(
        self,
        fill_identifiers: Optional[List[FillIdentifier]] = None,
        ws_uri: str = None,
    ):
        """
        Initialize a FillTracker. A FillTracker is an async task
        that can send Fill-related data, or receive real-time
        Fills a user has subscribed to.

        Parameters
        ----------
        fill_identifiers: Optional[List[FillIdentifier]]
            List of FillIdentifier user is subscribing to
        ws_uri : str
           WebSocket URI
        """

        self.fill_identifiers = fill_identifiers
        self.ws_uri = ws_uri

        self.fill_stream_queue: asyncio.Queue[ItemMessage] = asyncio.Queue()

    def _reset(self):
        self.fill_stream_queue: asyncio.Queue[ItemMessage] = asyncio.Queue()
        empty_queue(self.fill_stream_queue)

    async def _consumer_handler(self):
        """
        Consume data from the Auditor after subscribing to the various
        Identifiers.
        """

        while True:
            try:
                async with websockets.connect(self.ws_uri, max_size=2 ** 32) as ws:
                    ws: websockets.WebSocketClientProtocol = ws
                    for fill_identifier in self.fill_identifiers:
                        subscribe_request = WebsocketMessage(
                            WebsocketMessageType.SUBSCRIBE,
                            fill_identifier.topic_string,
                        )
                        subscribe_request_json = ComplexOutputEncoder().encode(
                            subscribe_request
                        )
                        await ws.send(subscribe_request_json)
                    async for raw_msg in self._inner_messages(ws):
                        message = json.loads(raw_msg)
                        fill_event_message = EventMessage.decode_value_into_cls(message)
                        self.fill_stream_queue.put_nowait(fill_event_message)

            except asyncio.CancelledError:
                raise
            except Exception:
                print(
                    "Unexpected error with WebSocket connection. Retrying after 30 seconds...",
                )
                await asyncio.sleep(30.0)
            finally:
                self._reset()

    async def _producer_handler(self):
        """
        Produce data to be saved and/or sent to the Trader based on
        what has already been consumed earlier.
        """

        while True:
            message = await self.fill_stream_queue.get()
            fill_event_data_entry = message.message_content

            # Construct FillIdentifier given the message topic
            fill_identifier = FillIdentifier.decode_topic_string_into_cls(
                fill_event_data_entry["t"]
            )

            # Obtain the real-time Fill transaction we have subscribed
            # to that was just emitted. Currently, nothing is done with
            # this but traders are welcome to handle this however they
            # so choose.
            if fill_event_data_entry["c"]["reason"] == "Trade":
                fill = TradeFill.decode_event_trigger_into_cls(
                    fill_event_data_entry["c"]
                )
