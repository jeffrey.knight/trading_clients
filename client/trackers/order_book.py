"""
DerivaDEX Order Book
"""

from typing import Dict, Optional, List
from decimal import Decimal
import numpy as np

from trading_clients.auditor.smt.h256 import H256
from trading_clients.auditor.state.book_order import BookOrder
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.transactions.cancel import Cancel
from trading_clients.auditor.transactions.event_types import EventType
from trading_clients.auditor.transactions.fill import Fill
from trading_clients.auditor.transactions.liquidation_fill import LiquidationFill
from trading_clients.auditor.transactions.post import Post
from trading_clients.auditor.transactions.trade_fill import TradeFill
from trading_clients.auditor.utils import to_camel_case


class OrderBook:
    def __init__(self) -> None:
        """
        Initialize an order book. Each order book is tied to a given
        market, and contains both L2 and L3 information. An L2 order
        book is an aggregate view of the market, providing information
        regarding unique price levels and total quantities at those
        price levels. L3 order books maintain order-by-order, granular
        data.
        """

        # Maintain a mapping of price level to another mapping of
        # a BookOrder's key to the BookOrder itself. Python dicts
        # are inherently guaranteed to maintain insertion order. (L3)
        self.bids_l3: Dict[Decimal, Dict[bytes, BookOrder]] = {}
        self.asks_l3: Dict[Decimal, Dict[bytes, BookOrder]] = {}

        # Maintain a mapping of price level to aggregate quantity. (L2)
        self.bids_l2: Dict[Decimal, Decimal] = {}
        self.asks_l2: Dict[Decimal, Decimal] = {}

        # Mapping of BookOrder's key to its price. We need to maintain
        # this to more efficiently delete orders from the book for
        # cancellation since the Cancel transaction doesn't readily
        # contain any price information.
        self.key_to_price: Dict[bytes, Decimal] = {}

    @property
    def best_bid_px(self) -> Optional[Decimal]:
        """
        Property returning the best available (highest) bid price.

        Returns
        -------
        Optional[Decimal]
            Best bid
        """

        return max(list(self.bids_l2.keys())) if self.bids_l2 else None

    @property
    def best_ask_px(self) -> Optional[Decimal]:
        """
        Property returning the best available (lowest) ask price.

        Returns
        -------
        Optional[Decimal]
            Best ask
        """

        return min(list(self.asks_l2.keys())) if self.asks_l2 else None

    @property
    def mid_px(self) -> Optional[Decimal]:
        """
        Property returning the mid price for the market.

        Returns
        -------
        Optional[Decimal]
            Mid price
        """

        if self.bids_l2 and self.asks_l2:
            return np.mean([self.best_bid_px, self.best_ask_px])
        elif self.bids_l2:
            return self.best_bid_px
        elif self.asks_l2:
            return self.best_ask_px
        return None

    @property
    def order_book_l2(self) -> Dict[str, Dict[Decimal, Decimal]]:
        """
        Property returning the L2 order book.

        Returns
        -------
        Dict[str, Dict[Decimal, Decimal]]
            L2 order book
        """

        return {"bids": self.bids_l2, "asks": self.asks_l2}

    @property
    def order_book_l3(self) -> Dict[str, Dict[Decimal, Dict[bytes, BookOrder]]]:
        """
        Property returning the L3 order book.

        Returns
        -------
        Dict[str, Dict[Decimal, Dict[bytes, BookOrder]]]
            L3 order book
        """

        return {"bids": self.bids_l3, "asks": self.asks_l3}

    async def process_book_order_partial(self, book_order_message: ItemMessage) -> None:
        """
        Process the partial response from the Auditor API, which returns
        a list of the BookOrder leaves currently on the order book.
        This snapshot is sent as soon as you first subscribe to the
        BookOrder state update channel for a given market. The response
        contains the BookOrder keys and the BookOrder values. We take
        this snapshot and assemble an L2 order book by aggregating
        quantities for all unique price levels. Additionally, we take
        this snapshot and assemble an L3 order book by ordering each
        of the BookOrders at their respective price levels based on
        their ordinal value.

        Parameters
        ----------
        book_order_message : ItemMessage
            Snapshot partial of the order book
        """

        # Sort partial by book ordinal to then enter in the L3 order
        # book data structure at the appropriate price level
        sorted_book_order_partial = sorted(
            book_order_message.message_content["item_data"],
            key=lambda book_order: book_order["c"]["bookOrdinal"],
        )

        # Loop through each BookOrder item in the sorted list
        for book_order_item in sorted_book_order_partial:
            # Construct BookOrder identifier based on the topic
            book_order_identifier = BookOrderIdentifier.decode_topic_string_into_cls(
                book_order_item["t"]
            )

            # Obtain BookOrder leaf
            book_order = BookOrder.decode_value_into_cls(book_order_item["c"])

            # Determine sided order book we are working with
            sided_book_l2 = self.bids_l2 if book_order.side == 0 else self.asks_l2
            sided_book_l3 = self.bids_l3 if book_order.side == 0 else self.asks_l3

            if book_order.price not in sided_book_l2:
                # BookOrder is forming a new price level
                sided_book_l2[book_order.price] = book_order.amount
                sided_book_l3[book_order.price] = {
                    book_order_identifier.encoded_key: book_order
                }
            else:
                # BookOrder is adding to an existing price level
                sided_book_l2[book_order.price] += book_order.amount
                sided_book_l3[book_order.price][
                    book_order_identifier.encoded_key
                ] = book_order

            # Add BookOrder key to the key to price mapping
            self.key_to_price[book_order_identifier.encoded_key] = book_order.price

    async def process_book_order_update(self, book_order_message: ItemMessage) -> None:
        """
        Process the update response from the Auditor API, which consists
        of a BookOrder update to the order book. An update could be
        due to a new order (Post tx), a filled order (Fill tx), or a
        cancellation (Cancel tx). This update message consists of the
        BookOrder key, the new BookOrder value, and also the event
        trigger that caused this update. We can use this data to adjust
        both the L2 and the L3 order book views.

        Parameters
        ----------
        book_order_message : ItemMessage
            Update to the order book
        """

        # Obtain the event trigger that caused this update
        book_order_event_trigger = book_order_message.message_content["event_trigger"]

        # Obtain the BookOrder that makes up this update
        book_order_item = book_order_message.message_content["item_data"]

        # Construct the BookOrder identifier based on this topic and
        # derive the BookOrder key
        book_order_identifier = BookOrderIdentifier.decode_topic_string_into_cls(
            book_order_item["t"]
        )
        book_order_encoded_key = book_order_identifier.encoded_key

        # Obtain BookOrder leaf
        book_order = BookOrder.decode_value_into_cls(book_order_item["c"])

        event_type = to_camel_case(
            EventType(book_order_event_trigger["eventType"]).name
        )

        # Handle this update depending on what kind of trigger caused it
        if event_type == "Post":
            # Update caused by a Post transaction (new order)

            # Obtain the Post transaction that triggered the update
            post_tx = Post.decode_event_trigger_into_cls(book_order_event_trigger)

            # Determine the sided order book we are working with
            sided_book_l2 = self.bids_l2 if post_tx.is_bid else self.asks_l2
            sided_book_l3 = self.bids_l3 if post_tx.is_bid else self.asks_l3

            if post_tx.price not in sided_book_l2:
                # BookOrder is forming a new price level
                sided_book_l2[post_tx.price] = post_tx.amount
                sided_book_l3[post_tx.price] = {book_order_encoded_key: book_order}
            else:
                # BookOrder is adding to an existing price level
                sided_book_l2[post_tx.price] += post_tx.amount
                sided_book_l3[post_tx.price][book_order_encoded_key] = book_order

            # Add BookOrder key to the key to price mapping
            self.key_to_price[book_order_encoded_key] = post_tx.price
        elif event_type == "Fill":
            # Update caused by a Fill transaction (either a CompleteFill
            # or a PartialFill)

            # Obtain the Fill transaction that triggered the update
            fill_tx = (
                TradeFill.decode_event_trigger_into_cls(book_order_event_trigger)
                if book_order_event_trigger["reason"] == "Trade"
                else LiquidationFill.decode_event_trigger_into_cls(
                    book_order_event_trigger
                )
            )

            # Determine the sided order book we are working with
            sided_book_l2 = self.bids_l2 if not fill_tx.taker_is_bid else self.asks_l2
            sided_book_l3 = self.bids_l3 if not fill_tx.taker_is_bid else self.asks_l3

            # Decrement the aggregate quantity at the price level (L2)
            sided_book_l2[fill_tx.price] -= fill_tx.amount

            if isinstance(book_order, Empty):
                # If the updated BookOrder is now empty

                # Delete the BookOrder from the L3 order book
                del sided_book_l3[fill_tx.price][book_order_encoded_key]

                if sided_book_l2[fill_tx.price] == Decimal("0"):
                    # If the price level is now empty, remove it
                    # entirely from the L2 and L3 order books
                    del sided_book_l2[fill_tx.price]
                    del sided_book_l3[fill_tx.price]
        else:
            # Update caused by a Cancel transaction

            # Obtain the Cancel transaction that triggered the update
            cancel_tx = Cancel.decode_event_trigger_into_cls(book_order_event_trigger)

            # The Cancel transaction itself doesn't contain any price
            # information readily, so we rely on the mapping we have
            # at our disposal to obtain the price for the BookOrder
            # that was canceled
            book_order_price = self.key_to_price[book_order_encoded_key]

            # Determine the sided order book we are working with
            sided_book_l2 = (
                self.bids_l2 if book_order_price in self.bids_l2 else self.asks_l2
            )
            sided_book_l3 = (
                self.bids_l3 if book_order_price in self.bids_l3 else self.asks_l3
            )

            # Decrement the aggregate quantity at the price level (L2)
            sided_book_l2[book_order_price] -= cancel_tx.amount

            if isinstance(book_order, Empty):
                # If the updated BookOrder is now empty

                # Delete the BookOrder from the L3 order book
                del sided_book_l3[book_order_price][book_order_encoded_key]

                if sided_book_l2[book_order_price] == Decimal("0"):
                    # If the price level is now empty, remove it
                    # entirely from the L2 and L3 order books
                    del sided_book_l2[book_order_price]
                    del sided_book_l3[book_order_price]

    def __repr__(self):
        return f"Order book (L2): bids = {self.bids_l2}; asks = {self.asks_l2}"
