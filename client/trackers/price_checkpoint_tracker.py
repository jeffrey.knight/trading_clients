"""
PriceCheckpointTracker module
"""

import asyncio
from collections import defaultdict
from typing import DefaultDict, Optional, List
import websockets
import simplejson as json

from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.transactions.event_message import EventMessage
from trading_clients.auditor.transactions.identifiers.price_checkpoint_identifier import (
    PriceCheckpointIdentifier,
)
from trading_clients.auditor.transactions.price_checkpoint import PriceCheckpoint
from trading_clients.auditor.utils import ComplexOutputEncoder
from trading_clients.client.asyncio_utils import empty_queue
from trading_clients.client.trackers.tracker import Tracker
from trading_clients.client.websocket_message import (
    WebsocketMessage,
    WebsocketMessageType,
)


class PriceCheckpointTracker(Tracker):
    """
    Defines a PriceCheckpointTracker
    """

    def __init__(
        self,
        price_checkpoint_identifiers: Optional[List[PriceCheckpointIdentifier]] = None,
        ws_uri: str = None,
    ):
        """
        Initialize a PriceCheckpointTracker. A PriceCheckpointTracker is
        an async task that can send PriceCheckpoint-related data, or
        receive real-time PriceCheckpoints a user has subscribed to.

        Parameters
        ----------
        price_checkpoint_identifiers: Optional[List[PriceCheckpointIdentifier]]
            List of PriceCheckpointIdentifiers user is subscribing to
        ws_uri : str
           WebSocket URI
        """

        self.price_checkpoint_identifiers = price_checkpoint_identifiers
        self.ws_uri = ws_uri

        self.latest_price_checkpoints: DefaultDict[
            str, Optional[PriceCheckpoint]
        ] = defaultdict(lambda: None)
        self.price_checkpoint_stream_queue: asyncio.Queue[ItemMessage] = asyncio.Queue()

    def _reset(self):
        self.latest_price_checkpoints.clear()
        empty_queue(self.price_checkpoint_stream_queue)

    async def _consumer_handler(self):
        """
        Consume data from the Auditor after subscribing to the various
        Identifiers.
        """

        while True:
            try:
                async with websockets.connect(self.ws_uri, max_size=2 ** 32) as ws:
                    ws: websockets.WebSocketClientProtocol = ws
                    for (
                        price_checkpoint_identifier
                    ) in self.price_checkpoint_identifiers:
                        subscribe_request = WebsocketMessage(
                            WebsocketMessageType.SUBSCRIBE,
                            price_checkpoint_identifier.topic_string,
                        )
                        subscribe_request_json = ComplexOutputEncoder().encode(
                            subscribe_request
                        )
                        await ws.send(subscribe_request_json)
                    async for raw_msg in self._inner_messages(ws):
                        message = json.loads(raw_msg)
                        price_checkpoint_event_message = EventMessage.decode_value_into_cls(
                            message
                        )
                        self.price_checkpoint_stream_queue.put_nowait(
                            price_checkpoint_event_message
                        )

            except asyncio.CancelledError:
                raise
            except Exception:
                print(
                    "Unexpected error with WebSocket connection. Retrying after 30 seconds...",
                )
                await asyncio.sleep(30.0)
            finally:
                self._reset()

    async def _producer_handler(self):
        """
        Produce data to be saved and/or sent to the Trader based on
        what has already been consumed earlier.
        """

        while True:
            message = await self.price_checkpoint_stream_queue.get()
            price_checkpoint_event_data_entry = message.message_content
            price_checkpoint_identifier = PriceCheckpointIdentifier.decode_topic_string_into_cls(
                price_checkpoint_event_data_entry["t"]
            )

            # Save latest price checkpoint in live-updating dict
            self.latest_price_checkpoints[
                price_checkpoint_identifier.symbol
            ] = PriceCheckpoint.decode_event_trigger_into_cls(
                price_checkpoint_event_data_entry["c"]
            )
