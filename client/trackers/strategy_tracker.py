"""
StrategyTracker module
"""

import asyncio
from collections import defaultdict
from typing import DefaultDict, Optional, List, Dict
import websockets
import simplejson as json

from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from trading_clients.auditor.state.item_message import ItemMessage
from trading_clients.auditor.state.strategy import Strategy
from trading_clients.auditor.utils import ComplexOutputEncoder
from trading_clients.client.asyncio_utils import empty_queue
from trading_clients.client.trackers.tracker import Tracker
from trading_clients.client.websocket_message import (
    WebsocketMessage,
    WebsocketMessageType,
    WebsocketEventType,
)


class StrategyTracker(Tracker):
    """
    Defines a StrategyTracker
    """

    def __init__(
        self,
        strategy_identifiers: Optional[List[StrategyIdentifier]] = None,
        ws_uri: str = None,
    ):
        """
        Initialize a StrategyTracker. A StrategyTracker is an async
        task that can send Strategy-related data, or receive real-time
        updates to Strategies a user has subscribed to.

        Parameters
        ----------
        strategy_identifiers: Optional[List[StrategyIdentifier]]
            List of StrategyIdentifiers user is subscribing to
        ws_uri : str
           WebSocket URI
        """

        self.strategy_identifiers = strategy_identifiers
        self.ws_uri = ws_uri

        self.strategies: DefaultDict = defaultdict(lambda: Empty())
        self.strategy_stream_queue: asyncio.Queue[ItemMessage] = asyncio.Queue()

    def _reset(self):
        self.strategies.clear()
        empty_queue(self.strategy_stream_queue)

    async def _consumer_handler(self):
        """
        Consume data from the Auditor after subscribing to the various
        Identifiers.
        """

        while True:
            try:
                async with websockets.connect(self.ws_uri, max_size=2 ** 32) as ws:
                    ws: websockets.WebSocketClientProtocol = ws
                    for strategy_identifier in self.strategy_identifiers:
                        subscribe_request = WebsocketMessage(
                            WebsocketMessageType.SUBSCRIBE,
                            strategy_identifier.topic_string,
                        )
                        subscribe_request_json = ComplexOutputEncoder().encode(
                            subscribe_request
                        )
                        await ws.send(subscribe_request_json)
                    async for raw_msg in self._inner_messages(ws):
                        message = json.loads(raw_msg)
                        strategy_leaf_event_message = ItemMessage.decode_value_into_cls(
                            message
                        )
                        self.strategy_stream_queue.put_nowait(
                            strategy_leaf_event_message
                        )

            except asyncio.CancelledError:
                raise
            except Exception:
                print(
                    "Unexpected error with WebSocket connection. Retrying after 30 seconds...",
                )
                await asyncio.sleep(30.0)
            finally:
                self._reset()

    async def _process_item_data_entry(self, item_data_entry: Dict):
        """
        Process individual state leaf update message from the Auditor.

        Parameters
        ----------
        item_data_entry: dict
            An individual state leaf update message entry
        """

        # Derive StrategyIdentifier based on the message topic
        identifier = StrategyIdentifier.decode_topic_string_into_cls(
            item_data_entry["t"]
        )

        # Save updated Strategy leaf in live-updating dict
        self.strategies[
            (identifier.trader_address, identifier.abbrev_strategy_id_hash,)
        ] = Strategy.decode_value_into_cls(item_data_entry["c"])

    async def _producer_handler(self):
        """
        Produce data to be saved and/or sent to the Trader based on
        what has already been consumed earlier.
        """

        while True:
            message = await self.strategy_stream_queue.get()
            if message.message_event == WebsocketEventType.PARTIAL:
                for item_data_entry in message.message_content["item_data"]:
                    await self._process_item_data_entry(item_data_entry)
            else:
                await self._process_item_data_entry(
                    message.message_content["item_data"]
                )
