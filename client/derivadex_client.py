"""
DerivaDEX WS Client with Auditor module
"""
import asyncio
from decimal import Decimal
from typing import List, Optional

import aiohttp
from web3 import Web3
from web3.middleware import geth_poa_middleware
from eth_abi import encode_single
from eth_abi.utils.padding import zpad32_right
import time

from zero_ex.contract_wrappers import TxParams

from trading_clients.auditor.state.identifiers.trader_identifier import TraderIdentifier
from trading_clients.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from trading_clients.auditor.state.identifiers.position_identifier import (
    PositionIdentifier,
)
from trading_clients.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from trading_clients.auditor.transactions.identifiers.fill_identifier import (
    FillIdentifier,
)
from trading_clients.auditor.transactions.identifiers.price_checkpoint_identifier import (
    PriceCheckpointIdentifier,
)
from trading_clients.auditor.utils import ComplexOutputEncoder
from trading_clients.client.encryption_utils import encrypt_with_nonce
from trading_clients.client.trackers.fill_tracker import FillTracker
from trading_clients.client.dummy_token import DummyToken
from trading_clients.client.trader import Trader
from trading_clients.client.ddx import DDX
from trading_clients.helpers.cancel_intent import CancelIntent
from trading_clients.client.asyncio_utils import safe_ensure_future
from trading_clients.client.sig_utils import (
    compute_eip712_create_order_hash,
    compute_eip712_cancel_order_hash,
)
from trading_clients.client.trackers.book_order_tracker import BookOrderTracker
from trading_clients.client.trackers.command_tracker import CommandTracker
from trading_clients.client.trackers.order_book_tracker import OrderBookTracker
from trading_clients.client.trackers.position_tracker import PositionTracker
from trading_clients.client.trackers.price_checkpoint_tracker import (
    PriceCheckpointTracker,
)
from trading_clients.client.trackers.strategy_tracker import StrategyTracker
from trading_clients.client.trackers.trader_tracker import TraderTracker
from trading_clients.helpers.order_intent import OrderIntent


class DerivaDEXClient:
    def __init__(
        self,
        auditor_host: str,
        rpc_url: str,
        staging_env: str,
        chain_id: int,
        verifying_contract: str,
        order_book_symbols: Optional[List[str]] = None,
        trader_identifiers: Optional[List[TraderIdentifier]] = None,
        strategy_identifiers: Optional[List[StrategyIdentifier]] = None,
        position_identifiers: Optional[List[PositionIdentifier]] = None,
        book_order_identifiers: Optional[List[BookOrderIdentifier]] = None,
        price_checkpoint_identifiers: Optional[List[PriceCheckpointIdentifier]] = None,
        fill_identifiers: Optional[List[FillIdentifier]] = None,
        private_key: str = None,
        mnemonic: str = None,
    ) -> None:
        """
        Initialize DerivaDEX client with Auditor

        Parameters
        ----------
        auditor_host: str
            The host URL of the auditor
        rpc_url : str
            RPC URL for ETH connections
        staging_env : str
            Staging environment (test || pre || alpha || beta)
        chain_id: int
            Chain ID of contracts used for EIP-712 signing
        verifying_contract: str
            DerivaDEX contract address used for EIP-712 signing
        order_book_symbols : List[str]
            Market symbols to track order books
        trader_identifiers : List[TraderIdentifier]
            Trader identifiers to track Trader data
        strategy_identifiers : List[StrategyIdentifier]
            Strategy identifiers to track Strategy data
        position_identifiers : List[PositionIdentifier]
            Position identifiers to track Position data
        book_order_identifiers : List[BookOrderIdentifier]
            BookOrder identifiers to track BookOrder data
        price_checkpoint_identifiers : List[PriceCheckpointIdentifier]
            PriceCheckpoint identifiers to track PriceCheckpoint txs
        fill_identifiers : List[FillIdentifier]
            FillIdentifier identifiers to track Fill txs
        private_key : str
            Ethereum private key for user
        mnemonic : str
            Ethereum mnemonic for user
        """

        # Set the auditor host URL.
        self.auditor_host = auditor_host

        # Initialize web3 service
        self.w3 = Web3(Web3.HTTPProvider(rpc_url, request_kwargs={"timeout": 60}))
        if staging_env == "fuzz":
            self.w3.middleware_onion.inject(geth_poa_middleware, layer=0)

        # Initialize web3 account from private key or mnemonic
        if private_key is not None:
            self.web3_account = self.w3.eth.account.from_key(private_key)
        else:
            self.w3.eth.account.enable_unaudited_hdwallet_features()
            self.web3_account = self.w3.eth.account.from_mnemonic(mnemonic)

        # Set default account for send transactions
        self.w3.eth.defaultAccount = self.web3_account.address

        # Staging environment string to be used in API connection URL
        self.staging_env = staging_env

        # Chain ID and verifying contract address (EIP-712 signing)
        self.chain_id = chain_id
        self.verifying_contract = verifying_contract

        # Set up trackers
        self.command_tracker = CommandTracker(self._get_url())
        self.order_book_tracker = OrderBookTracker(order_book_symbols, self._get_url())
        self.trader_tracker = TraderTracker(trader_identifiers, self._get_url())
        self.strategy_tracker = StrategyTracker(strategy_identifiers, self._get_url())
        self.position_tracker = PositionTracker(position_identifiers, self._get_url())
        self.book_order_tracker = BookOrderTracker(
            book_order_identifiers, self._get_url()
        )
        self.price_checkpoint_tracker = PriceCheckpointTracker(
            price_checkpoint_identifiers, self._get_url()
        )
        self.fill_tracker = FillTracker(fill_identifiers, self._get_url())

    async def start_network(self):
        """
        Start the task network for various trackers. The trackers
        can send and receive data from the Auditor API for commands
        and/or subscriptions to state updates or transaction events.
        """

        self._command_tracker_task = safe_ensure_future(self.command_tracker.start())

        self._order_book_tracker_task = safe_ensure_future(
            self.order_book_tracker.start()
        )
        self._trader_tracker_task = safe_ensure_future(self.trader_tracker.start())
        self._strategy_tracker_task = safe_ensure_future(self.strategy_tracker.start())
        self._position_tracker_task = safe_ensure_future(self.position_tracker.start())
        self._book_order_tracker_task = safe_ensure_future(
            self.book_order_tracker.start()
        )

        self._price_checkpoint_tracker_task = safe_ensure_future(
            self.price_checkpoint_tracker.start()
        )
        self._fill_tracker_task = safe_ensure_future(self.fill_tracker.start())

    @staticmethod
    def get_encoded_nonce() -> str:
        """
        Get encoded nonce to be used as the unique nonce in various
        commands

        Returns
        -------
        str
            32-byte hex-encoded nonce string
        """

        # Retrieve current UNIX time in nanoseconds to derive a unique, monotonically-increasing nonce
        nonce = str(time.time_ns())

        # abi.encode(['bytes32'], [nonce])
        return encode_single("bytes32", nonce.encode("utf8")).hex()

    def _get_url(self) -> str:
        """
        Implementing the URL connection for WebSocket manager

        Returns
        -------
        str
            URL connection string
        """

        return f"ws://{self.auditor_host}:8765"

    # ************** COMMANDS ************** #

    async def create_orders(self, order_intents: List[OrderIntent]):
        """
        Create orders against the Auditor WebSocket API

        Parameters
        ----------
        order_intents : List[OrderIntent]
            Order intents to create
        """

        async with aiohttp.ClientSession() as client:
            for order_intent in order_intents:
                contents = {
                    "t": "Order",
                    "c": {
                        "traderAddress": order_intent.trader_address,
                        "symbol": order_intent.symbol,
                        "strategy": order_intent.strategy,
                        "side": order_intent.side,
                        "orderType": order_intent.order_type,
                        "nonce": order_intent.nonce,
                        "amount": order_intent.amount,
                        "price": order_intent.price,
                        "stopPrice": order_intent.stop_price,
                        "signature": self.web3_account.signHash(
                            bytes.fromhex(
                                compute_eip712_create_order_hash(
                                    self.chain_id, self.verifying_contract, order_intent
                                )
                            )
                        ).signature.hex(),
                    },
                }
                encrypted_contents = encrypt_with_nonce(
                    ComplexOutputEncoder().encode(contents)
                )
                response = await client.post(
                    f"https://{self.staging_env}.derivadex.io/node0/v2/request",
                    data=encrypted_contents,
                )
                # try:
                #     print("response", await response.json())
                # except:
                #     pass

    async def cancel_orders(self, cancel_intents: List[CancelIntent]):
        """
        Cancel orders against the Auditor WebSocket API

        Parameters
        ----------
        cancel_intents : List[CancelIntent]
            Cancel intents containing information to cancel open orders
        """

        async with aiohttp.ClientSession() as client:
            for cancel_intent in cancel_intents:
                contents = {
                    "t": "CancelOrder",
                    "c": {
                        "symbol": cancel_intent.symbol,
                        "orderHash": f"0x{zpad32_right(bytes.fromhex(cancel_intent.order_hash[2:])).hex()}",
                        "nonce": cancel_intent.nonce,
                        "signature": self.web3_account.signHash(
                            bytes.fromhex(
                                compute_eip712_cancel_order_hash(
                                    self.chain_id,
                                    self.verifying_contract,
                                    cancel_intent,
                                )
                            )
                        ).signature.hex(),
                    },
                }
                encrypted_contents = encrypt_with_nonce(
                    ComplexOutputEncoder().encode(contents)
                )
                response = await client.post(
                    f"https://{self.staging_env}.derivadex.io/node0/v2/request",
                    data=encrypted_contents,
                )
                # try:
                #     print("response", await response.json())
                # except:
                #     pass

    # ************** REST ************** #

    async def get_trader(self, trader_address: Optional[str]):
        # Construct TraderIdentifier
        trader_identifier = TraderIdentifier(trader_address)

        async with aiohttp.ClientSession() as client:
            resp = await client.get(
                f"http://{self.auditor_host}:8766/trader",
                params={"topic": trader_identifier.topic_string},
            )
            resp_json = await resp.json()
            return resp_json

    async def get_strategy(
        self, trader_address: Optional[str], strategy_id: Optional[str],
    ):
        """
        Get Strategy leaves from the REST Auditor API based on a
        chain discriminant, trader address, and strategy ID.

        Parameters
        ----------
        trader_address : Optional[str]
            Trader's Ethereum address
        strategy_id : Optional[str]
            Cross-margined strategy ID (e.g. 'main')
        """

        # Derive abbreviated strategy ID hash (i.e. the first 4 bytes
        # of the hash of the strategy ID)
        abbrev_strategy_id_hash = (
            (f"{StrategyIdentifier.generate_strategy_id_hash(strategy_id).hex()}")
            if strategy_id is not None
            else None
        )

        # Construct StrategyIdentifier
        strategy_identifier = StrategyIdentifier(
            trader_address, abbrev_strategy_id_hash
        )

        async with aiohttp.ClientSession() as client:
            resp = await client.get(
                f"http://{self.auditor_host}:8766/strategy",
                params={"topic": strategy_identifier.topic_string},
            )
            resp_json = await resp.json()
            return resp_json

    async def get_position(
        self,
        symbol: Optional[str],
        trader_address: Optional[str],
        strategy_id: Optional[str],
    ):
        """
        Get Position leaves from the REST Auditor API based on a symbol,
        chain discriminant, trader address, and strategy ID.

        Parameters
        ----------
        symbol : Optional[int]
            Market symbol
        trader_address : Optional[str]
            Trader's Ethereum address
        strategy_id : Optional[str]
            Cross-margined strategy ID (e.g. 'main')
        """

        # Derive abbreviated strategy ID hash (i.e. the first 4 bytes
        # of the hash of the strategy ID)
        abbrev_strategy_id_hash = (
            (f"{StrategyIdentifier.generate_strategy_id_hash(strategy_id).hex()}")
            if strategy_id is not None
            else None
        )

        # Construct PositionIdentifier
        position_identifier = PositionIdentifier(
            symbol, trader_address, abbrev_strategy_id_hash
        )

        async with aiohttp.ClientSession() as client:
            resp = await client.get(
                f"http://{self.auditor_host}:8766/position",
                params={"topic": position_identifier.topic_string},
            )
            resp_json = await resp.json()
            return resp_json

    async def get_book_order(self, symbol: str, trader_address: str, strategy_id: str):
        """
        Get BookOrder leaves from the REST Auditor API based on a symbol,
        trader address, and strategy ID.

        Parameters
        ----------
        symbol : Optional[int]
            Market symbol
        trader_address : Optional[str]
            Trader's Ethereum address
        strategy_id : Optional[str]
            Cross-margined strategy ID (e.g. 'main')
        """

        # Derive abbreviated strategy ID hash (i.e. the first 4 bytes
        # of the hash of the strategy ID)
        abbrev_strategy_id_hash = (
            (f"{StrategyIdentifier.generate_strategy_id_hash(strategy_id).hex()}")
            if strategy_id is not None
            else None
        )

        # Construct BookOrderIdentifier
        book_order_identifier = BookOrderIdentifier(
            symbol, None, trader_address, abbrev_strategy_id_hash
        )

        async with aiohttp.ClientSession() as client:
            resp = await client.get(
                f"http://{self.auditor_host}:8766/book_order",
                params={"topic": book_order_identifier.topic_string},
            )
            resp_json = await resp.json()
            return resp_json

    async def approve(self, collateral_address: str, amount: Decimal):
        """
        Approve ERC-20 collateral (i.e., USDC).

        Parameters
        ----------
        collateral_address : str
            USDC collateral address
        amount : Decimal
            Amount to approve (and eventually deposit)
        """

        dummy_token_contract = DummyToken(self.w3, collateral_address)

        # Approve for transfer
        nonce = self.w3.eth.getTransactionCount(self.web3_account.address)
        built_tx = dummy_token_contract.approve.build_transaction(
            self.verifying_contract,
            int(amount * Decimal("1e6")),
            tx_params=TxParams(from_=self.web3_account.address, nonce=nonce),
        )
        signed_tx = self.w3.eth.account.sign_transaction(
            built_tx, private_key=self.web3_account.privateKey
        )
        self.w3.eth.sendRawTransaction(signed_tx.rawTransaction)

    async def deposit(self, collateral_address: str, amount: Decimal):
        """
        Deposit ERC-20 collateral (i.e., USDC).

        Parameters
        ----------
        collateral_address : str
            USDC collateral address
        amount : Decimal
            Amount to deposit
        """

        trader_contract = Trader(self.w3, self.verifying_contract)

        nonce = self.w3.eth.getTransactionCount(self.web3_account.address)
        built_tx = trader_contract.deposit.build_transaction(
            collateral_address,
            zpad32_right(
                len("main").to_bytes(1, byteorder="little") + "main".encode("utf8")
            ),
            int(amount * Decimal("1e6")),
            tx_params=TxParams(
                from_=self.web3_account.address, nonce=nonce, gas=500_000
            ),
        )
        signed_tx = self.w3.eth.account.sign_transaction(
            built_tx, private_key=self.web3_account.privateKey
        )
        self.w3.eth.sendRawTransaction(signed_tx.rawTransaction)

    async def mint(
        self,
        ddx_address: str,
        faucet_address: str,
        faucet_private_key: str,
        amount: Decimal,
    ):
        """
        Mint DDX (only meant for dev and testnet!!).

        Parameters
        ----------
        ddx_address : str
            DDX token address
        faucet_address : str
            Faucet address
        faucet_private_key : str
            Faucet private key
        amount : Decimal
            Amount to mint
        """

        ddx_contract = DDX(self.w3, ddx_address)

        nonce = self.w3.eth.getTransactionCount(faucet_address)
        built_tx = ddx_contract.transfer.build_transaction(
            self.web3_account.address,
            int(amount * Decimal("1e18")),
            tx_params=TxParams(from_=faucet_address, nonce=nonce),
        )
        signed_tx = self.w3.eth.account.sign_transaction(
            built_tx, private_key=faucet_private_key
        )
        self.w3.eth.sendRawTransaction(signed_tx.rawTransaction)

    async def approve_ddx(self, ddx_address: str, amount: Decimal):
        """
        Approve DDX for transfer.

        Parameters
        ----------
        ddx_address : str
            DDX token address
        amount : Decimal
            Amount to approve (and eventually transfer)
        """

        ddx_contract = DDX(self.w3, ddx_address)

        nonce = self.w3.eth.getTransactionCount(self.web3_account.address)
        built_tx = ddx_contract.approve.build_transaction(
            self.verifying_contract,
            int(amount * Decimal("1e18")),
            tx_params=TxParams(from_=self.web3_account.address, nonce=nonce),
        )
        signed_tx = self.w3.eth.account.sign_transaction(
            built_tx, private_key=self.web3_account.privateKey
        )
        self.w3.eth.sendRawTransaction(signed_tx.rawTransaction)

    async def deposit_ddx(self, amount: Decimal):
        """
        Deposit DDX to exchange.

        Parameters
        ----------
        amount : Decimal
            Amount to deposit
        """

        trader_contract = Trader(self.w3, self.verifying_contract)

        nonce = self.w3.eth.getTransactionCount(self.web3_account.address)
        built_tx = trader_contract.deposit_ddx.build_transaction(
            int(amount * Decimal("1e18")),
            tx_params=TxParams(
                from_=self.web3_account.address, nonce=nonce, gas=500_000
            ),
        )
        signed_tx = self.w3.eth.account.sign_transaction(
            built_tx, private_key=self.web3_account.privateKey
        )
        self.w3.eth.sendRawTransaction(signed_tx.rawTransaction)
